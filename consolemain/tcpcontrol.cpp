#include "globals.h"
PROGMEM const char lastsave[] ="2024-08-05 20:48:23";
#define TCPCONTROL_CPP_VER 3

/** @package

    tcpcontrol.cpp

    Thsi module is responsible for handling projector on off, status and also scene changes on SQ6 mixer

    Copyright(c)  2000
    
    Author: ANDREW COPSEY
    Created: AC  29/12/2018 17:37:08
	Last change: AC 14/04/2020 12:15:45
*/

#include <SkaarhojPgmspace.h>
#include "utilities.h"
#include <wire.h>
#include "config.h"
#include "buttondriver.h"
#include "leddriver.h"
#include "atemutils.h"
#include "camerautils.h"
#include "tcpControl.h"
#include <Ethernet.h>




// enable this for verbose debug
//#define TCP_DEBUG

// enable this for proj debugging - fake the presence of the projector
//#define FAKEPROJ

#ifdef FAKEPROJ
    // also define debug to make this useful
    #define TCP_DEBUG
    boolean fakeprojector;
#endif




EthernetClient tcpClient;

// This is the Panasonic MZ880 TCP port
#define PROJECTOR_TCP_PORT 1024

// and the allen and heath SQ6 port
#define SQ6_TCP_PORT 51325


//###############################################################
// ###### Timings and timeouts ############################
/*
*/


#define PROJSTATUSmS  5000  // 5 seconds

#define TCPTIMEOUTmS 100 // (default if we don't change it is 1000mS)


#define TXBUFFSIZE 8 // nominally only 6 bytes
#define RXBUFFSIZE 30  // max is actually 10 or so bytes - after that throw it away
#define ON_OFF_LOCKOUTmS 20000 // 20 seconds min time between projector on off commands

#define TX_ATTEMPTS 3
static int tcpRetries;
static uint8_t txBuffer[TXBUFFSIZE];
static uint8_t txBufferLen = 0;
static uint8_t rxBuffer[RXBUFFSIZE];
static uint16_t projLockoutTimer = 0;
static int projStatusTimer = 0;
static int sq6SceneDelay = 0;
static ubyte tcpTimer;
static boolean projPower = false;  // assume off
static boolean projError = false;  // assume no error

// thew command queue and a bit list of commands
uint8_t tcpCommands =0;
#define TCPC_PROJ_STATUS 0x01
#define TCPC_PROJ_ON 0x02
#define TCPC_PROJ_OFF 0x04
#define TCPC_SQ6_SCENE 0x08
uint8_t activeCommand =0;


static void printBuffer(uint8_t *buffer, uint8_t length)
{
    #define ASCBUFF 12
    char ascii[ASCBUFF];
    ubyte index;
    while ( length-- > 0 )
    {
        if (index < (ASCBUFF-1))
        {
            ascii[index++]=*buffer;
        }
        Serial.printf(F("%02xh "),*buffer++);
    }
    ascii[index] = '\0';
    Serial.printf(F("\t%s\n"),ascii);
}


typedef enum t_TCPControlStates
{
    TCP_BOOTUP,
    TCP_IDLE,
    TCP_CONNECT_RETRY_WAIT,
    TCP_CONNECT,
    TCP_CONNECT_ERROR,
    TCP_FLUSH,
    TCP_REPLY
    
};
static t_TCPControlStates TCPControlState = TCP_BOOTUP;


static void stopClient(void)
{
    #ifdef FAKEPROJ
    if (activeCommand == TCPC_SQ6_SCENE)
    {
        tcpClient.stop();
    }
    #else
    tcpClient.stop();
    #endif    

    #ifdef TCP_DEBUG
    printTime(true);
    Serial.printf(F("Stopping ethernet client \n\r"));
    #endif
}
 



static int connectClient(void)
{
    int returnVal = 1;

    if (activeCommand == TCPC_SQ6_SCENE)   
    {
        // client is the SQ6 desk
        tcpClient.setConnectionTimeout(TCPTIMEOUTmS);  // set the timeout duration for client.connect() and client.stop()

        returnVal = tcpClient.connect(configStore.IPConfigArray[CID_IP_ADDRESS_SQ6], SQ6_TCP_PORT);
    
        #ifdef TCP_DEBUG
        printTime(true); Serial.printf(F("SQ6 connect attempt %d - status = %d \n\r"),(TX_ATTEMPTS) - tcpRetries, returnVal);
        printIPAddress(CID_IP_ADDRESS_SQ6);
        Serial.printf(F(":%5d\n\r"),SQ6_TCP_PORT);
        #endif
    }
    else
    {
        #ifdef FAKEPROJ
        printTime(true);Serial.printf(F("Fake projector connected\n"));
        #else
        // client is the projector
        tcpClient.setConnectionTimeout(TCPTIMEOUTmS);  // set the timeout duration for client.connect() and client.stop()
        returnVal = tcpClient.connect(configStore.IPConfigArray[IP_ADDRESS_PROJECTOR], PROJECTOR_TCP_PORT);
        #endif

    
        #ifdef TCP_DEBUG
        printTime(true); Serial.printf(F("Projector connect attempt %d - status = %d \n\r"),(TX_ATTEMPTS) - tcpRetries, returnVal);
        printIPAddress(CID_IP_ADDRESS_PROJECTOR);
        Serial.printf(F(":%5d\n\r"),PROJECTOR_TCP_PORT);
        #endif

    }

    return returnVal;
}


static boolean clientAvailable(void)
{

    #ifdef FAKEPROJ
    if (activeCommand == TCPC_SQ6_SCENE)
    {
        return tcpClient.available()    ;
    }
    else
    {
        
        // had crashing problem  return (TCPControlState == TCP_REPLY)?true:false;
        if (TCPControlState == TCP_REPLY)
        {
            return true;
        }
        else
        {
            return false;

        }
    }

    #else
    return tcpClient.available();
    #endif
}

static void writeToClient(uint8_t *buffer, uint8_t length)
{

    memset(rxBuffer, RXBUFFSIZE, 0);
    #ifdef TCP_DEBUG
    printTime(true);  Serial.printf(F("Sent tx message to client ") );
    printBuffer(txBuffer,length);
    #endif

    #ifdef FAKEPROJ
    if (activeCommand == TCPC_SQ6_SCENE)
    {
        tcpClient.write(buffer, length );
    }
    #else
        tcpClient.write(buffer, length );
    #endif
}

static void clientRead(uint8_t *buffer)
{

    #ifdef FAKEPROJ
    if (activeCommand == TCPC_SQ6_SCENE)
    {
        tcpClient.read(buffer, RXBUFFSIZE);
    }
    else
    {
        // fake a proj on or off message in the buffer
        if (fakeprojector)
        {
            sprintf_P(rxBuffer,PSTR("00001\n")); //on message
        }
        else
        {
            sprintf_P(rxBuffer,PSTR("00000\n")); //oFF message
        }
    }

    #else
        tcpClient.read(buffer, RXBUFFSIZE);
    #endif



    #ifdef TCP_DEBUG
    printTime(true); Serial.printf(F("Rec'd "));
    printBuffer(rxBuffer, 12);
    #endif
}





static void parseReply(void)
{

    #ifdef TCP_DEBUG
    printTime(true); Serial.printf(F("%dWe recieved:"),activeCommand);
    printBuffer(rxBuffer,10);
    #endif

    if (activeCommand == TCPC_PROJ_STATUS)
    {
        if (memcmp_P(rxBuffer,PSTR("00000"),5 )== 0 )
        {
            #ifdef TCP_DEBUG
            Serial.printf(F("PRoJ is OFF\n"));
            #endif

            projPower = false; // OFF
            setLEDMode(LED_PROJ, LEDMODE_PATTERN_OFF);

        }
        else if (memcmp_P(rxBuffer,PSTR("00001"),5 )== 0 )
        {
            #ifdef TCP_DEBUG
            Serial.printf(F("PRoJ is ON\n"));
            #endif
            projPower = true; // ON
            setLEDMode(LED_PROJ, LEDMODE_PATTERN_ON);
        }

    }
}




void toggleProjectorOnOff(void)
{
    if(getVisualsPowerState() || (projPower == true))
    {

        // this can be called at any time on a long press of the Misc 2 button. 
        // If we not in proj lockout it queues an on or off command
        if (projLockoutTimer == 0)
        {
            #ifdef TCP_DEBUG
            printTime(true);Serial.printf(F("Toggle proj accepted\n"));
            #endif
            if (projPower)
            {
                tcpCommands |= TCPC_PROJ_OFF;            
            }
            else
            {
                tcpCommands |= TCPC_PROJ_ON;            
            }
        }
        else
        {
            #ifdef TCP_DEBUG
            printTime(true);Serial.printf(F("Toggle proj - wait - still in lockout\n"));
            #endif
            
            // cannot react at all the moment as we are busy, flash to indicate we are busy - 
            // use INVERTED flag to leave LED in correct state after flash
            if (projPower)
            {
                setLEDMode(LED_PROJ, LEDMODE_PATTERN_5050 |  LEDMODE_INVERTED, 1000/UI_UPDATEmS); // 1 secs fast flashing
            }
            else
            {
                setLEDMode(LED_PROJ, LEDMODE_PATTERN_5050 , 1000/UI_UPDATEmS); // 1 secs fast flashing
            }


    
        }
        projError= false; // have a go at talking to it - even if it wasn't responding last time
    }
    #ifdef TCP_DEBUG
    else
    {
        printTime(true);Serial.printf(F("Turn on proj not allowed when visuals keyswitch not on\n"));
    }
    #endif

}



void initTCPControl(void)
{
    printModuleStartup(PSTR("TCP Control Module"),TCPCONTROL_CPP_VER,TCPCONTROL_H_VER,lastsave);
   
    Serial.printf(F("\t"));
    printIPAddress(CID_IP_ADDRESS_PROJECTOR);
    Serial.printf(F("\t"));
    printIPAddress(CID_IP_ADDRESS_SQ6);

    #ifdef TCP_DEBUG
    Serial.printf(F("WARNING: Verbose TCP_DEBUG defined in TCPcontrol.cpp\n"));
    #endif

    #ifdef FAKEPROJ
    Serial.printf(F("WARNING: FAKE PROJECTOR defined in TCPcontrol.cpp\n"));
    #endif

    TCPControlState= TCP_BOOTUP;
    tcpTimer = 10;

}

boolean buildTxBuffer(void)
{
    if (tcpCommands != 0)
    {
        tcpRetries = TX_ATTEMPTS;
        txBufferLen = 6;


        if ((TCPC_SQ6_SCENE & tcpCommands) != 0)
        {
            tcpCommands &= ~TCPC_SQ6_SCENE;
            activeCommand = TCPC_SQ6_SCENE;
            txBuffer[0] = 0xB0; // bank number - midi channel 2
            txBuffer[1] = 0x00;
            txBuffer[2] = 0x00; // bank 0
            txBuffer[3] = 0xC0; // program change
            // we store scene number 1-nn. Sq6 also lables them as 1-nn but the midi command is 0 based
            txBuffer[4] = configStore.sq6_scene -1; // scene number
            txBufferLen = 5;
        }
        else if ((TCPC_PROJ_ON & tcpCommands) != 0)
        {
            sprintf_P(txBuffer,PSTR("00PON\r"));
            tcpCommands &= ~TCPC_PROJ_ON;
            activeCommand = TCPC_PROJ_ON;
            projLockoutTimer = ON_OFF_LOCKOUTmS/UI_UPDATEmS;
            projStatusTimer = 10; // check sttaus in about 10 ticks
            #ifdef FAKEPROJ
            fakeprojector = true;
            #endif
        }
        else if ((TCPC_PROJ_OFF & tcpCommands) != 0)
        {
            sprintf_P(txBuffer,PSTR("00POF\r"));
            tcpCommands &= ~TCPC_PROJ_OFF;
            activeCommand = TCPC_PROJ_OFF;
            projLockoutTimer = ON_OFF_LOCKOUTmS/UI_UPDATEmS;
            projStatusTimer = 10; // check sttaus in about 10 ticks
            #ifdef FAKEPROJ
            fakeprojector = false;
            #endif
        }
        else if ((TCPC_PROJ_STATUS & tcpCommands) != 0)
        {
            sprintf_P(txBuffer,PSTR("00QPW\r"));
            tcpCommands &= ~TCPC_PROJ_STATUS;
            activeCommand = TCPC_PROJ_STATUS;
        }
        else
        {
            Serial.printf(F("Build buffer command 0x%0x not handled\n"),tcpCommands)            ;
            tcpCommands = activeCommand = 0;
        }
        return true;
    }
    return false;
}

// caled every UI_UPDATEmS 10-200mS nominally 75mS
void TCPTick(void)
{
    if (projError == false)
    {
        if (--projStatusTimer <= 0)
        {
            // request projector status and set timer to do it again in future
            tcpCommands |= TCPC_PROJ_STATUS;
            projStatusTimer = PROJSTATUSmS/UI_UPDATEmS;
        }
    }
    if (sq6SceneDelay > 0 && --sq6SceneDelay == 0)
    {
        tcpCommands |= TCPC_SQ6_SCENE;
    }

    if ( projLockoutTimer >0 )
    {
        --projLockoutTimer;
    }
    switch ( TCPControlState )
    {
        case TCP_BOOTUP :
            TCPControlState = TCP_IDLE;
            break;

        case TCP_IDLE :
            if (buildTxBuffer())
            {
                // we have somethging to send to SQ6 or Projector
                if (activeCommand == TCPC_PROJ_ON)
                {
                    setLEDMode(LED_PROJ, LEDMODE_PATTERN_25PERCENT );
                }
                else if (activeCommand == TCPC_PROJ_OFF)
                {
                    setLEDMode(LED_PROJ, LEDMODE_PATTERN_25PERCENT | LEDMODE_SPEED_SLOW | LEDMODE_INVERTED );
                }
                TCPControlState = TCP_CONNECT;
            }
            break;

        case TCP_CONNECT_RETRY_WAIT :
            if ( --tcpTimer <= 0 )
            {
                TCPControlState = TCP_CONNECT;
            }
            break;

        case TCP_CONNECT :
            if ( tcpRetries-- > 0 )
            {
                if (1 == connectClient() )
                {
                    // connected. we'll hand around for it to send anything before
                    // we ask it something
                    tcpTimer = 2;
                    TCPControlState = TCP_FLUSH;
                }
                else
                {
                    TCPControlState = TCP_CONNECT_RETRY_WAIT;
                    tcpTimer = random(2,12); // random number of ticks before retrying
                }
            }
            else
            {
                TCPControlState = TCP_CONNECT_ERROR;
            }
            break;

        case TCP_CONNECT_ERROR :
            if (activeCommand == TCPC_SQ6_SCENE)
            {
                printTime(true);  Serial.printf(F("No SQ6 found\n\r"));
                stopClient();

            }
            else
            {
                // no projector. Lets stop sending status requests
                printTime(true);  Serial.printf(F("No projector found\n\r"));
                stopClient();
                projError = true;
                setLEDMode(LED_PROJ, LEDMODE_PATTERN_OFF);

            }
            TCPControlState = TCP_IDLE;
            break;

        case TCP_FLUSH:
            if ( --tcpTimer == 0 )
            {
                // connected, try and send the txBuffer
                #ifdef TCP_DEBUG
                printTime(true); Serial.printf(F("Sending buffer \n\r"));
                #endif

                writeToClient(txBuffer, txBufferLen );
                tcpTimer = random(200,1200)/UI_UPDATEmS; // 200-1200mS timeout
                TCPControlState = TCP_REPLY;
            }
            else if ( clientAvailable())
            {
                #ifdef TCP_DEBUG
                printTime(true);
                Serial.printf(F("got some rubbish, doing flush\n\r"));
                #endif
                clientRead(rxBuffer );

            }
            break;

        case TCP_REPLY :
            if ( --tcpTimer == 0 )
            {
                printTime(true);
                Serial.printf(F("No reply to message. timed out. Going to retry\n\r"));
                stopClient();
                TCPControlState = TCP_CONNECT;
            }
            else if ( clientAvailable())
            {
                #ifdef TCP_DEBUG
                printTime(true);
                Serial.printf(F("expected and got reply\n\r"));
                #endif
                // we have recieved a reply
                clientRead(rxBuffer );
                stopClient();
                parseReply();
                TCPControlState = TCP_IDLE;
            }
            break;



    }
}

void setSq6Scene(void)
{
    sq6SceneDelay = configStore.sq6_wait_scene * (1000/UI_UPDATEmS);
}

