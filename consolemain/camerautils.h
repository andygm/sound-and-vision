#ifndef __camerautils_h
#define  __camerautils_h
#include "globals.h"
#include "config.h"
/*

	Last change: AC 15/01/2019 21:56:45
*/

#define CAMERA_H_VER 6

typedef enum t_CameraGetData
{
    DATA_COMMAND_ISCONNECTED,
    DATA_COMMAND_GET_IRIS,
    DATA_COMMAND_GET_AUTO_IRIS,
    DATA_COMMAND_GET_AUTO_FOCUS,
};
extern int getCameraValue(t_CameraGetData command);


typedef enum t_CameraCommands
{
    // these used by queueCameraCommand()
    COMMAND_TILT,
    COMMAND_PAN,
    COMMAND_ZOOM,
    COMMAND_SET_IRIS,
    COMMAND_AUTOIRIS,
    COMMAND_FOCUS,
    COMMAND_AUTOFOCUS,
    COMMAND_ON_TOUCH_AUTOFOCUS,
    COMMAND_PRESET_STORE,

    // these are internal commands. Nomrally we do not directly ask
    // to use a preset as there is a sequence of commands to go through
    COMMAND_PRESET_USE,
};

extern void queueCameraCommand(t_CameraCommands command, int value);

extern void saveCameraPreset(ubyte presetNum);
extern void kickCameraTimer(void);
extern void selectNextCamera(void);
extern ubyte calculateCameraSpeed(uint16_t keyRepeats, t_SwitchConfigs switchSelection);
extern uint16_t calculateIrisPosition(uint16_t keyRepeats, bool directionOpen);
extern void loopCamera(void);
extern void cameraTick75ms(void);
extern void updateCameraLEDs(void);
extern void doCameraFocus(boolean focusIn, uint16_t keyRepeat);
extern void doCameraZoom(boolean out, uint16_t keyRepeat);
extern void storeCameraPresetSequence(int presetNum);


extern void toggleCameraDim(void);
extern boolean getCameraDim(void);
extern void cancelCameraDim(void);
extern void initCameras (void);
extern void stopFocusZoom(void);
extern ubyte getSelectedCamera(void);
extern void doWebZoom (int Value);
extern void doWebPan (int Value);
extern void doWebTilt (int Value);











#endif //  __camerautils_h
