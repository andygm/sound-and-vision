
#include "globals.h"
PROGMEM const char lastsave[] ="2024-08-05 20:45:56";
#define CONFIG_CPP_VER 5
#include <Streaming.h>

#include <ClientPanaAWHExTCP.h>

/*

	Last change: AC 12/04/2020 17:18:19
*/

#include <SkaarhojPgmspace.h>
#include "utilities.h"


#include <EEPROM.h>
#include <Wire.h>
#include "config.h"
#include "leddriver.h"



// the RAM copy of the config store. This shadows the EEPROM copy
t_configStore configStore;





#define MAX_NAME 28
struct confSt
{
    char name[MAX_NAME]; // max size of name
    ubyte type;
    int min;
    int max;
    void *ptr;
};


// some constanty values for names, min max limits etc. This must be in the same order as typedef enum t_ConfId
static const confSt allConf[] PROGMEM =
{
    /* CID_IP_ADDRESS_ATEM,    */ {"ATEM",(ubyte)CTYPE_IP,0,0, (void*)&configStore.IPConfigArray[0]},
    /* CID_IP_ADDRESS_CONSOLE, */ {"Console",(ubyte)CTYPE_IP,0,0,(void*)&configStore.IPConfigArray[1]},
    /* CID_IP_ADDRESS_CAMERA1, */ {"Camera 1",(ubyte)CTYPE_IP,0,0,(void*)&configStore.IPConfigArray[2]},
    /* CID_IP_ADDRESS_CAMERA2, */ {"Camera 2",(ubyte)CTYPE_IP,0,0,(void*)&configStore.IPConfigArray[3]},
    /* CID_IP_ADDRESS_PROJECTOR,*/ {"Projector",(ubyte)CTYPE_IP,0,0,(void*)&configStore.IPConfigArray[4]},
    /* CID_IP_ADDRESS_SQ6,      */ {"SQ6 Desk",(ubyte)CTYPE_IP,0,0,(void*)&configStore.IPConfigArray[5]},

    /* CID_ATEM_CAMERAS,        */ {"ATEM channels with cameras",(ubyte)CTYPE_STRING1,0,0,(void*)&configStore.cameraInput[0]},
    /* CID_JOYSTICK_DEAD_ZONE,  */ {"Joystick dead zone",(ubyte)CTYPE_PERCENT,1,15,(void*)&configStore.joyStickDeadzone},
    /* CID_SCREEN_DOWN_TIME,    */ {"Screen scroll down",(ubyte)CTYPE_TIMES,15,32,(void*)&configStore.screenDownTime},

    /* CID_IRIS_MIN_PERCENT,    */ {"IRIS min speed",(ubyte)CTYPE_PERCENT ,MIN_STARTSWITCHVALUEPERCENT,MAX_STARTSWITCHVALUEPERCENT,(void*)&configStore.switchConfig[SC_IRIS].minValuePercent},
    /* CID_IRIS_MIN_TIME,       */ {"IRIS time at min",(ubyte)CTYPE_TIMEMS,MIN_STARTSWITCHTIMEmS,MAX_STARTSWITCHTIMEmS,(void*)&configStore.switchConfig[SC_IRIS].minTimemS},
    /* CID_IRIS_RAMP_TIME,      */ {"IRIS ramp time",(ubyte)CTYPE_TIMEMS,MIN_SWITCHRAMPTIMEmS,MAX_SWITCHRAMPTIMEmS,(void*)&configStore.switchConfig[SC_IRIS].rampTimemS },
    /* CID_FOCUS_MIN_PERCENT,   */ {"FOCUS min speed",(ubyte)CTYPE_PERCENT,MIN_STARTSWITCHVALUEPERCENT,MAX_STARTSWITCHVALUEPERCENT,(void*)&configStore.switchConfig[SC_FOCUS].minValuePercent},
    /* CID_FOCUS_MIN_TIME,      */ {"FOCUS time at min",(ubyte)CTYPE_TIMEMS,MIN_STARTSWITCHTIMEmS,MAX_STARTSWITCHTIMEmS,(void*)&configStore.switchConfig[SC_FOCUS].minTimemS},
    /* CID_FOCUS_RAMP_TIME,     */ {"FOCUS ramp time",(ubyte)CTYPE_TIMEMS,MIN_SWITCHRAMPTIMEmS,MAX_SWITCHRAMPTIMEmS,(void*)&configStore.switchConfig[SC_FOCUS].rampTimemS },
    /* CID_ZOOM_MIN_PERCENT,    */ {"Not used",(ubyte)CTYPE_NOT_USED,MIN_STARTSWITCHVALUEPERCENT,MAX_STARTSWITCHVALUEPERCENT,(void*)&configStore.switchConfig[SC_ZOOM].minValuePercent},
    /* CID_ZOOM_MIN_TIME,       */ {"Not used",(ubyte)CTYPE_NOT_USED,MIN_STARTSWITCHTIMEmS,MAX_STARTSWITCHTIMEmS,(void*)&configStore.switchConfig[SC_ZOOM].minTimemS},
    /* CID_ZOOM_RAMP_TIME,      */ {"Not used",(ubyte)CTYPE_NOT_USED,MIN_SWITCHRAMPTIMEmS,MAX_SWITCHRAMPTIMEmS,(void*)&configStore.switchConfig[SC_ZOOM].rampTimemS },
    /* CID_SQ6_SCENE,           */ {"SQ6 startup scene",(ubyte)CTYPE_SCENE,1,99,(void*)&configStore.sq6_scene},
    /* CID_SQ6_WAIT_SCENE,      */ {"SQ6 wait startup",(ubyte)CTYPE_TIMES,3,20,(void*)&configStore.sq6_wait_scene},

};


// Checks an ATEM source index for validity
static boolean validateVideoSrcIndex(t_VideoIndex source)
{
    // is the value we got a valid setting?
    //apc implement this!
    return true;
}

typedef union confValue{
    int value;
    byte shortValue;
    uint32_t IPAdd;
    char s[MAX_PRESET_NAME_LEN];
};


static t_EEPROMADD getPresetEEPROMAddress(t_ConfId item)
{
    /*
    t_EEPROMADD returnVal =  EEPROM_PRESETS_START_ADDRESS + ((item-CID_FIRST_PR_NAME)  * MAX_PRESET_NAME_LEN);
    // Serial.printf("Get adderss %d = %d\n", item,returnVal);
    return returnVal;
    */
    return ( EEPROM_PRESETS_START_ADDRESS + ((item-CID_FIRST_PR_NAME)  * MAX_PRESET_NAME_LEN) );
}

static void writeConfigValue(confValue inBuff, t_ConfId item)
{
    ubyte type = getConfType(0, item);
    ubyte *ptr = pgm_read_word(&allConf[item].ptr);

    switch (type)
    {
        case CTYPE_PRESET_TEXT:
            {
                t_EEPROMADD destptr = getPresetEEPROMAddress(item);
                unsigned int ctr = 0;
                char *srcPtr;
                srcPtr = inBuff.s;
                do
                {
                    EEPROM.update(destptr++, *srcPtr);
                }while (*srcPtr++ != '\0');
            }
            break;

        case CTYPE_IP:
            *(IPAddress*)ptr = inBuff.IPAdd;
            break;
        case CTYPE_TIMEMS:
            *(int*)ptr = inBuff.value;
            break;
        case CTYPE_SCENE:
        case CTYPE_TIMES:
        case CTYPE_PERCENT:
            *(ubyte*)ptr = inBuff.shortValue;
            break;
        case CTYPE_STRING1:
            // ptr is to the first item in an array of booleans
            {
                ubyte offset = 0;
                boolean *dest;
                dest = (boolean*)ptr;
                char ch;
                do
                {
                    ch = toupper(inBuff.s[offset]);
                    *dest++ = (ch == 'Y' ||  ch == '1')?true:false;
    
                }while (++offset < MAX_ATEM_INPUTS);
            }
            break;
        default :
            break;
    }
}

static void defaultConfigValue(t_ConfId item)
{
    ubyte type = getConfType(0, item);
    union  confValue newConfVal;
    int min = getConfMin(item);
    int max = getConfMax(item);
    switch (type)
    {
        case CTYPE_PRESET_TEXT:
            // this type does not have a ram shadow so create and write straight to EEPROM
            if (item < CID_CAM1_PR1)
            {
                sprintf_P(newConfVal.s, PSTR("ATEM Preset %d"),item - CID_FIRST_PR_NAME +1);
            }
            else if (item < CID_CAM2_PR1)
            {
                sprintf_P(newConfVal.s, PSTR("Camera1: Preset %d"),item - CID_CAM1_PR1 +1);
            }
            else
            {
                sprintf_P(newConfVal.s, PSTR("Camera2: Preset %d"),item - CID_CAM2_PR1 +1);
            }
            t_EEPROMADD destptr = getPresetEEPROMAddress(item);
            writeConfigValue(newConfVal, item);
            break;
        case CTYPE_IP:
            {
                IPAddress defAddress(192, 168, 0, 1);
                newConfVal.IPAdd = defAddress;
                writeConfigValue(newConfVal, item);

            }
            break;
        case CTYPE_TIMEMS:
            newConfVal.value = (min+max)/2;
            writeConfigValue(newConfVal, item);
            break;
        case CTYPE_SCENE:
            newConfVal.shortValue = 0;
            writeConfigValue(newConfVal, item);
            break;
        case CTYPE_TIMES:
        case CTYPE_PERCENT:
            newConfVal.shortValue = (ubyte)((min+max)/2);
            writeConfigValue(newConfVal, item);
            break;
        case CTYPE_STRING1:
            for( ubyte index = 0; index < MAX_ATEM_INPUTS; index++)
            {
                configStore.cameraInput[index] = false;
            }
            break;
        default :
            break;
    }

}


boolean checkAndSaveConfString(char *inBuff, t_ConfId item, boolean saveIt= false)
{
//    Serial.printf(F("CASC %d: %s\n"),item,inBuff);
    ubyte type = getConfType(0, item);
    union  confValue newConfVal;
    boolean valOk = false;
    int min = getConfMin(item);
    int max = getConfMax(item);
    int value;
    getFirstVal(inBuff, &value);
    switch (type)
    {
        case CTYPE_PRESET_TEXT :
            {
                int length = strlen(inBuff);
                valOk = (length > 0 && length <MAX_PRESET_NAME_LEN-2);
                if (valOk && saveIt)
                {
                    // its valid
                    if (length < 3)
                    {
                        defaultConfigValue(item);
                    }
                    else
                    {
                        strcpy(&newConfVal.s[0],inBuff);
                        writeConfigValue(newConfVal, item);
                    }
                }
            }
            break;
        case CTYPE_NOT_USED :
            // don't care what this type is or its value
            valOk = true;
            break;

        case CTYPE_IP:
            {
                IPAddress newAddress;
                valOk = isIPAddressValid(inBuff, &newAddress);
                newConfVal.IPAdd = newAddress;
                if (saveIt && valOk)
                {
                    writeConfigValue(newConfVal, item);
                }
            }
            break;
        case CTYPE_TIMEMS:
            // valid value is any number between min and max
            valOk = (value >= min && value <= max)?true:false;
            
            if (valOk && saveIt)
            {
                newConfVal.value = value;
                writeConfigValue(newConfVal, item);
            }
            break;
        case CTYPE_SCENE:
        case CTYPE_TIMES:
        case CTYPE_PERCENT:
            // valid value is any number between min and max
            valOk = (value >= min && value <= max)?true:false;
            if (valOk && saveIt)
            {
                newConfVal.shortValue = value;
                writeConfigValue(newConfVal, item);
            }
            break;
        case CTYPE_STRING1:
            {
                ubyte length = strlen(inBuff);
                if (length > 2)
                {
                    valOk = true;
                    char ch;
                    for ( ubyte index = 0; index < MAX_ATEM_INPUTS ; index++ )
                    {
                        ch= (length <= index)?'N':toupper(*inBuff++);
                        newConfVal.s[index] = ch;
                        newConfVal.s[index+1] = '\0';
    
                        if (ch != 'Y' && ch != 'N' &&ch != '1' &&ch != '0')
                        {
                            valOk = false;
                            break;
                        }
                    }
                    if (valOk && saveIt)
                    {
                        writeConfigValue(newConfVal, item);
                    }
                }


            }
            break;
        default :
            valOk = false;
            Serial.printf(F("ERR. Type %d not handled\n"),type);
            *inBuff = '\0';
            break;

    }
    if (valOk && saveIt)
    {
//        Serial.printf("Save store item %d\n",item);
        saveConfigStoretoEEPROM();
    }
    return valOk;
}




// Checks and corrects the local RAM config store
static boolean validateConfigStore(void)
{
    ubyte index;
    boolean validStore = true;

    ubyte item = 0;
    char nameBuffer[MAX_NAME];
    char valBuffer[MAX_PRESET_NAME_LEN];
    char typestr[30];
    ubyte type;
    do
    {
        //type = getConfType(typestr,item);
        getConfValueStr(valBuffer,item);
        if (!checkAndSaveConfString(valBuffer, item, false))
        {
            getConfName(nameBuffer,item);
            Serial.printf(F("Item %d; %s "),item,nameBuffer);
            Serial.printf(F("value %s. This is invalid.\n"), valBuffer);
            validStore = false;
            defaultConfigValue(item);
            getConfValueStr(valBuffer,item);
            Serial.printf(F("Defaulted it to %s.\n\n"),valBuffer);

        }
    }while (++item < CID_LAST_ITEM);

    for ( index = 0 ; index < MAX_ATEM_PRESETS; index++ )
    {
        if ( !validateVideoSrcIndex(configStore.ATEMPresetStore[index].progIndex) )
        {
            Serial.printf(F("Error in ATEM preset %d prog index\n\r"),index);
            configStore.ATEMPresetStore[index].progIndex = 21; //colourbars
            validStore = false;
        }
        if ( !validateVideoSrcIndex(configStore.ATEMPresetStore[index].auxIndex) )
        {
            Serial.printf(F("Error in ATEM preset %d aux index\n\r"),index);
            configStore.ATEMPresetStore[index].progIndex = 21; //colourbars
            validStore = false;
        }
        if (configStore.ATEMPresetStore[index].dsk1enabled > 1)
        {
            Serial.printf(F("Error in ATEM preset %d dsk1\n\r"),index);
            configStore.ATEMPresetStore[index].dsk1enabled = false;
            validStore = false;
        }
        if (configStore.ATEMPresetStore[index].dsk2enabled > 1)
        {
            Serial.printf(F("Error in ATEM preset %d dsk2\n\r"),index);
            configStore.ATEMPresetStore[index].dsk2enabled = false;
            validStore = false;
        }
        if (configStore.ATEMPresetStore[index].usk1enabled > 1)
        {
            Serial.printf(F("Error in ATEM preset %d Usk1\n\r"),index);
            configStore.ATEMPresetStore[index].usk1enabled = false;
            validStore = false;
        }
    }
    for ( index = 0 ; index < SC_MAX_SWITCH_CONFIGS; index++ )
    {
        int value = configStore.switchConfig[index].minValuePercent;
        if ( value < MIN_STARTSWITCHVALUEPERCENT || value > MAX_STARTSWITCHVALUEPERCENT )
        {
            Serial.printf(F("Error in switch config %d min percent. value is %d\n\r"),index,value);
            configStore.switchConfig[index].minValuePercent = MIN_STARTSWITCHVALUEPERCENT;
            validStore = false;
        }
        value = configStore.switchConfig[index].minTimemS;
        if ( value < MIN_STARTSWITCHTIMEmS || value > MAX_STARTSWITCHTIMEmS )
        {
            Serial.printf(F("Error in switch config %d min time Value is %d\n\r"),index,value);
            configStore.switchConfig[index].minTimemS = MIN_STARTSWITCHTIMEmS;
            validStore = false;
        }
        value = configStore.switchConfig[index].rampTimemS;
        if ( value < MIN_SWITCHRAMPTIMEmS || value > MAX_SWITCHRAMPTIMEmS )
        {
            Serial.printf(F("Error in switch config %d ramp time. Value is %d\n\r"),index,value);
            configStore.switchConfig[index].rampTimemS = MIN_SWITCHRAMPTIMEmS;
            validStore = false;
        }
    }


    // now check the camera config stores

    for ( index = 0; index < MAX_CAMERAS ; index++ )
    {
        for ( ubyte presetNum = 0; presetNum < MAX_CAMERA_PRESETS ; presetNum++ )
        {
            if (
                ( configStore.cameraPreset[presetNum][index].iris > 1023 ) ||
                ( configStore.cameraPreset[presetNum][index].autoFocus > 1 ) ||
                ( configStore.cameraPreset[presetNum][index].autoIris > 1 )
               )
            {
                configStore.cameraPreset[presetNum][index].iris = 512;
                configStore.cameraPreset[presetNum][index].autoFocus = 0;
                configStore.cameraPreset[presetNum][index].autoIris = 0;
                Serial.printf(F("Error in Camera %d, preset %d. Set to defaults,\n\r"),index,presetNum);
                validStore = false;
            }
        }

    }
    // now check the cameraInput type stores

    for ( index = 0; index < MAX_ATEM_INPUTS ; index++ )
    {
        if (configStore.cameraInput[index] != true && configStore.cameraInput[index] != false)
        {
            configStore.cameraInput[index] = false; // false means not a camera
            Serial.printf(F("Error in CameraType for atem Input %d,Set to defaults,\n\r"),index+1);
            validStore = false;
        }
    }
    return validStore;
}


void saveConfigStoretoEEPROM(void)
{
    t_EEPROMADD destptr = EEPROM_START_ADDRESS;
    validateConfigStore();  // correct any errors in local ram store
    unsigned int ctr = 0;
    byte *src;
    src = (byte*)&configStore;
    do
    {
        EEPROM.update(destptr++, *src++);
    }
    while ( ++ctr < sizeof(configStore) );
}

void getConfigStoreFromEEPROM(void)
{
    t_EEPROMADD srcptr = EEPROM_START_ADDRESS;
    unsigned int ctr = 0;
    byte *dest;
    dest = (byte*)&configStore;
    do
    {
        *dest = EEPROM.read(srcptr++);
        dest++;
    }
    while ( ++ctr < sizeof(configStore) );
    // correct any errors and resave changes to EEPROM
    // apc debug

    validateConfigStore();  // correct any errors in local ram store
    saveConfigStoretoEEPROM();
}


void printIPAddress(t_ConfId item)
{
    t_ConfTypes type = getConfType(0, item);
    if (type == CTYPE_IP)
    {
        char buffer[MAX_NAME];
        getConfName(buffer, item);
        Serial.printf(F("%s address is : "),buffer);
        getConfValueStr(buffer, item);
        Serial.printf(F("%s\n"),buffer);
    }
}


boolean isIPAddressValid(char* inputBuffer, IPAddress *newAddress)
{
    int value;
    boolean result = true;
    char *ptr;
    uint32_t calc = 0;
    // find first numeric
    value = 0;
    while ( *inputBuffer > '9' || *inputBuffer < '0' )
    {
        ++inputBuffer;
        if ( ++value > 3 )
        {
            // didn't find a 0-9 in the first 3 characters
            result = false;
            break;
        }
    }
    if ( result == true )
    {
        if ( strlen(inputBuffer) < 8 ||  strlen(inputBuffer) > 16 )
        {
            result = false;
        }
        else
        {

            ubyte arrayindex = 0;
            do
            {
                inputBuffer = getFirstVal(inputBuffer, &value) +1;
                
                if ((arrayindex == 0 || arrayindex == 3)  && value == 0)
                {
                    result = false;
                    break;
                }
                if ( value < 0 || value > 254 )
                {
                    result = false;
                    break;
                }
                calc = (calc >> 8L) + (value * 256L * 256L * 256L);
    
            }
            while ( ++arrayindex < 4 );
        }
        if (newAddress != NULL)*newAddress = calc;
        //Serial.printf(F("Calc 0x%08lx\n"),calc);
    }
    return result;
}





 
boolean isUsableIpAddress(IPAddress testAddress) //apczzz to go
{
    unsigned long address = testAddress;
    boolean valid = true;
    if ( address == 0L )
    {
        valid = false;
    }
    ubyte ctr = 0;
    do
    {
        if (  (address & 0xFFL) == 0xFFL )
        {
            valid = false;
        }
        address >>= 8;
    }
    while ( ++ctr < 4 && valid == true);
    return valid;
}

static void getAtemName(char* buffer,ubyte index)
{
    switch (index)
    {

        case 0:  // Black
            sprintf_P(buffer,PSTR("Black"));
            break;
        case 1:  // Input 1
        case 2:  // Input 2
        case 3:  // Input 3
        case 4:  // Input 4
            buffer += sprintf_P(buffer,PSTR("HDMI Input %d"),index);
            if (index == 1)
            {
                sprintf_P(buffer,PSTR(": PA Desk laptop main input"));
            }
            else if (index == 2)
            {
                sprintf_P(buffer,PSTR(": PA Desk laptop AUX(stage) input"));
            }
            break;
        case 5:  // Input 5
        case 6:  // Input 6
        case 7:  // Input 7
        case 8:  // Input 8
            buffer += sprintf_P(buffer,PSTR("SDI Input %d"),index-4);
            if (index == 5)
            {
                sprintf_P(buffer,PSTR(": Stage laptop input"));
            }
            else if (index == 6)
            {
                sprintf_P(buffer,PSTR(": PTZ camera"));
            }
            else
            {
                sprintf_P(buffer,PSTR(": SDI Wall Socket %d"),index);
            }
            break;

        case 21:  // Color Bars
            sprintf_P(buffer,PSTR("Colour bars"));
            break;
        case 22:  // Solid colour 1
        case 23:  // Solid colour 2
            sprintf_P(buffer,PSTR("Solid colour %d"),index-21);
            break;
        case 24:  // Media player 1
        case 25:  // Media player 2
            sprintf_P(buffer,PSTR("ATEM Media Player %d"),index-23);
            break;
        case 43:  // as per program sourceMedia player 2
            sprintf_P(buffer,PSTR("As per program source"));
            break;

        default :
            sprintf_P(buffer,PSTR("Unknown source %d"),index);
            break;
    }
}
extern void dumpKeys(void);

void printConfig(void)
{
    printRow('+', 60, true);

    #define BUFFSIZE 50
    ubyte item = 0;
    char buffer[BUFFSIZE];
 #if (MAX_NAME >= BUFFSIZE)
    #error Local buffer not big enough for MAX_NAME
#endif
#if (MAX_PRESET_NAME_LEN >= BUFFSIZE)
    #error Local buffer not big enough for MAX_PRESET_NAME_LEN
#endif

    ubyte type;
    Serial.printf(F("Main config items list:-\n\r"));
    do
    {
        //getConfInfo(&myStruct,  item);
        //Serial.printf(F("Item %d name %s type %d min %d max %d typ %s\n"),item, myStruct.name, myStruct.type, myStruct.min, myStruct.max,myStruct.typeStr);
        type = getConfType(0,item); // get thetype as a number

        getConfName(buffer,item);
        Serial.printf(F("%d: %s "),item,buffer);
        getConfType(buffer,item); // ett hev type as a string
        Serial.printf(F("%s\n"),buffer);
        getConfValueStr(buffer,item);
        Serial.printf(F("\tValue is: %s\n"),buffer);

        // now print any other info peculiar to the type
        switch (type)
        {
            case CTYPE_PRESET_TEXT:
                Serial.printf(F("\tOther info:\n"));
                if (item >= CID_CAM1_PR1)
                {
                    uint8_t cameraNum;
                    uint8_t presetNum;
                    if (item >= CID_CAM2_PR1)
                    {
                        presetNum = item-CID_CAM2_PR1;
                        cameraNum = 2;
                    }
                    else
                    {
                        presetNum = item-CID_CAM1_PR1;
                        cameraNum = 1;
                    }
                    Serial.printf(F("\t  AutoIris %d\n"),configStore.cameraPreset[presetNum][cameraNum -1].autoIris);
                    Serial.printf(F("\t  AutoFocus %d\n"),configStore.cameraPreset[presetNum][cameraNum -1].autoFocus);
                    Serial.printf(F("\t  Iris value %d\n"),configStore.cameraPreset[presetNum][cameraNum -1].iris);

                    if (item >= CID_CAM2_PR1)
                    {
                    Serial.printf(F("\tCAM2\n"));
                    }
                    else if (item >= CID_CAM1_PR1)
                    {
                        Serial.printf(F("\tCAM1\n"));
                    }
                }
                else
                {
                    // assume its an atem preset
                    uint8_t ref = item -CID_FIRST_PR_NAME;
                    getAtemName(buffer,configStore.ATEMPresetStore[ref].progIndex);
                    Serial.printf(F("\t  Program source %s\n"),buffer);
                    getAtemName(buffer,configStore.ATEMPresetStore[ref].auxIndex);
                    Serial.printf(F("\t  Aux(stage) source %s\n"),buffer);
                    Serial.printf(F("\t  Downstream Key 1 %s\n"),configStore.ATEMPresetStore[ref].dsk1enabled?"On":"Off");
                    Serial.printf(F("\t  Downstream Key 2 %s\n"),configStore.ATEMPresetStore[ref].dsk2enabled?"On":"Off");
                    Serial.printf(F("\t  Upstream Key 1 %s\n"),configStore.ATEMPresetStore[ref].usk1enabled?"On":"Off");
                }
                break;
            case CTYPE_TIMEMS:
            case CTYPE_TIMES:
            case CTYPE_SCENE:
            case CTYPE_PERCENT:
                Serial.printf(F("\tMin/max values: %d/%d\n"),getConfMin(item),getConfMax(item));
                break;
            case CTYPE_IP:
            default :
                break;
        
        }

    }while (++item < (ubyte)CID_LAST_ITEM);
    dumpKeys();

}

void initConfig(void)
{
    printModuleStartup(PSTR("Config Module"),CONFIG_CPP_VER,CONFIG_H_VER,lastsave);

    Serial.printf(F("\tReading config from EEPROM\n\r"));
    getConfigStoreFromEEPROM();
    Serial.printf(F("\tRecovered EEPROM config. Used %d bytes of %d space\n\r"), sizeof(configStore), EEPROM.length());
    Serial.printf(F("\tPreset names start at EEPROM offset %d and use %d bytes\n"),EEPROM_PRESETS_START_ADDRESS, MAX_PRESET_NAME_LEN*NUMBER_OF_PRESETS);
    Serial.printf(F("\tSpace left in EEPROM is %d bytes\n"),EEPROM.length() - (EEPROM_PRESETS_START_ADDRESS +(MAX_PRESET_NAME_LEN*NUMBER_OF_PRESETS)));

    printNewLine();

}





// A family of functions to get confif info in printable form
ubyte getConfName(char *dest,t_ConfId item)
{
    ubyte retval = 0;   
    if (item < CID_FIRST_PR_NAME)
    {
        retval = sprintf_P(dest, allConf[item].name);
    }
    else
    {
        if (item < CID_CAM1_PR1)
        {
            retval = sprintf_P(dest, PSTR("ATEM Preset %d"),item - CID_FIRST_PR_NAME +1);
        }
        else if (item < CID_CAM2_PR1)
        {
            retval = sprintf_P(dest, PSTR("Camera 1: Preset %d"),item - CID_CAM1_PR1 +1);
        }
        else
        {
            retval = sprintf_P(dest, PSTR("Camera 2: Preset %d"),item - CID_CAM2_PR1 +1);
        }

    }
    return retval;
}
int getConfMax(t_ConfId item)
{
    if (item < CID_FIRST_PR_NAME)
    {
        return pgm_read_word_near(&allConf[item].max);
    }
    else
    {
        return 0;
    }
}
int getConfMin(t_ConfId item)
{
    if (item < CID_FIRST_PR_NAME)
    {
        return pgm_read_word_near(&allConf[item].min);
    }
    else
    {
        return 0;
    }
}

/*
    If dest ptr is valid, it fills the buffer in with a strinb and returns the no of characters
    printed. If dest ptr is null it returns the type
*/

ubyte getConfType(char *dest,t_ConfId item)
{
    ubyte retval;
    if (item >= CID_FIRST_PR_NAME)
    {
        item = item - CID_FIRST_PR_NAME;
        // it should now be  an index into the array of preset names stored only in EEPROM
        if (item < NUMBER_OF_PRESETS)
        {
            retval = CTYPE_PRESET_TEXT;
        }
        else
        {
            retval = CTYPE_NOT_USED;
        }

    }
    else
    {
        retval = pgm_read_byte_near(&allConf[item].type);
    }

    if (dest != NULL)
    {
        switch (retval)
        {
            case CTYPE_PRESET_TEXT:
                retval = sprintf_P(dest, PSTR("Preset name"));
                break;
            case CTYPE_IP:
                retval = sprintf_P(dest, PSTR("IP address"));
                break;
            case CTYPE_TIMEMS:
                retval = sprintf_P(dest, PSTR("time in mS"));
                break;
            case CTYPE_TIMES:
                retval = sprintf_P(dest, PSTR("time in Secs"));
                break;
            case CTYPE_SCENE:
                retval = sprintf_P(dest, PSTR("Scene No"));
                break;

            case CTYPE_PERCENT:
                retval = sprintf_P(dest, PSTR("Percentage"));
                break;
            default :
                retval = sprintf_P(dest, PSTR("string value"));
                break;
    
        }

    }
    return retval;
}


/* Get the value as a string
*/


// returns no of chars printed
int getConfValueStr(char *dest,t_ConfId item)
{
    void *ptr = pgm_read_word_near(&allConf[item].ptr);
    t_ConfTypes type = getConfType(0, item);
//    Serial.printf("Item %d Ptr val %0x type %d\n",item,ptr,type);
    int charsPrinted = 0;
    switch (type)
    {
        case CTYPE_PRESET_TEXT:
            {
              t_EEPROMADD srcptr = getPresetEEPROMAddress(item);
//            charsPrinted = sprintf_P(dest,PSTR("andy item %d ere"),item);

            ubyte ctr = MAX_PRESET_NAME_LEN -1;
            do
            {
                *dest = EEPROM.read(srcptr++);
                if (*dest == '\0')break;
                ++charsPrinted;
                dest++;

            }while (--ctr != 0);
            *dest = '\0';
      

            }
            break;
        case CTYPE_IP:
            {
                IPAddress IPAdd = *(IPAddress*)ptr;
                charsPrinted = sprintf_P(dest, PSTR("%d.%d.%d.%d"), IPAdd[0], IPAdd[1], IPAdd[2], IPAdd[3]);
            }
            break;

        case CTYPE_TIMEMS:
            charsPrinted = sprintf_P(dest, PSTR("%dmS"), *(int*)ptr);
            break;
        case CTYPE_SCENE:
            charsPrinted = sprintf_P(dest, PSTR("%d"), *(ubyte*)ptr);
            break;
        case CTYPE_TIMES:
            charsPrinted = sprintf_P(dest, PSTR("%dS"), *(ubyte*)ptr);
            break;
        case CTYPE_PERCENT:
            charsPrinted = sprintf_P(dest, PSTR("%d%%"), *(ubyte*)ptr);
            break;
        
        case CTYPE_STRING1:
            // assume its the list of ATEM channels with cameras present
            for ( byte index = 0; index < MAX_ATEM_INPUTS; index++ )
            {
                *dest++ =(  configStore.cameraInput[index] == true)?'Y':'n';
                charsPrinted++;
            }
            *dest = '\0';
            break;
        case CTYPE_NOT_USED :
            charsPrinted = sprintf_P(dest, PSTR("Not Used"));
            break;
        default :
            charsPrinted = sprintf_P(dest, PSTR("Not handled"));
            Serial.printf(F("Error default not handled\n"));
            break;

    }
    return charsPrinted;
}


void enableSavePresetMode(boolean active)
{
    uint8_t mode;
    int time;
    if (active)
    {
        mode = LEDMODE_PATTERN_25PERCENT | LEDMODE_SPEED_FAST;
        time = 4000/UI_UPDATEmS;
    }
    else
    {
        mode = LEDMODE_PATTERN_OFF;
        time = 0;

    }
    setLEDMode(LED_MISC1,mode,time);
    setLEDMode(LED_CAMERA1,mode,time);
    setLEDMode(LED_CAMERA2,mode,time);
        
    uint8_t led =LED_ATEM_PRESET1;  
    do
    {
        setLEDMode(led,mode,time);

    }while (++led <= LED_ATEM_PRESET8);
}



