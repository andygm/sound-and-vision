#ifndef __tcpcontrol_h
#define __tcpcontrol_h
/** @package

    tcpcontrol.h

    Thsi module is responsible for handling the projector on off, status and also scene changes to the SQ6 mixing console

    Copyright(c)  2000
    
    Author: ANDREW COPSEY
    Created: AC  29/12/2018 17:37:08
	Last change: AC 02/04/2020 10:11:42
*/

#define TCPCONTROL_H_VER 3


extern void setSq6Scene(void);
extern void toggleProjectorOnOff(void);
extern void TCPTick(void);
extern void initTCPControl(void);
#endif// __projectorcontrol_h


