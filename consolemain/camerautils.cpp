#include "globals.h"
PROGMEM const char lastsave[] ="2024-08-05 16:32:03";
#define CAMERA_CPP_VER 6
/*

	Last change: AC 12/04/2020 17:47:32
*/

#include <SkaarhojPgmspace.h>


#include <EEPROM.h>
#include <Wire.h>
#include "config.h"
#include "camerautils.h"
#include "atemutils.h"
#include "utilities.h"
#include "leddriver.h"
#include "buttondriver.h"
#include <Ethernet.h>




// some times needed whwen sending thinsg to the camera. Eg - after applyoing use
//preset commadn we need to wait before doing anything elseas sendinga new command
// causes the camera to forget it is moving
#define WAIT_FOR_AUTO_IRISmS   250
#define WAIT_FOR_AUTO_FOCUSmS   250
#define WAIT_FOR_IRISmS   250
#define WAIT_FOR_MOVEmS   850
#define IRIS_DIMMED_LEVEL 2   // 1-1023 1 is dim

static boolean dimCamera = false;

// We work on 1 of n cameras. This variable controls which one is selected
static ubyte selectedCamera;

// This variable controls the state of initialisation of each camera as we
// switch to it to control it
static ubyte cameraInited = 0;

#include <ClientPanaAWHExTCP.h>
ClientPanaAWHExTCP cameraObj;

#include "SkaarhojUtils.h"
extern SkaarhojUtils utils;

// Some timers that prevent us continuously trying to contact devices that aren't connected
// This avoids the loop functions being delayed whilst ethernet retries are performed
static ubyte cameraTimer;


// Tell the UI (LEDS) that we are doing something with a camera
static void indicateCameraActivity(void)
{
    setLEDMode(LED_CAMERA_OK, LEDMODE_PATTERN_5050, 10);
}
//#define MAINDEBUG
#ifdef MAINDEBUG
const static char *cameraCommandNames[] =
{
    "Tilt",
    "Pan",
    "Zoom",
    "Set Iris",
    "Set Auto Iris",
    "Focus",
    "Auto focus",
    "OT Focus",
    "Store preset",
    "Apply preset",
};
#endif


// define a FIFO queue for camera commands, Q&D design
#define MAX_QUEUE 16
static int cameraFIFO[MAX_QUEUE];
static ubyte queueIn = 0;
static ubyte queueOut = 0;
// Compress each element to be 5 bits camera command and 11 bits value


void queueCameraCommand(t_CameraCommands command, int value)
{
    /*
    if (command == COMMAND_PAN)
    {
        printTime(true);Serial.printf(F("Pan %d\n", value));
    }
    if (command == COMMAND_TILT)
    {
        printTime(true);Serial.printf(F("Tilt %d\n", value));
    }
    */
    #ifndef MAINDEBUG
    if (getVisualsPowerState())
    #endif
    {
        if ( (ubyte)((ubyte)(queueOut- queueIn) != 1) )
        {
            // there is space
            cameraFIFO[queueIn] = ((ubyte)command & 0x1F) + (value << 5);
            #ifdef MAINDEBUG
           printTime(true);
            Serial.printf(F("QAdd  %d %s V %d. \n\r"),queueIn,cameraCommandNames[command], value);
            #endif
            if ( ++queueIn >= MAX_QUEUE)
            {
                queueIn = 0;
            }
        }
        else
        {
            Serial.printf(F("ERROR Q Overflow\n\r"));
        }
    }
}


// tries to get something from queue returns true if succeeded
static boolean getFromFIFO(t_CameraCommands *command, int *value)
{
    boolean returnVal = false;
//    if ( queueIn != queueOut  && command != NULL && value != NULL)
    if ( queueIn != queueOut )
    {
        // theres something in the queue and we can return it
        *command = (t_CameraCommands)(cameraFIFO[queueOut] & 0x1F);
        *value = cameraFIFO[queueOut] >> 5;
        #ifdef MAINDEBUG
        printTime(true);
        Serial.printf(F(" Get from cam queue %d com %s val %d\n\r"),queueOut,cameraCommandNames[*command], *value);
        #endif
        if ( ++queueOut >= MAX_QUEUE)
        {
            queueOut = 0;
        }
        returnVal = true;
    }
    return returnVal;
}


/* ================================================================
Some functionality to handle the complex sequence when we activate a camera preset
or other actions that need timed sequences. This is to avoid confusing the camera.
EWG tell it to move to a preset and then send a focus command too quickly it forgets
it is moveing and doesn't do the focus either
*/

typedef enum t_PresetSequencecommands
{
    PS_SET_IRIS,  // set the iris position and then wait the defined delay
    PS_APPLY_PRESET, // set the camera moving and then wait the defined delay
    PS_RESTORE_AUTO_FOCUS, // put the autofocus value in and then wait the defined delay
    PS_RESTORE_AUTO_IRIS, // put the autoiris value in and then wait the defined delay
    PS_WAIT,  // just wait - otherwise do nothing
};

// define a sequenceFIFO queue for camera commands, Q&D design
#define MAX_SEQ_QUEUE 12
static int seqFIFO[MAX_SEQ_QUEUE];
static ubyte seqIn = 0;
static ubyte seqOut = 0;

static ubyte tickCtr;  // counts time in units of UI_UPDATEmS


// Compress each element to be 5 bits command and 11 bits value

void queueSequenceCommand(t_PresetSequencecommands command, int value)
{
    if ( (ubyte)((ubyte)(seqOut- seqIn) != 1) )
    {
        // there is space
        seqFIFO[seqIn] = ((ubyte)command & 0x1F) + (value << 5);
        #ifdef MAINDEBUG
       printTime(true);
        Serial.printf(F("SeqQAdd  %d %s V %d. \n\r"),seqIn,cameraCommandNames[command], value);
        #endif
        if ( ++seqIn >= MAX_SEQ_QUEUE)
        {
            seqIn = 0;
        }
    }
    else
    {
        Serial.printf(F("ERROR SeqQ Overflow\n\r"));
    }
}


// tries to get something from queue returns true if succeeded
static boolean getFromSeqFIFO(t_PresetSequencecommands *command, int *value)
{
    boolean returnVal = false;
    if ( seqIn != seqOut )
    {
        // theres something in the queue and we can return it
        *command = (t_PresetSequencecommands)(seqFIFO[seqOut] & 0x1F);
        *value = seqFIFO[seqOut] >> 5;
        #ifdef MAINDEBUG
        printTime(true);
        Serial.printf(F(" Get from seqFifo %d com %s val %d\n\r"),queueOut,cameraCommandNames[*command], *value);
        #endif
        if ( ++seqOut >= MAX_SEQ_QUEUE)
        {
            seqOut = 0;
        }
        returnVal = true;
    }
    return returnVal;
}

static ubyte spaceInSeqQueue(void)
{
    return ( seqOut > seqIn )?(seqOut - seqIn -1):(MAX_SEQ_QUEUE-1 + seqOut - seqIn);
}



void storeCameraPresetSequence(int presetNum)
{
    #ifdef OLD_NON_FTBMETHOD
    
    dimCamera = false;
    // is there enough space in the queue
    if ( spaceInSeqQueue() > 6 )
    {
        // turn off auto iris if on first
        if ( getCameraValue(DATA_COMMAND_GET_AUTO_IRIS) == true )
        {
            queueCameraCommand(COMMAND_AUTOIRIS, 0);
            // and wait
            tickCtr = WAIT_FOR_AUTO_IRISmS/UI_UPDATEmS;
        }
        queueSequenceCommand(PS_SET_IRIS, IRIS_DIMMED_LEVEL); // dim iris
        queueSequenceCommand(PS_APPLY_PRESET, presetNum);
        queueSequenceCommand(PS_SET_IRIS, configStore.cameraPreset[presetNum][selectedCamera].iris); // put new iris
        queueSequenceCommand(PS_RESTORE_AUTO_FOCUS,configStore.cameraPreset[presetNum][selectedCamera].autoFocus);
        queueSequenceCommand(PS_RESTORE_AUTO_IRIS,configStore.cameraPreset[presetNum][selectedCamera].autoIris);
    }
    else
    {
        // the user will be aware because no led flashes occur if no commands are sent
        Serial.printf(F("ERROR SeqQ No space\n\r"));
    }


    #else
    
    startStoredFTB();
    queueCameraCommand(COMMAND_PRESET_USE, presetNum);
    #endif
}


// called every UI_UPDATEmS
static void handleCameraPresetSequence(void)
{
    if ( tickCtr > 0 )
    {
        --tickCtr;
    }
    else
    {
        // anything in the queue?
        int value;
        t_PresetSequencecommands command;
        if ( getFromSeqFIFO(&command, &value) == true)
        {
            switch ( command )
            {
                case PS_SET_IRIS:
                    queueCameraCommand(COMMAND_SET_IRIS, value);
                    //apczzztickCtr = WAIT_FOR_IRISmS/UI_UPDATEmS;
                    break;
                case PS_APPLY_PRESET:
                    queueCameraCommand(COMMAND_PRESET_USE, value);
                    tickCtr = WAIT_FOR_MOVEmS/UI_UPDATEmS;
                    break;
                case PS_RESTORE_AUTO_FOCUS:
                    queueCameraCommand(COMMAND_AUTOFOCUS, value);
                    tickCtr = WAIT_FOR_AUTO_FOCUSmS/UI_UPDATEmS;
                    break;
                case PS_RESTORE_AUTO_IRIS:
                    queueCameraCommand(COMMAND_AUTOIRIS, value);
                    //apczzztickCtr = WAIT_FOR_AUTO_IRISmS/UI_UPDATEmS;
                    break;

                default :
                    // treat as wait
                    if ( value > 20 )
                    {
                        value = 20; // keep it reaosnable - say 1.5 secodns
                    }
                    tickCtr = value;;
                    break;
            }
        }
    }
}

/*========================================================================
Some functionality to dim the seleected camera using the MISC1 button. The function
toggles on and off with each press of the button.
Any other camera action such as change of presets or touch the iris controls
 cancels the dimmed value
*/

static int lastIrisPosition;

boolean getCameraDim(void)
{
    return dimCamera;
}


void cancelCameraDim(void)
{
    if ( spaceInSeqQueue() > 2 )
    {
        if ( dimCamera == true )
        {
            dimCamera = false;
            if ( lastIrisPosition == 0 )
            {
                // we were in auto iris so
                queueSequenceCommand(PS_RESTORE_AUTO_IRIS,true);
            }
            else
            {
                queueSequenceCommand(PS_SET_IRIS, lastIrisPosition);
            }
        }
    }
}

void toggleCameraDim(void)
{
    if ( dimCamera == false )
    {
        if ( spaceInSeqQueue() > 2 )
        {
            // we aren't dimmed - get the current values of iris, auto iris and save them
            dimCamera = true;
            boolean lastAutoIris = getCameraValue(DATA_COMMAND_GET_AUTO_IRIS);
            if ( lastAutoIris == true )
            {
                lastIrisPosition = 0;
                queueSequenceCommand(PS_RESTORE_AUTO_IRIS, false);
            }
            else
            {
                lastIrisPosition = getCameraValue(DATA_COMMAND_GET_IRIS);
                //Serial.printf(F("Get iris %d\n\r"),lastIrisPosition);
                if ( lastIrisPosition < 512 )
                {
                    // make sure we weren't saving a closed iris
                    lastIrisPosition = 512;
                }
            }
            queueSequenceCommand(PS_SET_IRIS, IRIS_DIMMED_LEVEL);
            // then manually set the dimmed level
        }
    }
    else
    {
        cancelCameraDim();
    }
}


//number ^ 2.5 tweaked near bottom 0-50 in 0-50 out
static const byte lookuppower2_5[] PROGMEM =
{
0,0,1,1,1,
1,1,1,2,2,
2,2,2,3,3,
4,4,5,5,6,
7,8,8,9,10,
11,12,13,14,15,
17,18,19,21,22,
23,25,27,28,30,
32,33,35,37,39,
41,43,46,48,50,
50
};


static const byte lookuppower1_8[] PROGMEM =
{
0,0,0,1,1,1,1,1,1,2,
2,2,3,3,3,4,4,5,5,6,
6,7,7,8,8,9,9,10,11,11,
12,13,14,14,15,16,17,18,18,19,
20,21,22,23,24,25,26,27,28,29,
30,31,32,33,34,35,36,38,39,40,
41,42,44,45,46,47,49,50,51,53,
54,55,57,58,60,61,62,64,65,67,
68,70,72,73,75,76,78,79,81,83,
84,86,88,89,91,93,95,96,98,100
};


static ubyte adjustWebPanTilt(int input)
{
    // input is -100 to+ 100
    // output needs to be 0-100, mid range is 50
    boolean negative = input < 0? true: false;

    if (negative) input *= -1;
    input /= 2;
    if (input > 50) input = 50;
    // input is now 0-50

    input = pgm_read_byte_near(lookuppower2_5 + input);
    if (negative)
    {
      input *= -1;  
    }
    input += 50;
    return (ubyte)input;
}


//================================================================

static uint8_t webZoom = 50;
static uint16_t webAnaStamp;
static uint8_t webTilt = 50;
static uint8_t webPan = 50;

void doWebZoom (int value)
{
    // we get a -100 to +100 max value from the web zoom widget
    static ubyte oldWebZoom;
    bool negative = (value < 0)?true:false;
    if (negative) value *= -1;
    if (value > 99)value = 99;
    value = pgm_read_byte_near(lookuppower1_8 + value);
    if (negative) value *= -1;

    webZoom =  value/2 +50;
    if (oldWebZoom != webZoom)
    {
        oldWebZoom = webZoom;   
        // 0 is fast zoom in (closer) 100 is fast zoom out 50 is static
        //printTime(true);Serial.printf("Web zoom %d\n",webZoom);
        queueCameraCommand(COMMAND_ZOOM, webZoom);
    }
    webAnaStamp = millis();    
}

void doWebPan (int value)
{
    // we get a += 100 max value from the web pantilt widget
    static ubyte oldWebPan;
    //webPan = (value/2) + 50;
    webPan = adjustWebPanTilt(value);
    if (webPan != oldWebPan)
    {
        oldWebPan = webPan;
        //printTime(true);Serial.printf("Web pan %d\n",webPan);
        queueCameraCommand(COMMAND_PAN, webPan);
    }
    webAnaStamp = millis();    
}

void doWebTilt (int value)
{
    static ubyte oldWebTilt;
    // we get a += 100 max value from the web zoom widget
//    webTilt = (value/2) + 50;
    webTilt = adjustWebPanTilt(value);
    if(oldWebTilt != webTilt)
    {
        oldWebTilt = webTilt;
        //printTime(true);Serial.printf("Web tilt %d\n",webTilt);
        queueCameraCommand(COMMAND_TILT, webTilt);
    }
    webAnaStamp = millis();    
}

/*
    Check if we've lost contact with a browser gui, If so zero any analog commands
*/
static void checkWebCommands(void)
{
    if ((uint16_t)(millis() - webAnaStamp) > 850 )
    {
        if (webZoom != 50)
        {
            webZoom = 50;
            queueCameraCommand(COMMAND_ZOOM, webZoom);
        }
        if (webPan != 50)
        {
            webPan = 50;
            queueCameraCommand(COMMAND_PAN,webPan);
        }
        if (webTilt != 50)
        {
            webTilt = 50;
            queueCameraCommand(COMMAND_TILT,webTilt);
        }
    }

}


void selectNextCamera(void)
{
    ubyte ctr = 0;
    do
    {
        if( ++selectedCamera >= MAX_CAMERAS)
        {
            selectedCamera = 0;
        }
    }
    while ( (isUsableIpAddress(configStore.IPConfigArray[selectedCamera + IP_ADDRESS_CAMERA1]) == false) && ++ctr < MAX_CAMERAS );

//    Serial.printf(F("Selected cam %d\n\r"),selectedCamera);
    cameraInited = 0;
}



// called every UI_UPDATEmS tick - nominally 75mS
void cameraTick75ms(void)
{

    checkWebCommands();
    // Camera ethernet laptop connection
    boolean status = getCameraValue(DATA_COMMAND_ISCONNECTED);
    #ifdef MAINDEBUG
    status = true;
    #endif
    static ubyte cameraReset;    
    if ( status == true)
    {
        kickCameraTimer();
        cameraReset = 255;
    }
    else if (cameraTimer > 0)
    {
        --cameraTimer;
    }
    if (cameraTimer == 0)
    {
        if (--cameraReset == 0)
        {
            // after a long period without contact, try and find a working camera
            printTime(true);Serial.printf(F("Selected camera not responding, trying next one\n"));
            selectNextCamera();
        }
    }
    // chekc for any sequenced command sets and put thenm into the command queue
    handleCameraPresetSequence();

    // update any camera status leds
    setNonBusyLED(LED_CAMERA_OK, status == true?LEDMODE_PATTERN_ON:LEDMODE_PATTERN_OFF);
    // Now update the camera selection indicators
    for ( ubyte ledNumber = 0; ledNumber < MAX_CAMERAS  ; ledNumber++ )
    {
        setNonBusyLED((tLED_ids)(ledNumber + (ubyte)LED_CAMERA1), (selectedCamera == ledNumber)?LEDMODE_PATTERN_ON:LEDMODE_PATTERN_OFF);
    }

    // auto iris led
    setLEDMode(LED_AUTO_IRIS, getCameraValue(DATA_COMMAND_GET_AUTO_IRIS) == true?LEDMODE_PATTERN_ON:LEDMODE_PATTERN_OFF);
    // auto focus led
    setLEDMode(LED_AUTO_FOCUS, getCameraValue(DATA_COMMAND_GET_AUTO_FOCUS) == true?LEDMODE_PATTERN_ON:LEDMODE_PATTERN_OFF);


    switch ( cameraInited )
    {
        case 1:
            // we have tried to select a camera and are waiting for it to connect
            if (cameraObj.isReady())
            {
                cancelCameraDim();
                cameraInited = 2;
            }
            break;
        case 2:
            // The currently selected camera is or has been talking to us and is online
            break;

        default :
            // we have selected a new camera so lets set it all up
            kickCameraTimer();
            cameraObj.begin(configStore.IPConfigArray[selectedCamera + IP_ADDRESS_CAMERA1]);
            cameraObj.connect();
            cameraObj.serialOutput(false); // disable serial debug
            cameraInited = 1;
            break;
    }
}




// A bunch of functions that tell us whether the ATEM and selected camera are
// present and responding. These avoid wasting time trying to repeatedly talk to
// items that are not connected. We retry if a relevant button is pushed and timeout
// reasonably quickly if there is no reply
void kickCameraTimer(void)
{
    cameraTimer = CAMERA_TIMEOUTmS/UI_UPDATEmS;
}

int getCameraValue(t_CameraGetData command)
{
    int returnVal = 0;

    switch ( command )
    {
        case DATA_COMMAND_ISCONNECTED :
            returnVal = cameraObj.isConnected(0);
            break;
        case DATA_COMMAND_GET_AUTO_IRIS:
            returnVal = cameraObj.getAutoIris(0);
            break;
        case DATA_COMMAND_GET_AUTO_FOCUS:
            returnVal = cameraObj.getAutoFocus(0);
            break;
        case DATA_COMMAND_GET_IRIS :
            returnVal = cameraObj.getIris(0);
            break;
        default :
            Serial.printf(F("Error. getCameraValue doesn't support the %d command\n\r"),command);
            break;
    }
    return returnVal;
}


/*
    A helper used when zoom in or out key is pressed.
    Calculate how fast to zoom and sends the command to the camera
    It repeats the zoom speed calc and sends the command 2- 3 times a second
*/
void doCameraZoom(boolean out, uint16_t keyRepeat)
{
    static int oldZoom;
    #define ZOOMUPDATEmS 300  // send a new zoom speed command every nmS
    if (  (keyRepeat % (ZOOMUPDATEmS/KEYREPEATmS)) == 0  )
    {
        // repeat is either 0 6, 12 (example) etc. If 0 the key has been released so stop zooming
        int reading = 50; // zoom stop value

        if ( keyRepeat != 0 )
        {
            reading = analogRead(A10) /(1024/50);
            if ( reading > 50 )
            {
                reading =50;
            }
            if ( reading < 1 )
            {
                reading =1;
            }
            if ( out )
            {
                reading = 50 + reading; // zoom out
            }
            else
            {
                reading = 50 - reading; // zoom in
            }
        }

        if ( oldZoom != reading )
        {
            oldZoom = reading ;
            // 0 is fast zoom in (closer) 100 is fast zoom out 50 is static
            queueCameraCommand(COMMAND_ZOOM, reading);
        }
    }
}

void stopFocusZoom(void)
{
    doCameraFocus(false,0) ;
    doCameraZoom(false,0) ;

}

/*
    A helper used when focus in or out key is pressed.
    Calculate how fast to focus and sends the command to the camera
    It only sends the command when values have changed to avoid multiple repeats
*/
void doCameraFocus(boolean focusIn, uint16_t keyRepeat)
{
    if (keyRepeat == 1 && getCameraValue(DATA_COMMAND_GET_AUTO_FOCUS) == true)
    {

        // turn off auto focus on first press of switch
        queueCameraCommand(COMMAND_AUTOFOCUS, 0);
    }
    ubyte newSpeed = calculateCameraSpeed(keyRepeat, SC_FOCUS);
    static ubyte oldFocusSpeed;
    if ( focusIn )
    {
        newSpeed += 50;
    }
    else
    {
        newSpeed = 50 - newSpeed;
    }
    if ( newSpeed != oldFocusSpeed )
    {
        oldFocusSpeed = newSpeed;
        queueCameraCommand(COMMAND_FOCUS, newSpeed);
    }
}

/*
    Gets the current settings from the currently selected camera
    The iris position
    Auto focus enabled/disabled
    Auto iris enabled/disabled
*/
void saveCameraPreset(ubyte presetNum)
{
    configStore.cameraPreset[presetNum][selectedCamera].iris = getCameraValue(DATA_COMMAND_GET_IRIS);
    configStore.cameraPreset[presetNum][selectedCamera].autoFocus = getCameraValue(DATA_COMMAND_GET_AUTO_FOCUS);
    configStore.cameraPreset[presetNum][selectedCamera].autoIris = getCameraValue(DATA_COMMAND_GET_AUTO_IRIS);
    saveConfigStoretoEEPROM(); // save any changes
}



/*
    This function turns the amount of time a switch has been closed into
    a speed value for the camera adjustments to focus and zoom.
    It is used for the zoom and focus paddle switches.
    When the switch first closes, the function starts moving at the minimum
    speed defined. After the MIN_KEY_HOLDmS delay the speed of movement starts
    to accellerate and reaches the max MAX_SPEED at approx MAX_KEY_HOLDmS

*/
ubyte calculateCameraSpeed(uint16_t keyRepeats, t_SwitchConfigs switchSelection)
{
    // assumes switchtypes 0-FOCUS,1-IRIS,2-ZOOM

    //returns a value of 0 (don't move) to 50 (full speed), depending
    // on the keyRepeats value. Each keyrepeat increment is
    // key held ON for KEYREPEATmS
    #define MAX_SPEED 50 // 1-50 defined by the camera functions
    ubyte speed = 0;
    int keyRepeatsAtMin = configStore.switchConfig[switchSelection].minTimemS/KEYREPEATmS;
    int minSpeed =
        ((configStore.switchConfig[switchSelection].minValuePercent * MAX_SPEED) + (MAX_SPEED/2))/100;
    if ( minSpeed < 1 )
    {
        minSpeed = 1;
    }
    if ( minSpeed > 50 )
    {
        minSpeed = 50;
    }
    int stepsOnRamp =
         configStore.switchConfig[switchSelection].rampTimemS/KEYREPEATmS;
    if ( stepsOnRamp < 1 )
    {
        stepsOnRamp = 1;
    }
    if ( keyRepeats > 0 )
    {
        speed = minSpeed;
        if ( keyRepeats > keyRepeatsAtMin )
        {
            // Key is held down and we are on the increasing speed ramp.
            // Calculate how far up it we have got
            keyRepeats -= keyRepeatsAtMin;
            if ( keyRepeats > MAX_SWITCHRAMPTIMEmS/KEYREPEATmS )
            {
                keyRepeats = MAX_SWITCHRAMPTIMEmS/KEYREPEATmS; // cap at 7 seconds
            }
            speed = MAX_SPEED-minSpeed; // this is the amount of 'travel' left
            minSpeed += ((speed *keyRepeats)/stepsOnRamp);
            speed = minSpeed;
        }
    }

    // else dont' move - return zero speed

    if ( speed > MAX_SPEED)
    {
        speed = MAX_SPEED;
    }
    return speed;
}



/*
    This function turns the amount of time a switch has been closed into
    how faR to move the iris. The Iris command is a position command rather
    than a speed of movement command like zoom and focus and therefore we need to
    know the current iris position, add or subtract a small amount from it and
    send it back.
    Iris value of 1 is very dark and 1023 is fully open
    0x0FFFF is unknown - don't do anything
*/
uint16_t calculateIrisPosition(uint16_t keyRepeats, bool directionOpen)
{
    static int currentIris = 512;
    static int oldIris = 512;
    #define MAX_IRIS_STEP  100  // say 10%
    #define MIN_IRIS_JUMP 2  // if too small it is ignored - try 2-10
    if ( keyRepeats == 1 )
    {
        // when we start the iris move get its current position as the starting point
        // this isn't updated fast enough to use it every time - just on the initial key press
        currentIris = getCameraValue(DATA_COMMAND_GET_IRIS);
        if ( getCameraValue(DATA_COMMAND_GET_AUTO_IRIS) )
        {
            // auto iris is ON, turn it off
             queueCameraCommand(COMMAND_AUTOIRIS, 0); // turn off autoiris
        }
       //Serial.printf(F("apczzz IRIS move %d\n\r"),getCameraValue(DATA_COMMAND_GET_IRIS)); // apczzz debug
    }

    int speed = calculateCameraSpeed(keyRepeats, SC_IRIS);
    int newPosition = 0xFFFF;
    if ( directionOpen )
    {
         currentIris += (speed * MAX_IRIS_STEP)/MAX_SPEED;
    }
    else
    {
        currentIris -= (speed * MAX_IRIS_STEP)/MAX_SPEED;
    }
    if ( currentIris < 0 )
    {
        currentIris = 0;
    }
    else if ( currentIris > 1023 )
    {
        currentIris = 1023;
    }
    if ( abs(currentIris - oldIris ) > MIN_IRIS_JUMP)
    {
        oldIris = currentIris ;
        newPosition = currentIris;
       //Serial.printf(F("apczzz IRIS position %d, new %d\n\r"),getCameraValue(DATA_COMMAND_GET_IRIS),newPosition); // apczzz debug
    }
    return newPosition;
}


// called every time around main loop. Checks if there is anything in any queue or sequence that needs
// acting on
static void handleCameraQueues(void)
{
    #ifdef MAINDEBUG
        t_CameraCommands command;
        int value;
        if (getFromFIFO(&command, &value) == true)
        {
//           printTime(true);    Serial.printf(F("DEBUG No command sent: Camera command %s from q, value %d\n\r"),cameraCommandNames[command],  value);
        }
        return;
    #endif

    if ( cameraObj.isReady() && cameraObj.isConnected(0))
    {
        t_CameraCommands command;
        int value;
        if (getFromFIFO(&command, &value) == true)
        {
            #ifdef MAINDEBUG
            printTime(true);
            Serial.printf(F("DEBUG: Camera command %s from q, value %d\n\r"),cameraCommandNames[command],  value);
            #endif

            kickCameraTimer();

            // indicate activity on the selected camera
            setLEDMode((tLED_ids)(selectedCamera + (ubyte)LED_CAMERA1), LEDMODE_PATTERN_5050, 6);

            switch ( command )
            {
                case COMMAND_TILT :
                    cameraObj.doTilt(0,(uint8_t)value); // 50 is neutral
                    break;
                case COMMAND_PAN :
                    cameraObj.doPan(0,(uint8_t)value); // 50 is neutral
                    break;
                case COMMAND_ZOOM :
                    cameraObj.doZoom(0,(uint8_t)value); // 50 is neutral
                    break;
                case COMMAND_SET_IRIS :
                    cameraObj.setIris(0,value);
                    break;
                case COMMAND_AUTOIRIS :
                    cameraObj.setAutoIris(0,(value == 0)?false:true);
                    break;
                case COMMAND_FOCUS :
                    cameraObj.doFocus(0,(uint8_t)value);
                    break;
                case COMMAND_AUTOFOCUS :
                    cameraObj.setAutoFocus(0,(value == 0)?false:true);
                    break;
                case COMMAND_ON_TOUCH_AUTOFOCUS :
                    cameraObj.onTouchAutofocus(0);
                    break;
                case COMMAND_PRESET_STORE :
                    cameraObj.storePreset(0, value);
                    break;
                case COMMAND_PRESET_USE :
                    cameraObj.recallPreset(0,value);
                    break;
                default :
                    Serial.printf(F("Error. doCameraCommand doesn't support the %d command\n\r"),command);
                    break;
            }
            indicateCameraActivity();

        }
    }
}
int tiltPanLookup(int value)
{
// takes a value from -100 to +100 scales and curve fits it.
// outputs a value from 0 to +100, 50 is neutral

    if (value > 100) value = 100;
    if (value < -100) value = -100;
    return (value/2) + 50;

/*
    boolean negative = false;
    if ( value < 0 )
    {
        value *= -1;
        negative = true;
    }
    if  (value > 30)
    {
        value = (((value *200)/70) - 25)/5;
    }
    else
    {
        value = value/3;
    }
    if ( value < 0 )
    {
        value = 0;
    }
    if ( negative )
    {
        value *= -1;
    }
    if ( value > 50 )
    {
        value = 50;
    }
    value += 50;

    return value;
*/

}

// called every pass of main loop
void loopCamera(void)
{

    if ( cameraTimer > 0)
    {
        cameraObj.runLoop();
    }

    handleCameraQueues();
    static ubyte timestamp;
    if ( (ubyte)((ubyte)millis() - timestamp) > 40 )
    {
        timestamp = millis();
        //don't do this too often or it will clog buffers with small moves
        // evr 40mS is fast enough
        int value;
        // now  check if joysticks have moved and queue up any resulatant
        // pan and tilt commands
        if (utils.joystick_hasMoved(0))
        {
            static int lastPan;
            value = utils.joystick_position(0);
            //create a dead zone
            if ( value >= 0 )
            {
                if ( value > configStore.joyStickDeadzone )
                {
                    value -= configStore.joyStickDeadzone;
                }
                else
                {
                    value = 0;
                }
            }
            else
            {
                if ( value < (configStore.joyStickDeadzone * -1) )
                {
                    value += configStore.joyStickDeadzone;
                }
                else
                {
                    value = 0;
                }
            }
            value = tiltPanLookup(value);
            if ( lastPan != value )
            {
                 lastPan = value;
                queueCameraCommand(COMMAND_PAN,value);
            }
        }

        if (utils.joystick_hasMoved(1))
        {
            static int lastTilt;

            value = utils.joystick_position(1) ;  // don't invert it;
            //create a dead zone
            if ( value >= 0 )
            {
                if ( value > configStore.joyStickDeadzone )
                {
                    value -= configStore.joyStickDeadzone;
                }
                else
                {
                    value = 0;
                }
            }
            else
            {
                if ( value < (configStore.joyStickDeadzone * -1) )
                {
                    value += configStore.joyStickDeadzone;
                }
                else
                {
                    value = 0;
                }
            }
            value = tiltPanLookup(value);
            if ( lastTilt != value )
            {
                lastTilt = value;
                queueCameraCommand(COMMAND_TILT,value);
            }
        }
    }
}

ubyte getSelectedCamera(void)
{
    return selectedCamera;
}

void initCameras (void)
{
    printModuleStartup(PSTR("Camera Utilities"),CAMERA_CPP_VER,CAMERA_H_VER,lastsave);
}
