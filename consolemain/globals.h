#ifndef __globals_h
#define __globals_h
/*
	Last change: AC 14/04/2020 12:37:10
*/


// define this for comprehensive debug messages
//#define MAINDEBUG

// define this for screen and projector control debug messages
//#define SCREENDEBUG



#include <arduino.h>
#include "revision.h"




// the baud rate for comms
#define SERIAL_BAUD 115200


// max adc output. Generic define
#define ADCMAX 1023

// define the max and min values that can be sent to setIris() camera object
#define MAX_IRIS 1023
#define MIN_IRIS 0


//Some useful types
typedef unsigned int t_VideoSrc;
typedef unsigned char t_VideoIndex;
typedef unsigned int t_Timer16;
typedef unsigned int t_EEPROMADD;
typedef unsigned char ubyte;



// define BUTTON MATRIX pins. It is an 8 in x 4 out matrix
// Button inputs are ALL portL pins
#define BUTTON_IN0_PIN 49
#define BUTTON_IN1_PIN 48
#define BUTTON_IN2_PIN 47
#define BUTTON_IN3_PIN 46
#define BUTTON_IN4_PIN 45
#define BUTTON_IN5_PIN 44
#define BUTTON_IN6_PIN 43
#define BUTTON_IN7_PIN 42
#define BUTTON_IN_PORT PINL  // the register to read for all 8 lines

// Button output scan lines
#define BUTTON_OUT0_PIN 25
#define BUTTON_OUT1_PIN 27   //
#define BUTTON_OUT2_PIN 31
#define BUTTON_OUT3_PIN 33


// SCreen control relay outputs
#define SCREEN_DOWN_PIN 15
#define SCREEN_UP_PIN 14
#define SCREEN_OUT_PIN 16
#define SCREEN_IN_PIN 17


// screen relay operation times (hpw long in seconds the relay is on for)
#define SCREEN_OUTmS 48000L // this has a mechanical limit so we need to be on for long enough
#define SCREEN_INmS 48000L  // this has a mechanical limit so we need to be on for long enough
#define SCREEN_UPmS 33000L  // this has a mechanical limit so we need to be on for long enough



#define I2C_IN_ADD 0x20
#define I2C_OUT0_ADD 0x21
#define I2C_OUT1_ADD 0x22
#define I2C_OUT2_ADD 0x23


// define the time after which we stop trying to raise a camera that should be there
// this stops us slowing down userinterface responses whilst awaiting ethernet packets
// from non existant devices. Suggest 5000-10000. It restarts if we touch a camera function
// switch
#define CAMERA_TIMEOUTmS 5000  // max 15000

// define the time after which we stop trying to raise the ATEM that should be there
// this stops us slowing down userinterface responses whilst awaiting ethernet packets
// from non existant devices. Suggest 5000-10000. It restarts if we touch an ATEM function
// switch
#define ATEM_TIMEOUTmS 5000  // max 15000

// define the update period for LEDs and other user interface type stuff.
// For leds - this is the shortest on or off time when flashing. suggest 50-100mS
// it is also used as a slow prescaler timebase for other things so best not to change it without
// checking on adverse effects
#define UI_UPDATEmS 75L

// Define the led bits numbers along the three I2C expanders
// Expander I2C_OUT0_ADD has leds 0-7
// Expander I2C_OUT1_ADD has leds 8-15
// Expander I2C_OUT2_ADD has leds 16-23
#define I2C_PIN_LED_MISC2           0
#define I2C_PIN_AUTO_IRIS           1
#define I2C_PIN_LED_CAMERA1          2
#define I2C_PIN_LED_CAMERA2          3
#define I2C_PIN_LED_MISC1           4
#define I2C_PIN_LED_CAMERA_OK        5
#define I2C_PIN_AUTO_FOCUS          6
#define I2C_PIN_LED_ATEM_OK          7

#define I2C_PIN_LED_ATEM_PRESET6     8
#define I2C_PIN_LED_ATEM_PRESET1     9
#define I2C_PIN_LED_ATEM_PRESET7     10
#define I2C_PIN_LED_ATEM_PRESET2     11
#define I2C_PIN_LED_ATEM_PRESET3     12

#define I2C_PIN_LED_ATEM_PRESET8     13
#define I2C_PIN_LED_ATEM_PRESET4     14
#define I2C_PIN_LED_ATEM_PRESET5     15

#define I2C_PIN_LED_MP3              16
#define I2C_PIN_LED_PROJ             17
#define I2C_PIN_LED_SCREEN           18
#define I2C_PIN_LED_FTB              19
#define I2C_PIN_LED_SPARE            20
#define I2C_PIN_BLUETOOTH_POWER      21

// not used #define I2C_PIN_LED_RIGHT            21
// not used #define I2C_PIN_LED_GREEN            22
// not used #define I2C_PIN_LED_BLUETOOTH_OK     23



#ifdef apczzz
// The 2560 board general IO pin definitions.
// Because we have two shields some pins are covered byte the shields even if ( not actually
// used on them. I have tried to avoid using those pins)
// See also spread sheet "arduino mega 2560 pinouts.xlsx" in onedrive here:
// https://stjohnschurch809.sharepoint.com/_layouts/15/guestaccess.aspx?docid=1fda35b4bb11c4b169646776d14266928&authkey=Ae_n-R9fFPsdGB7JxQDmRig&e=ca62582233864ce58345c89ba4ef2aec
// This list is not necessarily up to date. Check the hardware defines in code for the absolute latest

 2560 Pin name       Arduino pin                 Our functionality
PA0 (AD0)	        Digital pin 22
PA1 (AD1)	        Digital pin 23              BUTTON_OUTRADIO_PIN
PA2 (AD2)	        Digital pin 24              NU
PA3 (AD3)	        Digital pin 25              BUTTON_OUT0_PIN
PA4 (AD4)	        Digital pin 26              NU
PA5 (AD5)	        Digital pin 27              BUTTON_OUT1_PIN
PA6 (AD6)	        Digital pin 28              NU
PA7 (AD7)	        Digital pin 29
PB0 (SS/PCINT0)	    Digital pin 53 (SS)	        SS_OUTPUTxxx
PB1 (SCK/PCINT1)	Digital pin 52 (SCK)	    SCKxxxx
PB2 (MOSI/PCINT2)	Digital pin 51 (MOSI)	    MOSIxxxx
PB3 (MISO/PCINT3)	Digital pin 50 (MISO)	    MISOxxxx
PB4 (OC2A/PCINT4)	Digital pin 10 (PWM)	    CS_ENETxxxx
PB5 (OC1A/PCINT5)	Digital pin 11 (PWM)
PB6 (OC1B/PCINT6)	Digital pin 12 (PWM)
PB7 (OC0A/OC1C/PCINT7)Digital pin 13 (PWM)
PC0 ( A8 )	        Digital pin 37              RADIO_PROG_LED_IN
PC1 ( A9 )	        Digital pin 36
PC2 ( A10 )	        Digital pin 35              RADIO_PROG_OUT
PC3 ( A11 )	        Digital pin 34
PC4 ( A12 )	        Digital pin 33              BUTTON_OUT3_PIN
PC5 ( A13 )	        Digital pin 32              NU
PC6 ( A14 )	        Digital pin 31              BUTTON_OUT2_PIN
PC7 ( A15 )	        Digital pin 30
PD0 (SCL/INT0) 	    Digital pin 21 (SCL)
PD1 (SDA/INT1)	    Digital pin 20 (SDA)
PD2 (RXDI/INT2)	    Digital pin 19 (RX1)
PD3 (TXD1/INT3)	    Digital pin 18 (TX1)
PD4 (ICP1)
PD5 (XCK1)
PD6 ( T1 )
PD7 ( T0 )	        Digital pin 38
PE0 (RXD0/PCINT8)	Digital pin 0 (RX0)	        RX0debugxxx
PE1 (TXD0)	        Digital pin 1 (TX0)	        TX0Debugxxx
PE2 (XCK0/AIN0)
PE3 (OC3A/AIN1)	    Digital pin 5 (PWM)
PE4 (OC3B/INT4)	    Digital pin 2 (PWM)	        ENETIRQxxxx
PE5 (OC3C/INT5)	    Digital pin 3 (PWM)
PE6 (T3/INT6)
PE7 (CLKO/ICP3/INT7)
PF0 (ADC0)	        Analog pin 0
PF1 (ADC1)	        Analog pin 1
PF2 (ADC2)	        Analog pin 2
PF3 (ADC3)	        Analog pin 3
PF4 (ADC4/TMK )	    Analog pin 4
PF5 (ADC5/TMS )	    Analog pin 5
PF6 (ADC6)	        Analog pin 6
PF7 (ADC7)	        Analog pin 7
PG0 ( WR )	        Digital pin 41
PG1 ( RD )	        Digital pin 40
PG2 ( ALE )	        Digital pin 39              RADIO_POWER_OUT
PG3 (TOSC2)
PG4 (TOSC1)
PG5 (OC0B)	        Digital pin 4 (PWM)	        SSDCARDCSxxx
PH0 (RXD2)	        Digital pin 17 (RX2)	    RS485_RXD
PH1 (TXD2)	        Digital pin 16 (TX2)	    RS485_TXD
PH2 (XCK2)
PH3 (OC4A)	        Digital pin 6 (PWM)	        RS485_TXEN
PH4 (OC4B)	        Digital pin 7 (PWM)	        BTIRQxxxx
PH5 (OC4C)	        Digital pin 8 (PWM)	        BTCSxxxx
PH6 (OC2B)	        Digital pin 9 (PWM)
PH7 (T4 )
PJ0 (RXD3/PCINT9)	Digital pin 15 (RX3)
PJ1 (TXD3/PCINT10)	Digital pin 14 (TX3)
PJ2 (XCK3/PCINT11)
PJ3 (PCINT12)
PJ4 (PCINT13)
PJ5 (PCINT14)
PJ6 (PCINT 15 )
PJ7
PK0 (ADC8/PCINT16)	Analog pin 8                JOYSTICK VERTICAL
PK1 (ADC9/PCINT17)	Analog pin 9                JOYSTICK HORIZONTAL
PK2 (ADC10/PCINT18)	Analog pin 10               ZOOM SPEED CONTROL
PK3 (ADC11/PCINT19)	Analog pin 11
PK4 (ADC12/PCINT20)	Analog pin 12
PK5 (ADC13/PCINT21)	Analog pin 13
PK6 (ADC14/PCINT22)	Analog pin 14
PK7 (ADC15/PCINT23)	Analog pin 15
PL0 (ICP4)	        Digital pin 49              BUTTON_IN0_PIN
PL1 (ICP5)	        Digital pin 48              BUTTON_IN1_PIN
PL2 (T5 )	        Digital pin 47              BUTTON_IN2_PIN
PL3 (OC5A)	        Digital pin 46 (PWM)        BUTTON_IN3_PIN
PL4 (OC5B)	        Digital pin 45 (PWM)        BUTTON_IN4_PIN
PL5 (OC5C)	        Digital pin 44 (PWM)        BUTTON_IN5_PIN
PL6                 Digital pin 43              BUTTON_IN6_PIN
PL7                 Digital pin 42              BUTTON_IN7_PIN
 			
#endif
    
    
    
    
    
    
    
#endif

