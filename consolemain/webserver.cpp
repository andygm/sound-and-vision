#include "globals.h"
PROGMEM const char lastsave[] ="2024-08-05 20:46:56";
#define WEB_CPP_VER 3

/*
    This contains the webserver
*/
// Last saved 2023-09-13 18:34:41

#include <Ethernet.h>
#include "revision.h"

#include "webserver.h"

#include <SkaarhojPgmspace.h>
#include "leddriver.h"
#include "buttondriver.h"
#include "utilities.h"

#include "index.h"

#include "camerautils.h"
//#include "atemutils.h"
//#include <wire.h>
//#include "config.h"


#define MAXHTML 65 // general receive buffer
static char HTTP_req[MAXHTML+1]; // stores the HTTP request
static uint8_t HTTPOffset;




EthernetServer server = EthernetServer(80);

EthernetClient ethcl = NULL;

static void dump(char *buffer, int maxChars)
{
    do
    {
        if(*buffer == '\0') break;
        Serial.printf(F("%c"),*buffer++);

    }while (--maxChars > 0);
}


#define MAXTXBUFF 125 // general transmit buffer
static char txBuff[MAXTXBUFF+3]; // stores the HTTP request
static uint16_t txOffset;

void txReset(void)
{
    txOffset = 0;   
    txBuff[txOffset] = '\0';
}

static void txFlush(void)
{
    if (txOffset > 0)
    {

        txBuff[txOffset] = '\0';
        ethcl.write(txBuff,txOffset);
    
        txOffset = 0;
    }
}
static void txPrint(char* string)
{
    int length = strlen(string);
    int charsToWrite;
    do
    {
        charsToWrite = MAXTXBUFF-txOffset;
        if (length < charsToWrite) charsToWrite = length;

        memcpy(&txBuff[txOffset], string, charsToWrite);
        string += charsToWrite;
        length -= charsToWrite;
        txOffset += charsToWrite;
        txBuff[txOffset] = '\0';

        if (txOffset > (MAXTXBUFF-5))
        {
            int count = ethcl.write(txBuff,txOffset);
            txOffset = 0;
        }
    }while(length > 0);


}

static void txPrint_P(char* string)
{
    int length = strlen_P(string);

    int charsToWrite;
    do
    {
        charsToWrite = MAXTXBUFF-txOffset;
        if (length < charsToWrite) charsToWrite = length;

        memcpy_P(&txBuff[txOffset], string, charsToWrite);
        string += charsToWrite;
        length -= charsToWrite;
        txOffset += charsToWrite;
        txBuff[txOffset] = '\0';

        if (txOffset > (MAXTXBUFF-5))
        {
            ethcl.write(txBuff,txOffset);
            txOffset = 0;
        }
    }while(length > 0);

}


#define CF(x) ((const __FlashStringHelper *)x)

void dump_P(char *source)
{
    return;
    char ch;
    do
    {
        ch = pgm_read_byte(source++);
        Serial.printf(F("%c"),ch);

    }while (ch != NULL);

}


typedef enum t_configPage
{
    TCONF_ATEM_PRESETS,
    TCONF_CAMERA1_PRESETS,
    TCONF_CAMERA2_PRESETS,
    TCONF_CONFIG,
};



const char html_header_top[] PROGMEM = 
R"rawliteral(<!DOCTYPE HTML><html><head>
  <title>St Johns Console Config page</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
<script>
    let joyval={x:0,y:0,time:0};
    let zoomval={x:0,time:0};
    function QueueJoy(x, y)
    {
        if (x != joyval.x)
        {
            if (x == 0 || joyval.x == 0)
            {
                joyval.time =0;
            }
            joyval.x = x;
        }
        if (y != joyval.y)
        {
            if (y == 0 || joyval.y == 0)
            {
                joyval.time =0;
            }
            joyval.y = y;
        }
    }
    function QueueZoom(x)
    {
        if (x != zoomval.x)
        {
            if (x == 0 || zoomval.x == 0)
            {
                zoomval.time =0;
            }
            zoomval.x = x;
        }
    }

  var lastDown = false;
  var lastId = -1;
  var repeats = 0;
  var divctr = 0;

    function SendTouch(id,down) {
    var rq = new XMLHttpRequest();
    rq.open("GET", "m0usE"+ id + "="+ (down?"true":"false") , false);
    rq.send();
    }

    function resetJoy() {
        let jsbdy = document.getElementById("joystick").getBoundingClientRect();
        knob.style.left = window.scrollX + ((jsbdy.left + jsbdy.width/2) -(knob.offsetWidth/2)) +"px";
        knob.style.top = window.scrollY +((jsbdy.top + jsbdy.height/2) -(knob.offsetWidth/2) )+"px";
        QueueJoy(0, 0);
    }

  function aNaTouch(event) {
    let id = event.currentTarget.id;
    if ((event.type != 'mousemove') || (event.type == 'mousemove' && event.buttons ===1))
    {
        let x =-1
        let y = -1;
        let relx = -1;
        let rely = -1;

        if (event.type == 'mouseup' || event.type == 'touchend')
        {
            if (id == 'zoom')
            {
                zoom.value = 0;
                QueueZoom( 0);
            }
            else
            {
                resetJoy();
            }
        }
        else  if (event.type == 'mousemove' || event.type == 'touchmove')
        {
            if (id == 'joystick')
            {
                let jsbdy = document.getElementById("joystick").getBoundingClientRect();
                if (event.type == 'touchmove')
                {
                    x = event.touches[0].clientX;
                    y = event.touches[0].clientY;
                }
                else
                {
                    x = event.clientX;
                    y = event.clientY;

                }
                
                relx = x- jsbdy.left ;
                rely = y- jsbdy.top ;
                y += window.scrollY;
                x += window.scrollX;
                let knobrad = knob.offsetWidth/2;
                let min = knobrad;
                let max =  jsbdy.width-knobrad;
                if (relx >= min && relx < max && rely >= min && rely < max)
                {
                    knob.style.left = (x - (knob.offsetWidth/2)) +"px";
                    knob.style.top = (y -(knob.offsetWidth/2))+"px";
                    relx = Math.floor(((relx-min) * 200 /(max-min)) - 100);
                    rely = Math.floor(((rely-min) * 200 /(max-min)) - 100);
                    QueueJoy(relx, rely);
                }
            }
            else
            {
                QueueZoom(zoom.value);

            }
        }
    }

}
  function IsTouch(event) {
    let id = event.currentTarget.id.split('~')[1];
    let down = (event.type == "mousedown" || event.type == "touchstart")?true:false;
    if (lastId != id || lastDown != down)
    {
        lastId = id;
        lastDown = down;
        repeats = 50; // x 150mS
        SendTouch(id,down);
    }
  }

    var knob;
    var zoom;
    var refDiv = 1;

  function doOnLoad() {
     knob = document.getElementById("knob");
     zoom = document.getElementById("zoom");

    OpenPage(lastPage);
    setTimeout(GetConf, 250);
    setInterval(DoRefresh, 150);
    setInterval(DoAna, 42);
  }
  
</script>
  )rawliteral";

const char html_header_tail[] PROGMEM = 
R"rawliteral(
</head><body>
  )rawliteral";




const char html_reboot[] PROGMEM = 
R"rawliteral(<h2>Rebooting. Please wait and then click to refresh</h2>
  </body></html>
)rawliteral";



const char html_cssblink_style[] PROGMEM = 
R"rawliteral(
<style>

#js-container {
    border: solid 5px ;
    border-top-color:#554455;
    border-left-color:#776677;
    border-bottom-color:#554455;
    border-right-color:#776677;
    display: inline-block;
}
#joystick {
    width: 200px;
    height: 200px;
    margin: 3px;
    cursor: move;
}

.dot {
  position:absolute;
  height: 45px;
  width: 45px;
  background-color: #AAAAFF;
  border-style: solid ;
  border-color: #444488;
  border-width: 6px;
  border-radius: 50%;
  display: inline-block;
}

.slider {

  -webkit-appearance: none;
  appearance: slider-vertical;
  width: 25px;
  height: 88%;
  background: #d3d3d3;
  outline: none;
  opacity: 0.6;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider:hover {
  opacity: 0.8;
}


.bK{
    -webkit-animation-name: blinker;
    -webkit-animation-duration: 0s;
    -webkit-animation-iteration-count:infinite;
    -webkit-animation-timing-function:ease-in-out;
    -webkit-animation-direction: forwards ;
}
@keyframes blinker {
    0%   {opacity: 1.0;}
    80%  {opacity: 1.0;}
    81%  {opacity: 0.2;}
    100%  {opacity: 0.2;}

}

.ptz-container {
  display: grid;
  grid-template-columns: 19% 81%;
   background-color: #2196F3;
  padding: 3px;
  width: 320px;
}

.cam-g-container {
  display: grid;
  grid-template-columns: 22% 39% 39%;
   background-color: #2196F3;
  padding: 5px;
  width: 310px;
}
.g-item {
  background-color: rgba(255, 255, 255, 0.8);
  border: 1px solid rgba(0, 0, 255, 0.8);
  padding: 2px;
  font-size: 10px;
  text-align: center;
}


.butt {
    background-color:#888888;
    padding: 3px;
    width: 42px;
    height: 42px;
    border-style: solid ;
    border-color: #333333;
    border-width: 5px;
    border-radius: 35px;
    margin: 2px;
}

.butt:hover {
    opacity: 0.8;
}
.butt:active {
    opacity: 0.6;
  box-shadow: 0 3px #666;
  transform: translateY(3px);
}


.led {
    background-color:#333333;
    padding: 3px;
    width: 42px;
    height: 42px;
    border-style: solid !important;
    border-color: #000 !important;
    border-width: 8px;
    border-radius: 35px;
    margin: 2px;
    margin-left: 7px;
}
.label{
    background-color: #FFF;
    color:#000;
    padding: 5px;
    width: 95px;
    height: 60px;
    position: relative;
    vertical-align:top;
    font-size: 13px;
    font-family:monospace;
    border-style: solid !important;
    border-color: #000 !important;
    border-width: 2px;
    border-radius: 9px;
    margin: 2px;
    font-weight: bold;
}
.lab{
    background-color: #c8e0e6;
    color:#000;
    padding: 5px;
    position: relative;
    vertical-align:top;
    font-size: 13px;
    font-family:monospace;
    border-style: none;
    margin: 2px;
    font-weight: bold;
}

.tB {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    background-color: #440000;
    color:#fff;
    padding: 5px;
    width: 160px;
    height: 70px;
    position: relative;
    vertical-align:top;
    font-size: 14px;
    -webkit-text-stroke-width: 0.3px;
    -webkit-text-stroke-color: black;
    font-family:monospace;
    border-style: solid !important;
    border-color: #000 !important;
    font-weight: bold;
    border-width: 4px;
    border-radius: 9px;
    margin: 2px;
}
.tB:hover {
    opacity: 0.8;
}
.tB:active {
    opacity: 0.6;
  box-shadow: 0 5px #666;
  transform: translateY(4px);

}

</style>
)rawliteral";

const char html_buthdr_style[] PROGMEM = 
R"rawliteral(
<style>
buthdr{
    color : yellow;
    font-size: 16px;
    font-weight: bold;
}

</style>

)rawliteral";



const char html_openpage[] PROGMEM = 
R"rawliteral(
<script>

  var lastPage = "atemcontrol";
  function OpenPage(pN) {
  if (pN == "config")  
   {
      var result = confirm("Changing the config could have serious consequences. Continue?");
      if (result != true)return;
   }
  let request = new XMLHttpRequest();
  if (pN == "reboot")  
   {
      var result = confirm("Reboot and apply any changes?");
      if (result == true)
      {
        request.open("GET", "m0usE999=reboot" , true);
        request.send();
        pN="atemcontrol";
      }
      else
      {
        return;
      }
   }
   lastPage = pN;
    request.open("GET", "pAg3="+pN , true);
    request.send();
    if (document.getElementById(pN).className.includes("qu"))
    {
        refDiv = 1;
    }
    else
    {
        refDiv = 6;
    }

  var i, t_c;
  t_c = document.getElementsByClassName("t_c");
  for (i = 0; i < t_c.length; i++) {
    t_c[i].style.display = "none";
  }
  document.getElementById(pN).style.display = "block";
  resetJoy();
  GetConf();
}
</script>
)rawliteral";


const char setup_clicks_script[] PROGMEM = 
R"rawliteral(
<script>
    let items  = document.getElementsByClassName('cK');
    for (let index = 0; index < items.length; index++)
    {
        items[index].addEventListener("touchstart",IsTouch);
        items[index].addEventListener("mousedown",IsTouch);
        items[index].addEventListener("mouseup",IsTouch);
        items[index].addEventListener("touchend",IsTouch);
    }
    items  = document.getElementsByClassName('aNa');
    for (let index = 0; index < items.length; index++)
    {
        items[index].addEventListener("touchstart",aNaTouch);
        items[index].addEventListener("touchmove",aNaTouch);
        items[index].addEventListener("mousedown",aNaTouch);
        items[index].addEventListener("mousemove",aNaTouch);
        items[index].addEventListener("mouseup",aNaTouch);
        items[index].addEventListener("touchend",aNaTouch);
    }

    let forms  = document.getElementsByTagName('form');
    for (let index = 0; index < forms.length; index++)
    {
        forms[index].addEventListener("submit",sendConf);
    }



</script>
)rawliteral";

const char html_head_ajax_conf[] PROGMEM = 
R"rawliteral(
    <script>
    function GetConf() {
    var request = new XMLHttpRequest();
    nocache = "&nocache=" + Math.random() * 1000000;
    request.onreadystatechange = function() {
    if (this.readyState == 4) {
      if (this.status == 200) {
      if (this.responseText != null) {
        let confs = this.responseText.split('~');
        let targets  = document.getElementsByTagName('form');
        for (let index = 0; index < targets.length; index++)
        {
            targets[index][0].value = confs[targets[index][0].id];
        }
        targets  = document.getElementsByClassName('cF');
        for (let index = 0; index < targets.length; index++)
        {
            let confId = targets[index].id.split('~')[2];
            targets[index].innerHTML = confs[confId];
        }

    }}}}
    request.open("GET", "ajax_conf" + nocache, true);
    request.send(null);
    }
</script>
)rawliteral";



const char html_head_ajax_script[] PROGMEM = 
R"rawliteral(
    <script>
    function DoAna()
    {
        let now = Date.now();
        if (now > joyval.time)
        {
            let rq = new XMLHttpRequest();
            rq.open("GET", "aNa=joystick_"+joyval.x+'_'+joyval.y,true);
            rq.send();
            if (joyval.x == 0 && joyval.y == 0)
            {
                joyval.time = now + 4000;
            }
            else
            {
                joyval.time = now + 150;
            }

        }
        if (now > zoomval.time)
        {
            let rq = new XMLHttpRequest();
            rq.open("GET", "aNa=zoom_"+zoomval.x+'_0',true);
            rq.send();
            if (zoomval.x == 0 )
            {
                zoomval.time = now + 4100;
            }
            else
            {
                zoomval.time = now + 153;
            }

        }
    }
    function DoRefresh() {
    if (lastDown)
    {
        if (--repeats <= 0)
        {
            lastDown = false;       
        }
        SendTouch(lastId,lastDown);
    }
    if (++divctr > refDiv)
    {
        divctr =0;
        var request = new XMLHttpRequest();
        nocache = "&nocache=" + Math.random() * 1000000;
        request.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {
          if (this.responseText != null) {
            
            let ledStates = this.responseText.split('~');
            let elements  = document.getElementsByClassName('bK');
            for(var index= 0; index < elements.length; index++)
            {
                let id = elements[index].id;
                id = Number(id.split('~')[0]);
                if (id >= ledStates.length)
                {
                    console.log("Error string length: "+ this.responseText);
                    break;
                }
                else
                {
                    var bg,bgdim;
                    var ledState = ledStates[id];
                    if (ledState[1] == 'w') 
                    {
                        bg = "#FFFFFF";
                        bgdim = "#444444";
                    }
                    else if (ledState[1] == 'y') 
                    {
                        bg = "#e7f218";
                        bgdim = "#626611";
                    }
                    else if (ledState[1] == 'g')  
                    {
                        bg = "#09ed1c";
                        bgdim = "#04520b";
                    }
                    else if (ledState[1] == 'b') 
                    {
                        bg = "#3c78f0";
                        bgdim = "#113170";
                    }
                    else
                    {
                        bg = "#f21f0c";
                        bgdim = "#700d04";
                    }
                    if (ledState[0] == '1' )
                    {
                        elements[index].style.animationDuration = '0s';
                        elements[index].style.backgroundColor = bg;
                    }
                    else if (ledState[0] == '2')
                    {
                        elements[index].style.animationDuration = '0.6s';
                        elements[index].style.backgroundColor = bg;
                    }
                    else
                    {
                        elements[index].style.animationDuration = '0s';
                        elements[index].style.backgroundColor = bgdim;
                    }
                }
            }
    
        }}}}
        request.open("GET", "ajax_switch" + nocache, true);
        request.send(null);
        }
    }    
    function sendConf(event){
    event.preventDefault();
    let cEl = event.currentTarget;
    let rq = new XMLHttpRequest();
     rq.open("GET", "CoNf"+ cEl[0].id + "="+ cEl[0].value , false);
     rq.send();
     GetConf();
    }
</script>
)rawliteral";


const char html_form_top[] PROGMEM = R"rawliteral(
  <form <font size=3><b>
)rawliteral";

const char html_form_tail[] PROGMEM = R"rawliteral(
    <input type="submit" value="Submit"  >
  </form><br>
)rawliteral";

// sends all the config items as editable boxes, click and change the values
void sendEditableConfigPage(t_configPage pageType)
{
    //printTime(true);Serial.printf(F("main config\n"));
//printTime(true);Serial.printf(F("SEND EDITABLE PAGE =========\n"));
    ubyte minItem = 0;
    ubyte maxItem = CID_LAST_ITEM;
    switch (pageType)
    {
        default : 
        case TCONF_CONFIG:
            txPrint_P(PSTR("<h3>Config page"));
            maxItem = CID_FIRST_PR_NAME;
            break;
        case TCONF_ATEM_PRESETS:
            txPrint_P(PSTR("<h3>ATEM Studio preset names"));
            minItem = CID_FIRST_PR_NAME;
            maxItem = CID_ATEM_PR8+1;
            break;
        case TCONF_CAMERA1_PRESETS:
            txPrint_P(PSTR("<h3>Camera 1 preset names"));
            minItem = CID_CAM1_PR1;
            maxItem = CID_CAM2_PR1;
            break;
        case TCONF_CAMERA2_PRESETS:
            txPrint_P(PSTR("<h3>Camera 2 preset names"));
            minItem = CID_CAM2_PR1;
            break;
    }
    txPrint_P(PSTR(". Edit each item and click submit when done</h3>"));


    ubyte item = minItem;
    do
    {
        ubyte type = getConfType(0, item);
        if (type == CTYPE_NOT_USED)
        {
            continue;
        }
        switch (item)
        {
            case CID_IP_ADDRESS_ATEM :
                txPrint_P(PSTR("<h3>Ipaddresses. Edit the address as 111.222.033.044 etc and click submit</h3>"));
                break;
            case CID_ATEM_CAMERAS:
                txPrint_P(PSTR("<h3>Edit a string of Y or N to indicate which ATEM channels have a camera. EG:</h3>"));
                txPrint_P(PSTR("<p>12345678<br>nnYnYnnn means channels 3 & 5 have cameras.</p>"));
                break;
            case CID_JOYSTICK_DEAD_ZONE:
                txPrint_P(PSTR("<h3>Set the joystick deadzone in percent.</h3>"));
                break;
            case CID_SCREEN_DOWN_TIME:
                txPrint_P(PSTR("<h3>Set the time in seconds to wind the screen down to its ideal height.</h3>"));
                break;
            case CID_SQ6_SCENE:
                txPrint_P(PSTR("<h3>This section sets the startup scene and delay time for the SQ6 audio console.<br>The scene is requested 'WAIT' secs after power on</h3>"));
                break;                
            case CID_IRIS_MIN_PERCENT:
                txPrint_P(PSTR("<h3>This section is the paddle switches for IRIS and FOCUS. Each switch has three values:</h3>"));
                txPrint_P(PSTR("a) The speed of change on first click.<br>"));
                txPrint_P(PSTR("b) How long to hold before the speed increases<br>"));
                txPrint_P(PSTR("c) How long to accellerate to max rate of change"));
                break;

            default :
                break;
        }


        txPrint_P(html_form_top);
        getConfName(HTTP_req,item);
        txPrint(HTTP_req);
        txPrint_P(PSTR(" (type is: "));
        getConfType(HTTP_req,item);
        txPrint(HTTP_req);
        txPrint_P(PSTR(")</b></font><font size=2><br>"));
        
        int min = getConfMin(item);
        int max = getConfMax(item);
        if (min != max)
        {
            sprintf_P(HTTP_req, PSTR("(min/max) %d to %d : "),min,max);
            txPrint(HTTP_req);
        }

        sprintf_P(HTTP_req, PSTR("</font><input type=\"text\" id=\"%d\" value=\" \""),item);
        txPrint(HTTP_req);
        if (type == CTYPE_PRESET_TEXT)
        {
            // longer box to enter a longer name
            txPrint_P(PSTR(" size=\"40\">"));
        }
        else
        {
            txPrint_P(PSTR(">"));
        }

        txPrint_P(html_form_tail);
        txPrint_P(PSTR("\n"));
        txFlush();
    }while (++item < maxItem);


}
 



void sendCamPTZPage(void)
{
    char* buffer;
    buffer = HTTP_req; // the recieve buffer not needed whilst we're doing this
    txPrint_P(PSTR("<h4>Camera Pan Tilt Zoom Page.</h4>"));
    txPrint_P(PSTR("<div class=\"ptz-container\">"));
    // do the header row grid row
    txPrint_P(PSTR("<div class=\"g-item\">"));
    txPrint_P(PSTR("<span>ZOOM</span>"));
    txPrint_P(PSTR("</div>"));
    txPrint_P(PSTR("<div class=\"g-item\">"));
    txPrint_P(PSTR("<span>PAN and TILT Joystick</span>"));
    txPrint_P(PSTR("</div>"));

    
    // do the zoom slider in the first grid cell
    txPrint_P(PSTR("<div class=\"g-item\">"));
    txPrint_P(PSTR("<span>Tele<br></span>"));
    txPrint_P(PSTR("<input type=\"range\" id=\"zoom\" data-lpignore=\"true\" class= \"slider aNa\" min=\"-99\" max=\"99\" value=\"0\">"));
    txPrint_P(PSTR("<span><br>Wide</span>"));
    txPrint_P(PSTR("</div>"));
    
    // do the pan tilt joystick
    txPrint_P(PSTR("<div class=\"g-item\"> "));
    txPrint_P(PSTR("<div id=\"js-container\" >"));
       txPrint_P(PSTR("<div id=\"joystick\" class=\"aNa\"> "));
       txPrint_P(PSTR("<span class=\"dot\" id=\"knob\"> </span>"));
       txPrint_P(PSTR("</div>"));
    txPrint_P(PSTR("</div>"));
    
    txPrint_P(PSTR("</div>"));
    txPrint_P(PSTR("</div>"));

}

void sendMiscIOPage(void)
{
    char buttonid = KEYCODE_PROJECTOR;
    char ledNum;
    char* buffer;
    buffer = HTTP_req; // the recieve buffer not needed whilst we're doing this
    txPrint_P(PSTR("<h4>Miscellaneous Items Page.</h4>"));
    do
    {
        switch (buttonid)
        {
            case KEYCODE_PROJECTOR:
                ledNum= LED_PROJ;
                break;
            case KEYCODE_SCREEN :
                ledNum= LED_SCREEN;
                break;
            case KEYCODE_MP3:
                ledNum= LED_BLUETOOTH_POWER;
                break;
            case KEYCODE_MISC1:
                ledNum= LED_MISC1;
                break;
            case KEYCODE_MISC2:
                ledNum= LED_MISC2;
                break;
            default:    
            case KEYCODE_SPARE:
                ledNum= LED_SPARE;
                break;

        }

        txPrint_P(PSTR("<button style=\"font-size:17px\" class=\"bK tB cK \" id=\""));
        sprintf_P(buffer, PSTR("%d~%d~0\"> "),ledNum,buttonid);    // led number~keycode~configid
        txPrint(buffer);
        getInputName(buffer,buttonid);
        txPrint(buffer);
        txPrint_P(PSTR("</button>"));
        if ((buttonid & 0x01) == 0)
        {
            txPrint_P(PSTR("<br>"));

        }

    } while (++buttonid <= KEYCODE_SPARE);
    txFlush();
}


void sendCamPresetsPage(void)
{
    char* buffer;
    buffer = HTTP_req; // the recieve buffer not needed whilst we're doing this
    txPrint_P(PSTR("<h4>Camera Presets Page.</h4>"));
    txPrint_P(PSTR("<div class=\"cam-g-container\">"));
    for (ubyte row =0; row < MAX_CAMERA_PRESETS +1; row++)
    {
        for (ubyte col =0; col < 3; col++)
        {
            txPrint_P(PSTR("<div class=\"g-item\">"));
            if (row == 0)
            {
                // header row with camera select
                if (col ==0)
                {
                    txPrint_P(PSTR("<button class=\"butt cK\" id=\""));
                    sprintf_P(buffer, PSTR("0~%d~0\"></button>"),KEYCODE_CAMERA_SELECT);
                    txPrint(buffer);
                    txPrint_P(PSTR("<button class=\"lab\"> Select Camera"));
                }
                else
                {
                    txPrint_P(PSTR("<button class=\"bK led\" id=\""));
                    sprintf_P(buffer, PSTR("%d~0\"</button>"),LED_CAMERA1-1 +col);
                    txPrint(buffer);
                    sprintf_P(buffer, PSTR("<button class=\"lab\"> Camera<br> %d"),col);
                    txPrint(buffer);
                }
            }
            else
            {
                // camera preset row
                if (col ==0)
                {
                    txPrint_P(PSTR("<button class=\"butt cK\" id=\""));
                    sprintf_P(buffer, PSTR("0~%d~0\"> "),(row-1) + KEYCODE_CAMERA_PRESET1);
                }
                else
                {
                    txPrint_P(PSTR("<button class=\"lab cF\" id=\""));
                    ubyte confId = (row-1) + (col-1)*7 + CID_CAM1_PR1;
                    sprintf_P(buffer, PSTR("0~0~%d\"> "), confId);
                }
                txPrint(buffer);
            }    
            txPrint_P(PSTR("</button></div>"));
        }
        
        // column 0 is the button, column 1 is camera1 preset, col2 is cam2 preset
        // button

    }
    txPrint_P(PSTR("</div>"));
    txFlush();
}




// sends all the config items as editable boxes, click and change the values
void sendAtemPage()
{

    char* buffer;
    buffer = HTTP_req; // the recieve buffer not needed whilst we're doing this
 
    txPrint_P(PSTR("<h4>ATEM Presets Page. </h4>"));

    ubyte ledNum = LED_ATEM_PRESET1;
    char buttonid = KEYCODE_ATEM_PRESET1;
    ubyte itemCtr = 0;
    do
    {

        if (ledNum == LED_FTB)
        {
            txPrint_P(PSTR("<button class=\"bK tB cK\" id=\""));
            sprintf_P(buffer, PSTR("%d~%d"),ledNum,KEYCODE_FTB);
            txPrint(buffer);
            txPrint_P(PSTR("\"> <buthdr>FTB<br>(Fade to Black) </buthdr></button>"));

        }
        else if (ledNum == LED_ATEM_ENET_OK)
        {
            txPrint_P(PSTR("<button class=\"bK led\" id=\""));
            sprintf_P(buffer, PSTR("%d~0"),ledNum);
            txPrint(buffer);
            txPrint_P(PSTR("\"> </button>"));

            txPrint_P(PSTR("<button class=\"label\" id=\""));
            sprintf_P(buffer, PSTR("%d~0"),ledNum);
            txPrint(buffer);
            txPrint_P(PSTR("\"> ATEM<br>Connected </button>"));

        }
        else
        {
            txPrint_P(PSTR("<button class=\"bK tB cK cF\" id=\""));
            sprintf_P(buffer, PSTR("%d~%d~%d\"> </button>"),ledNum,    // led number~keycode~configid
                (ledNum- LED_ATEM_PRESET1) + KEYCODE_ATEM_PRESET1, 
                (ledNum- LED_ATEM_PRESET1) + CID_ATEM_PR1);
            txPrint(buffer);

        }
        if (++itemCtr > 1)
        {
            itemCtr = 0;
            txPrint_P(PSTR("<br>"));

        }

    }while (++ledNum <=LED_ATEM_ENET_OK);
    char *ptr;
    ptr = getRevString();
    txPrint_P(ptr);
    ptr = getCompileString();
    txPrint_P(ptr);
    //txPrint_P(compileDate);
    txFlush();

}



void initWebServer(void)
{

    printModuleStartup(PSTR("Webserver Module"),WEB_CPP_VER,WEB_H_VER,lastsave);
    Serial.printf(F("\t"));
    printIPAddress(CID_IP_ADDRESS_CONSOLE);

    server.begin();
}



void printfLarge(char *ptr_P)
{
    boolean keepGoing = true;
    char buffer[50];
    ubyte loopCtr = 0;
    do
    {
        memcpy_P(buffer,ptr_P, 45);
        ptr_P += 45;
        Serial.printf(buffer);
        if (strlen(buffer) < 45)
        {
            break;
        }
        buffer[45] = '\0';

    }while (++loopCtr < 5);
}

#define LEDRED 0
#define LEDBLUE 8
#define LEDGREEN 16
#define LEDYELLOW 24
#define LEDWHITE 32

static void GetLeds()
{
    ubyte ledId = 0;

    char *buff;
    buff = HTTP_req;

    ubyte ledColor;
    do
    {
        *buff++ = getLEDState(ledId) + '0';
        if (ledId <= LED_FTB)
        {
            *buff++ ='r';
        }
        else if (ledId == LED_SPARE)
        {
            *buff++ ='w';
        }
        else if (ledId == LED_SCREEN)
        {
            *buff++ ='y';
        }
        else if (ledId == LED_MP3 || ledId == LED_BLUETOOTH_POWER)
        {
            *buff++ ='b';
        }
        else
        {
            *buff++ ='g';
        }
        *buff++ ='~';

        /* code */
    } while (++ledId < LED_LAST_LED);
    *buff++ = '\0';
    txPrint(HTTP_req);
}




extern ubyte webKey;

static void handleAnalog(char *input)
{
    // the string should be idname_x_y  where x and y will be -100 to +100
    char *ptr;
    int x;
    int y;

    ptr = strstr_P(input, PSTR("zoom_" ));
    if (ptr != NULL)
    {
        ptr += 5;
        sscanf(ptr,"%d_",&x);
        if (x > -100 && x < 100)
        {
            doWebZoom(x);
        }
    }
    ptr = strstr_P(input, PSTR("joystick_" ));
    if (ptr != NULL)
    {
        ptr += 9;
        sscanf(ptr,"%d_%d",&x,&y);
        if (x > -100 && x < 100 && y > -100 && y < 100)
        {
            doWebPan(x);
            doWebTilt(y);
        }
    }

}

static void handleMouseAction(char *input)
{
    //printTime(true);dump(input, 30);
    static ubyte lastWebClick = 250;
    static boolean down;

    char *ptr;
    ptr = strstr_P(input, PSTR("reboot" ));
    if (ptr != NULL)
    {
        reboot(20); // reboot in 20 ticks
        return;
    }
    // led command -= get the id
    int id = 250; // illegal number
    sscanf(input,"%d",&id);
    if (id < KEYCODE_LAST_VALUE)
    {
        handleWebKey(id, (strstr_P(input, PSTR("true" )) != NULL)?true:false);
    }
}

void GetConf(void)
{
    ubyte item = 0;
    char *dest;
    do
    {
        dest = HTTP_req;
        dest += getConfValueStr (dest,item);
        *dest++ = '~';
        *dest++ = '\0';
        txPrint(HTTP_req);
    } while (++item < CID_LAST_ITEM);
}
void printDiff(boolean clear=false)
{
    static uint16_t time;
    if (clear)
    {
        time = millis();
        return;
    }
    Serial.printf(F("%04dms: "),(uint16_t)(millis()-time));
    time = millis();
 
}
void handleRequest()
{
    boolean handled = false;
    char *ptr = strstr_P(HTTP_req, PSTR("favicon.ico" ));
    if (ptr != NULL)
    {
        // ignore this request
        txPrint_P(PSTR("\n"));
        txFlush();//
        return;
    }
 
    ptr = strstr_P(HTTP_req, PSTR("aNa=" ));
    if (ptr != NULL)
    {
       //printTime(true);dump(HTTP_req, 40);
       //printNewLine();
       handleAnalog(ptr+4);
       return;
    }
 
    ptr = strstr_P(HTTP_req, PSTR("pAg3" ));
    if (ptr != NULL)
    {
        //Serial.printf(F("Page open\n"));
        //dump(HTTP_req,40);
        return;
    }

 
    ptr = strstr_P(HTTP_req, PSTR("ajax_switch" ));
    if (ptr != NULL)
    {
        GetLeds();
//printDiff();Serial.printf(F("Got leds\n"));
        return;

    }

    ptr = strstr_P(HTTP_req, PSTR("ajax_conf" ));
    if (ptr != NULL)
    {
        GetConf();
        return;

    }

    ptr = strstr_P(HTTP_req, PSTR("m0usE" ));

    if ( ptr != NULL)
    {
        handleMouseAction(ptr+5);
        return;
    }

 
    ubyte id = 250; // an illegal number
 
    if (!handled )
    {
        
        // the reply may contain the conf identifier with a new value for something
        char *ptrBeg = strstr_P(HTTP_req, PSTR("/CoNf" ));
        if ( ptrBeg != NULL)
        {
            handled = true;
            ptrBeg += 5; // skip the string we jkust searched
            char *ptrEnd = strstr_P(ptrBeg, PSTR(" HTTP" ));
            if (ptrEnd != NULL )
            {
                *ptrEnd = '\0';
                // we now hopefully have a string something like 12=sometext. we need the number and the sometext
                sscanf(ptrBeg,"%d",&id);
                if (id < CID_LAST_ITEM)
                {
                    ptrBeg = strstr_P(ptrBeg, PSTR("=" ));
                    if (ptrBeg != NULL)
                    {
                        ++ptrBeg;
                       // printDiff();Serial.printf("id%d setting to %s\n",id,ptrBeg);
                        checkAndSaveConfString(ptrBeg, id, true);
                    }

                }


            }
        }

    }

    if (!handled)
    {
        //txReset();
        txFlush();
        txPrint_P(html_header_top);
        txPrint_P(html_cssblink_style);
        txPrint_P(html_buthdr_style);
        txPrint_P(html_head_ajax_script);
        txPrint_P(html_head_ajax_conf);
        txPrint_P(html_openpage);

        txPrint_P(PSTR("<body onload=\"doOnLoad()\">"));
        txPrint_P(html_header_tail);
        txPrint_P(PSTR("<button onmousedown=\"OpenPage('atemcontrol')\" >Choose ATEM Preset</button>"));
        txPrint_P(PSTR("<button onmousedown=\"OpenPage('cameracontrol')\" >Choose Camera Preset</button>"));
        txPrint_P(PSTR("<button onmousedown=\"OpenPage('cameraptz')\" >Camera Pan Tilt Zoom</button>"));
        txPrint_P(PSTR("<button onmousedown=\"OpenPage('miscio')\" >Misc IO items</button>"));
        txPrint_P(PSTR("<button onmousedown=\"OpenPage('atempresets')\" >Edit ATEM preset names</button>"));
        txPrint_P(PSTR("<button onmousedown=\"OpenPage('camerapresets')\" >Edit Camera preset names</button>"));
        txPrint_P(PSTR("<button onmousedown=\"OpenPage('config')\" >Edit System Config</button>"));
        txPrint_P(PSTR("<button onmousedown=\"OpenPage('reboot')\" >Restart System</button>"));

        txPrint_P(PSTR("<div id=\"atemcontrol\" class=\"t_c qu\" "));
        txPrint_P(PSTR("style=\"background-image: linear-gradient(#e6f2ff,#3392ff)\">"));

        sendAtemPage();
        txPrint_P(PSTR("</div>"));

        txPrint_P(PSTR("<div id=\"cameracontrol\" class=\"t_c qu\" "));
        txPrint_P(PSTR("style=\"background-image: linear-gradient(#66f2ff,#77FF11)\">"));
        sendCamPresetsPage();
        txPrint_P(PSTR("</div>"));

        txPrint_P(PSTR("<div id=\"cameraptz\" class=\"t_c\" "));
        txPrint_P(PSTR("style=\"background-image: linear-gradient(#55d2ff,#88dd11)\">"));
        sendCamPTZPage();
        txPrint_P(PSTR("</div>"));

        txPrint_P(PSTR("<div id=\"miscio\" class=\"t_c qu\" "));
        txPrint_P(PSTR("style=\"background-image: linear-gradient(#c6e2f0,#3382af)\">"));
        sendMiscIOPage();
        txPrint_P(PSTR("</div>"));

        txPrint_P(PSTR("<div id=\"atempresets\" class=\"t_c\" "));
        txPrint_P(PSTR("style=\"display:none;background-image: linear-gradient(#ccffcc,#80ff80)\">"));

        sendEditableConfigPage( TCONF_ATEM_PRESETS);
        txPrint_P(PSTR("</div>"));
        txFlush();//


        txPrint_P(PSTR("<div id=\"camerapresets\" class=\"t_c\" "));
        txPrint_P(PSTR("style=\"display:none;background-image: linear-gradient(#e6fff9,#66ffdb)\">"));
        sendEditableConfigPage( TCONF_CAMERA1_PRESETS);
        sendEditableConfigPage( TCONF_CAMERA2_PRESETS);
        txPrint_P(PSTR("</div>"));

        txPrint_P(PSTR("<div id=\"config\" class=\"t_c\" "));
        txPrint_P(PSTR("style=\"display:none;background-image: linear-gradient(#ffe6e6,#ff8080)\">"));
        sendEditableConfigPage( TCONF_CONFIG);
        txPrint_P(PSTR("</div>"));
        txPrint_P(setup_clicks_script );

        txPrint_P(PSTR("</body></html>"));
        txFlush();//
 
    }
}
ubyte getDecHex(char input)
{
    if (input >= '0' && input <= '9')
    {
        return input -'0';
    }
    else
    {
        input = toupper(input);
        input -= 'A';
        input += 10;
        return input;
    }
}

void webServerLoop()
{
    ethcl = server.available();  // try to get client
    if (ethcl) {  // got client?
        ethcl.setConnectionTimeout(50);
        boolean currentLineIsBlank = true;
        while (ethcl.connected()) 
        {

            if (ethcl.available()) {   // client data available to read

                char c = ethcl.read(); // read 1 byte (character) from client
                if (HTTPOffset <MAXHTML)
                {
                    if (c == '+')c = ' ';
                    if (c == '%')
                    {
                        // need to decode a hex character
                        c = getDecHex(ethcl.read());
                        c *= 16;
                        c += getDecHex(ethcl.read());
                    }
                    HTTP_req[HTTPOffset++] = c;
                    HTTP_req[HTTPOffset] = '\0';
                }
                // last line of client request is blank and ends with \n
                // respond to client only after last line received
                if (c == '\n' && currentLineIsBlank) {
                    // send a standard http response header
                    txReset();
                    txPrint_P(PSTR("HTTP/1.1 200 OK"));
                    txPrint_P(PSTR("Content-Type: text/html"));
                    txPrint_P(PSTR("Connection: keep-alive\n\n"));
                    //Serial.printf(F("\nStandard html reposne header\n"));
                    txFlush();
                    handleRequest();
                    // display received HTTP request on serial port
                    //Serial.print(HTTP_req);
                    HTTPOffset = 0; // finished with request, empty string
                    txFlush();
                    break;
                }
                // every line of text received from the client ends with \r\n
                if (c == '\n') {
                    // last character on line of received text
                    // starting new line with next character read
                    currentLineIsBlank = true;
                } 
                else if (c != '\r') {
                    // a text character was received from client
                    currentLineIsBlank = false;
                }
            } // end if (client.available())
        } // end while (client.connected())
        delay(1);      // give the web browser time to receive the data

        ethcl.stop(); // close the connection
    } // end if (client)
}


/* Todo 26/10/2023


Add rest of camera functionality iris and focus and also camera connected led
Tidy up repeated HTML and CSS  and script code to make initial load much quicker
Also to make it more readable


*/
/*

queueanalog
puts the value for this id in the last value for this id
if value is now 0 and was non zero send straight away
if value is now non zero and was 0 send straight away
calls refreshana

sendanalogue(id)
Does a send and timestanps it

refreshana()
Called every 75ms or so
Depends on values whethe rit sends after 75ms or after 4000
Calls send and then sets refresh slow or fast depending on value




*/
