#include "globals.h"
PROGMEM const char lastsave[] ="Last saved 2024-08-05 16:17:40";
#define LED_CPP_VER 3
/*
    Thios contains most code for the buttons and leds user interface panel
	Last change: AC 12/04/2020 17:18:19
*/


#include <SkaarhojPgmspace.h>

#include "leddriver.h"
#include "camerautils.h"
#include "atemutils.h"
#include <EEPROM.h>
#include "utilities.h"
#include <wire.h>
#include "config.h"


// if this is defined we output a message on seriual whenever any call to setLEDMode
// OR setNonBusyLED IS CALLED
//#define DEBUG_LEDS


#define MAX_LED_NAME_LEN 18
const char PROGMEM ledNames[LED_LAST_LED][MAX_LED_NAME_LEN]
{
    { "ATEM Preset1"},
    { "ATEM Preset2"},
    { "ATEM Preset3"},
    { "ATEM Preset4"},
    { "ATEM Preset5"},
    { "ATEM Preset6"},
    { "ATEM Preset7"},
    { "ATEM Preset8"},
    { "FadeToBlack"},
    { "ATEM connected"},
    { "Camera connected"},
    { "Camera1 Selected"},
    { "Camera2 Selected"},
    { "Misc1"},
    { "Misc2"},
    { "Auto IRIS"},
    { "Auto FOCUS"},
    { "MP3 Power"},
    { "Proj power"},
    { "Screen out&down"},
    { "Spare Led"},
    { "Bluetooth power"},
};


// helper functions - may also be used for webserver eventually
static void getLedName(tLED_ids ledID, char* buffer)
{
    int length = strlen_P(ledNames[ledID]);
    if (length >0 && length <= MAX_LED_NAME_LEN)
    {
        sprintf_P(buffer,ledNames[ledID] );
    }
    else
    {
        *buffer ='\0';
    }
}


static void getLedStateString(ubyte state, char *buffer)
{
    *buffer ='\0';
    bool flashing = true;
    switch ( state & (0x07<< LEDMODE_PATTERN_BIT_SHIFT))
    {
        case LEDMODE_PATTERN_5050:
            // flash on and off 50 50
            buffer +=  sprintf_P(buffer,PSTR("flashing 50/50 ") );
            break;
        case LEDMODE_PATTERN_12PERCENT:
            buffer +=  sprintf_P(buffer,PSTR("flashing 12%% " ) );
            break;

        case LEDMODE_PATTERN_25PERCENT:
            buffer +=  sprintf_P(buffer,PSTR("flashing 25/75 " ) );
            break;
        case LEDMODE_PATTERN_GROW:
            // grow from on a bit to on a lot and repeat
            buffer +=  sprintf_P(buffer,PSTR("growing flash ") );
            break;
        case LEDMODE_PATTERN_ON:
            // static LED ON
            buffer +=  sprintf_P(buffer,PSTR("static ON") );
            flashing = false;
            break;
        default:
            buffer +=  sprintf_P(buffer,PSTR("static OFF") );
            flashing = false;
            break;
    }
    if (flashing)
    {
        switch ( state & (0x0f << LEDMODE_SPEED_BIT_SHIFT))
        {
            case LEDMODE_SPEED_VERY_SLOW:
                buffer +=  sprintf_P(buffer,PSTR("very slow") );
                break;
            case LEDMODE_SPEED_SLOW:
                buffer +=  sprintf_P(buffer,PSTR("slow") );
                break;

            default :
                buffer +=  sprintf_P(buffer,PSTR("fast") );
                // no action required
                break;
            }

    }
    if ((LEDMODE_INVERTED & state) != 0)
    {
        buffer +=  sprintf_P(buffer,PSTR("(inverted)") );
    }



}


static ubyte ledTimer; // incremented every 75ms and used for flashing leds


// timers used for controlling flashing and other timed LED functions
static ubyte LEDtimers[LED_LAST_LED];
static ubyte LEDstates[LED_LAST_LED];


// A store of led statuses. These are a cache of the 3 x I2C output devices
// driving the leds. The bits are in order 0-23 in a long varaiable
// The bits are set and reset and an I2C update performed to make the led state
// match the cache.
static unsigned long ledCache;
static boolean ledCacheDirty;


// This table is a bitlist lookup and should be in the same order as the enum LED_ids
// This enables the lookup between the LED ID and the i2c bit as a 24 bit value
// this gives the bits in order of the 3off eight bit I2C expanders the leds are connected to
// 0-7 are on expander 1, 8-15 on expander 2 etc

static const ubyte ledOutputs[] PROGMEM =
//static const ubyte ledOutputs[] =
{
    I2C_PIN_LED_ATEM_PRESET1,     //  LED_ATEM_PRESET1,
    I2C_PIN_LED_ATEM_PRESET2,     //  LED_ATEM_PRESET2,  
    I2C_PIN_LED_ATEM_PRESET3,     //  LED_ATEM_PRESET3,  
    I2C_PIN_LED_ATEM_PRESET4,     //  LED_ATEM_PRESET4,  
    I2C_PIN_LED_ATEM_PRESET5,     //  LED_ATEM_PRESET5,  
    I2C_PIN_LED_ATEM_PRESET6,     //  LED_ATEM_PRESET6,  
    I2C_PIN_LED_ATEM_PRESET7,     //  LED_ATEM_PRESET7,  
    I2C_PIN_LED_ATEM_PRESET8,     //  LED_ATEM_PRESET8,  
    I2C_PIN_LED_FTB,              //  LED_FTB,           
    I2C_PIN_LED_ATEM_OK,          //  LED_ATEM_ENET_OK,
    I2C_PIN_LED_CAMERA_OK,        //  LED_CAMERA_OK,     
    I2C_PIN_LED_CAMERA1,          //  LED_CAMERA1,       
    I2C_PIN_LED_CAMERA2,          //  LED_CAMERA2,       
    I2C_PIN_LED_MISC1,            //  LED_MISC1,
    I2C_PIN_LED_MISC2,            //  LED_MISC2,
    I2C_PIN_AUTO_IRIS,            //  LED_AUTO_IRIS,
    I2C_PIN_AUTO_FOCUS,           //  LED_AUTO_FOCUS,
    I2C_PIN_LED_MP3,               //  LED_MP3
    I2C_PIN_LED_PROJ,             //  LED_PROJ,
    I2C_PIN_LED_SCREEN,             //  LED_SCREEN,
    I2C_PIN_LED_SPARE,            //  LED_SPARE,
    I2C_PIN_BLUETOOTH_POWER,            //  BLUE,
};


// Returns true if the LED is busy doing a timed function
boolean isLEDTimerBusy(tLED_ids ledID)
{
    return (LEDtimers[ledID]==0)?false:true;
}


// Returns true if the LED is flashing or doing a timed pulse
boolean isLEDActive(tLED_ids ledID)
{
    return  (LEDtimers[ledID]!=0) ||
            ((LEDstates[ledID] & (0x07 << LEDMODE_PATTERN_BIT_SHIFT) ) != 0);

}

// returns 0 if OFF, 1 if flashing and 2 if steady ON at the instant of the call
ubyte getLEDState(tLED_ids ledID)
{
    if (LEDstates[ledID] == LEDMODE_PATTERN_OFF)
    {
        return 0;
    }
    else if (LEDstates[ledID] == LEDMODE_PATTERN_ON)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}



#ifdef zzzxxx




// Debug fucntion. Called every 2-3 minutes. Checks for corruption of I2C
// direction settings. 29/3/2018. Still not found the bug that screws them.
void checkI2CSettings(void)
{
    ubyte data;
    Wire.beginTransmission(I2C_OUT0_ADD);
    Wire.write(0);             // Reg 0 is the GPIO dir g
    Wire.endTransmission();     // stop transmitting
    Wire.requestFrom(I2C_OUT0_ADD, 1);
    data = Wire.read();

    if ( data != 0 )
    {
        printTime(true);
        Serial.printf(F("ERROR I2C0 setup is now 0x%0x\n\r"),data);
    }

    Wire.beginTransmission(I2C_OUT1_ADD);
    Wire.write(0);             // Reg 0 is the GPIO dir g
    Wire.endTransmission();     // stop transmitting
    Wire.requestFrom(I2C_OUT1_ADD, 1);
    data = Wire.read();

    if ( data != 0 )
    {
        printTime(true);
        Serial.printf(F("ERROR I2C1 setup is now 0x%0x\n\r"),data);
    }

    Wire.beginTransmission(I2C_OUT2_ADD);
    Wire.write(0);             // Reg 0 is the GPIO dir g
    Wire.endTransmission();     // stop transmitting
    Wire.requestFrom(I2C_OUT2_ADD, 1);
    data = Wire.read();

    if ( data != 0 )
    {
        printTime(true);
        Serial.printf(F("ERROR I2C2 setup is now 0x%0x\n\r"),data);
    }
}
#endif

static void updateLedCache(boolean LEDOn, ubyte bitNo)
{
    unsigned long oldCache = ledCache;
    static uint16_t timeStamp;
 
    if ( LEDOn == true)
    {
        ledCache |= (0x01L << bitNo);
    }
    else
    {
        ledCache &= ~(0x01L << bitNo);
    }
    if ( (uint16_t)((uint16_t) millis() -timeStamp) > 10000)
    {
        // force an update evry 10 secs or so just to ensure the LEDs dont' get left behind
        // there is/was a bug where and I2C write occasionally goes wrong
        timeStamp = millis();
        // may also reset up the i2c buffers, I2C0 gets screwed from time to time
        ledCacheDirty = true;
    }

    if (ledCache != oldCache)
    {
        timeStamp = millis();
        ledCacheDirty = true;
    }
}


static void apcdump(tLED_ids ledIdentity,ubyte thisLedTimer, ubyte ledTimer,ubyte thisState)
{
    static long oldMillis;
    if ( ledIdentity == LED_FTB )
    {
        printTime(true);

        Serial.printf(F("%02d this %d min %d state 0x%02x\n\r"),
        (ubyte)(millis() - oldMillis),thisLedTimer, ledTimer,thisState);
        oldMillis= millis();
    }
}
static void updateLED(tLED_ids ledIdentity, boolean decrementTime)
{
    boolean LEDOn = false;
    ubyte thisLed = (ubyte)ledIdentity;
    ubyte thisState = LEDstates[thisLed];
 //   static uint16_t timeStamp;
    if ( decrementTime && LEDtimers[thisLed] > 0 && --LEDtimers[thisLed] == 0)
    {
        LEDstates[thisLed] &= LEDMODE_CLEAR_ALL;
        thisState = LEDstates[thisLed];
    }
    else
    {
        ubyte thisLedTimer = ledTimer;
        switch ( thisState & (0x0f << LEDMODE_SPEED_BIT_SHIFT))
        {
            case LEDMODE_SPEED_VERY_SLOW:
                thisLedTimer >>= 2;
                break;
            case LEDMODE_SPEED_SLOW:
                thisLedTimer >>= 1;
                break;

            default :
                // no action required
                break;
        }
//apcdump(ledIdentity,thisLedTimer,ledTimer,thisState);

        switch ( thisState & (0x07<< LEDMODE_PATTERN_BIT_SHIFT))
        {
            case LEDMODE_PATTERN_5050:
                // flash on and off 50 50
                if ( (thisLedTimer & 0x01) != 0 )
                {
                    LEDOn = true;
                }
                break;
            case LEDMODE_PATTERN_12PERCENT:
                if ( (thisLedTimer & 0x07) == 0x07 )
                {
                    LEDOn = true;
                }
                break;
            case LEDMODE_PATTERN_25PERCENT:
                if ( (thisLedTimer & 0x03) == 0x03 )
                {
                    LEDOn = true;
                }
                // flash on 25 and off 75
                break;
            case LEDMODE_PATTERN_GROW:
                // grow from on a bit to on a lot and repeat
                thisLedTimer = thisLedTimer%13;
                LEDOn = true;
                if ( thisLedTimer == 0 || thisLedTimer == 3 ||thisLedTimer == 6|| (thisLedTimer >= 9 && thisLedTimer <= 12) )
                {
                    LEDOn = false;
                }
                break;

            case LEDMODE_PATTERN_ON:
                // static LED ON
                LEDOn = true;
                break;

            default :
                // static LED OFF
                break;
        }
    }
    if ( (thisState & LEDMODE_INVERTED) != 0)
    {
        LEDOn = (LEDOn==true)?false:true;
    }
    // don't refresh the leds if test mode is running as we drive them directly

    if ( UITestMode != TEST_MODE_SCAN_LEDS && UITestMode != TEST_MODE_STEP_LEDS )
    {
        updateLedCache(LEDOn,pgm_read_byte_near(&ledOutputs[thisLed]));
//        updateLedCache(LEDOn,ledOutputs[thisLed]);
    }

}


static int ledDelay = 0;

void resetLEDTestTimer(void)
{
    ledDelay = 0;
}



//called every UI_UPDATEmS (nominally 75ms)
void manageLedsTick(void)
{
    boolean status;
    ledTimer++;
    if ( UITestMode == TEST_MODE_SCAN_LEDS || UITestMode == TEST_MODE_STEP_LEDS )
    {
        static ubyte thisTestLed = 0;
        if ( --ledDelay < 0 )
        {

            ledDelay = (UITestMode == TEST_MODE_SCAN_LEDS)? (550/UI_UPDATEmS):(5000/UI_UPDATEmS);
            updateLedCache(false,pgm_read_byte_near( ledOutputs +thisTestLed)); // turn off
            if ( ++thisTestLed >= (ubyte)LED_LAST_LED )
            {
                thisTestLed = 0;
            }
            updateLedCache(true,pgm_read_byte_near( ledOutputs +thisTestLed)); // turn on

        }
    }

    for (ubyte ledNumber = 0 ;ledNumber < (ubyte)LED_LAST_LED  ; ledNumber++ )
    {
        if ( isLEDActive((tLED_ids)ledNumber))
        {
            updateLED((tLED_ids)ledNumber, true);
        }
    }
    if ( ledCacheDirty == true )
    {
        ledCacheDirty = false;
        unsigned long longData = ledCache;
        ubyte data = longData & 0x0FF;
    
        Wire.beginTransmission(I2C_OUT0_ADD);
        Wire.write(9);             // Reg 9 is the data reg
        Wire.write(data);
        Wire.endTransmission();     // stop transmitting
    
        longData >>= 8;
        data = longData & 0x0FF;
        Wire.beginTransmission(I2C_OUT1_ADD);
        Wire.write(9);             // Reg 9 is the data reg
        Wire.write(data);
        Wire.endTransmission();     // stop transmitting
    
        longData >>= 8;
        data = longData & 0x0FF;
        Wire.beginTransmission(I2C_OUT2_ADD);
        Wire.write(9);             // Reg 9 is the data reg
        Wire.write(data);
        Wire.endTransmission();     // stop transmitting
    }

}


static serialReportLed(tLED_ids ledID, ubyte state, ubyte time)
{
    char buffer[70];
    getLedName(ledID, buffer);
    printTime(true);Serial.printf(F("Setting led%d: %s"),ledID,buffer);
    getLedStateString(state, buffer);
    Serial.printf(F(" to %s"),buffer);
    if (time != 0)
    {
        uint16_t time10ths = (time * UI_UPDATEmS)/100;
        Serial.printf(F(" for %d.%d seconds"),time10ths /10,time10ths %10 );
    }
    Serial.printf(F("\n"));

}

// A helper function to start an LED flashing or a timed or non timed on or OFF state
// The time is in led update chunks - see UI_UPDATEmS. A value of 5 would be
// 5 x UI_UPDATEmS or approx 375mS assuming UI_UPDATEmS is 75
void setLEDMode(tLED_ids ledID, ubyte state, ubyte time = 0)
{

    if ( ledID >= LED_LAST_LED)
    {
        Serial.printf(F("ERROR led number %d\n\r"),ledID);
    }
    else
    {

        boolean dirty = false;
        if ( LEDtimers[ledID] != time)
        {
            LEDtimers[ledID] = time;
            dirty = true;
        };
        if ( LEDstates[ledID] != state)
        {
            LEDstates[ledID] = state;
            dirty = true;
        };
        if ( dirty )
        {
            #ifdef DEBUG_LEDS
            serialReportLed(ledID, state, time);
            #endif

            updateLED(ledID, false);
        }
    }
}
// A helper function to start an LED flashing or a timed or non timed on or OFF state
// BUT ONLY if ( it isn't ) being timed already
// The time is in led update chunks - see UI_UPDATEmS. A value of 5 would be
// 5 x UI_UPDATEmS or approx 375mS assuming UI_UPDATEmS is 75

void setNonBusyLED(tLED_ids ledID, ubyte state, ubyte time = 0)
{
    if( isLEDTimerBusy(ledID) == false)
    {
       setLEDMode(ledID, state, time);
    }
}

// a quick way of setting all leds to the same state
void setAllLEDs(ubyte state, ubyte time)
{
    for (ubyte counter = 0 ; counter < (ubyte)LED_LAST_LED ; counter++)
    {
        if (counter != (ubyte)LED_BLUETOOTH_POWER)
        {    
            setLEDMode((tLED_ids)counter, state, time);
        }
    }

}

// a quick way of setting all camera selection leds to the smae state
void setAllCameraLeds(ubyte state, ubyte time)
{
    for ( ubyte ledNumber = 0; ledNumber < MAX_CAMERAS  ; ledNumber++ )
    {
        setLEDMode((tLED_ids)(ledNumber + (ubyte)LED_CAMERA1),state, time);
    }
}

// Init function
void initLeds(void)
{
    byte counter;

    printModuleStartup(PSTR("LED driver"),LED_CPP_VER,LED_H_VER,lastsave);

    #ifdef DEBUG_LEDS
    Serial.printf(F("Warning: Verbos DEBUG_LEDS is defined in leddrive.cpp\n"));
    #endif


    Serial.printf(F("\tSetting up i2c interface.."));
    Wire.begin(); // set up I2c as master
    Wire.setClock(400000); // at 400Khz

    // set I2C device 0x20 as radio inputs (not strictly part fo the led driver!)
    Serial.printf(F("Device 0x20 - "));
    Wire.beginTransmission(I2C_IN_ADD);
    Wire.write(0);             // Reg 0 IODIR
    Wire.write(0xFF);             // All inputs
    Wire.endTransmission();     // stop transmitting


    // set device 0x21 as outputs
    Serial.printf(F("Device 0x21 - "));
    Wire.beginTransmission(I2C_OUT0_ADD); // transmit to device #44 (0x2c)
    Wire.write(0);             // Reg 0 IODIR
    Wire.write(0);             // All outputs
    Wire.endTransmission();     // stop transmitting

    // set device 0x22 as outputs
    Serial.printf(F("Device 0x22 - "));
    Wire.beginTransmission(I2C_OUT1_ADD); // transmit to device #44 (0x2c)
    Wire.write(0);             // Reg 0 IODIR
    Wire.write(0);             // All outputs
    Wire.endTransmission();     // stop transmitting

    // set device 0x23 as outputs
    Serial.printf(F("Device 0x23 - "));
    Wire.beginTransmission(I2C_OUT2_ADD); // transmit to device #44 (0x2c)
    Wire.write(0);             // Reg 0 IODIR
    Wire.write(0);             // All outputs
    Wire.endTransmission();     // stop transmitting
    Serial.printf(F("\n\r\tI2C setup Completed\n\r\r"));

}

