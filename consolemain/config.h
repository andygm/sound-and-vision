/** @package 

    config.h
    
    Copyright(c) Microsoft 2000
    
    Author: Andrew Copsey
    Created: AC  27/03/2018 14:46:55
	Last change: AC 20/03/2020 13:04:40
*/
#ifndef _config_hh
#define _config_hh

#include <Ethernet.h>
#include "globals.h"


#define CONFIG_H_VER 5


#define MAX_ATEM_PRESETS 8

typedef enum t_SwitchConfigs
{
    SC_FOCUS,
    SC_IRIS,
    SC_ZOOM,  // actually not used
    SC_MAX_SWITCH_CONFIGS,
};


#define EEPROM_START_ADDRESS 0

#define MAX_CAMERAS 2
#define MAX_CAMERA_PRESETS 7
#define MAX_ATEM_INPUTS 8


typedef enum t_IPAddress
{
    IP_ADDRESS_ATEM,
    IP_ADDRESS_CONSOLE,
    IP_ADDRESS_CAMERA1,
    IP_ADDRESS_CAMERA2,
    IP_ADDRESS_PROJECTOR,
    IP_ADDRESS_SQ6,
    MAX_IP_ADDRESSES,
};


// this typedef is the same order the config will be presented onscreen - keep it logical
// if you change the order of this the table static const confSt allConf[] must be kept in the same order

typedef enum t_ConfId
{
    // keep the ipaddress types together and in this order as some code depends on it - yuk!
/* 0 */   CID_IP_ADDRESS_ATEM,
/* 1 */   CID_IP_ADDRESS_CONSOLE,
/* 2 */    CID_IP_ADDRESS_CAMERA1,
/* 3 */    CID_IP_ADDRESS_CAMERA2,
/* 4 */    CID_IP_ADDRESS_PROJECTOR,
/* 5 */    CID_IP_ADDRESS_SQ6,
    
    // misc items
/* 6 */    CID_ATEM_CAMERAS,
/* 7 */    CID_JOYSTICK_DEAD_ZONE,
/* 8 */    CID_SCREEN_DOWN_TIME,

    // keep the paddle switch types in order
/* 9 */    CID_IRIS_MIN_PERCENT,
/* 10*/    CID_IRIS_MIN_TIME,
/* 11*/    CID_IRIS_RAMP_TIME,
/* 12*/    CID_FOCUS_MIN_PERCENT,
/* 13*/    CID_FOCUS_MIN_TIME,
/* 14*/    CID_FOCUS_RAMP_TIME,
/* 15*/    CID_ZOOM_MIN_PERCENT,
/* 16*/    CID_ZOOM_MIN_TIME,
/* 17*/    CID_ZOOM_RAMP_TIME,
/* 18*/    CID_SQ6_SCENE,
/* 19*/    CID_SQ6_WAIT_SCENE,

            CID_FIRST_PR_NAME,

 /* 20*/   CID_ATEM_PR1 = CID_FIRST_PR_NAME,
 /* 21*/   CID_ATEM_PR2,
 /* 22*/   CID_ATEM_PR3,
 /* 23*/   CID_ATEM_PR4,
 /* 24*/   CID_ATEM_PR5,
 /* 25*/   CID_ATEM_PR6,
 /* 26*/   CID_ATEM_PR7,
 /* 27*/   CID_ATEM_PR8,
 /* 28*/   CID_CAM1_PR1,
 /* 29*/   CID_CAM1_PR2,
 /* 30*/   CID_CAM1_PR3,
 /* 31*/   CID_CAM1_PR4,
 /* 32*/   CID_CAM1_PR5,
 /* 33*/   CID_CAM1_PR6,
 /* 34*/   CID_CAM1_PR7,
 /* 35*/   CID_CAM2_PR1,
 /* 36*/   CID_CAM2_PR2,
 /* 37*/   CID_CAM2_PR3,
 /* 38*/   CID_CAM2_PR4,
 /* 39*/   CID_CAM2_PR5,
/* 40*/    CID_CAM2_PR6,
/* 41*/    CID_CAM2_PR7,
/* 42*/    CID_LAST_ITEM,


};




typedef enum t_ConfTypes
{
    CTYPE_IP,
    CTYPE_PERCENT,
    CTYPE_TIMEMS,
    CTYPE_TIMES,
    CTYPE_STRING1, // 8 character YNYYY string
    CTYPE_SCENE,
    CTYPE_PRESET_TEXT,
    CTYPE_NOT_USED,

};




// a type that defines one preset for the ATEM
typedef struct
{
    t_VideoIndex progIndex;
    t_VideoIndex auxIndex;
    bool dsk1enabled;  // downstream key 1 enabled?
    bool dsk2enabled;   // downstream key 2 enabled?
    bool usk1enabled;    // upstream key 1  enabled
    
}t_preset;


// some paddle switches have a configurable ramp up time consisting of 3 values
// The start value as a percent of max
// How long to hold the start value in mS
// How long to take to ramp to max in mS
// A Type that defines one switch config store
typedef struct
{
    int minTimemS;
    int rampTimemS;
    ubyte minValuePercent;
}t_SwitchConfig;

// Define a store for camera preset info.
// Most camera info is stored in the camera but I have found the following:
// a) The iris position does not seem to be restored
// b) The auto focus on/off state is not stored
// c) The auto iris on/off state is not stored
typedef struct
{
    int iris;
    int autoFocus;
    int autoIris;
}t_CameraConfig;


#define MIN_STARTSWITCHVALUEPERCENT 1
#define MAX_STARTSWITCHVALUEPERCENT 40
#define MIN_STARTSWITCHTIMEmS 100
#define MAX_STARTSWITCHTIMEmS 2000
#define MIN_SWITCHRAMPTIMEmS 1000
#define MAX_SWITCHRAMPTIMEmS 7000


// Now the definitions of the complete config store
typedef struct
{
    // the working copy of the atem presets,
    t_preset ATEMPresetStore[MAX_ATEM_PRESETS];
    // the working copy of the IP addresses,
    IPAddress IPConfigArray[MAX_IP_ADDRESSES];
    t_SwitchConfig switchConfig[SC_MAX_SWITCH_CONFIGS];
    ubyte joyStickDeadzone;
    t_CameraConfig cameraPreset[MAX_CAMERA_PRESETS][MAX_CAMERAS];
    boolean cameraInput[MAX_ATEM_INPUTS];
    ubyte screenDownTime;
    ubyte sq6_scene;
    ubyte sq6_wait_scene;

}t_configStore;

// We also have a config store for all the Preset names but we don't shadow it in RAM.
// It starts just above the config store
#define MAX_PRESET_NAME_LEN 40  // 40 chars inclding trailing NULL
#define EEPROM_PRESETS_START_ADDRESS 500
#define NUMBER_OF_PRESETS (MAX_ATEM_PRESETS+ (MAX_CAMERA_PRESETS*MAX_CAMERAS) )
#define SIZEOF_PRESETS (MAX_PRESET_NAME_LEN * NUMBER_OF_PRESETS)



extern t_configStore configStore;
extern void saveConfigStoretoEEPROM(void);
extern void initConfig(void);
extern boolean isUsableIpAddress(IPAddress address);


// A family of functions to get confif info in printable form
extern ubyte getConfName(char *dest,t_ConfId item);
extern int getConfMax(t_ConfId item);
extern int getConfMin(t_ConfId item);
extern ubyte getConfType(char *dest,t_ConfId item);
extern int getConfValueStr(char *dest,t_ConfId item);
extern boolean isIPAddressValid(char* inputBuffer, IPAddress *newAddress);
extern boolean checkAndSaveConfString(char *inBuff, t_ConfId item, boolean saveIt= false);
extern void printIPAddress(t_ConfId item);
extern void enableSavePresetMode(boolean active);
extern void printConfig(void);

#endif
