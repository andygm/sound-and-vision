#include <Streaming.h>

#include <Wire.h>

/*****************
    This is St Johns Church Console firmware developed from a number of arduino examples
    It is built in an arduino Mega 2560 clone PCB with the following shields fitted:

    a) Adafruit 2746 Bluefruit LE Shield adding Bluetooth capability
    b) W5100 Ethernet shield
    c) Additional I2c led drive and xy matrix Button/switch input

    It is linked to a panel containing about a dozen illuminated push buttons and
    a few discrete LED indicators and all runs from a 12v mains PSU. Also linked
    is an 8 channel RF solutions Bravo 868MHz radio link to allow remote control
    of some of the features.

    All the electronics is built on veroboard and documented on a public bitbucket
    repository at https://bitbucket.org/coppo86/sound-and-vision

    User manuals etc are on the church onedrive documents area and only open to
    members of appropriate teams in St Johns Church

    This code is very much hacked together so apologies. It works reliably but sort of
    grew rather than being crafted.

    It was based on Arduino 1.6.5 arduino Mega board, It must use 1.6.5 as 1.6.6 had problems
    The latest

    Change History - see revision.h
    ==============

	Last change: AC 12/04/2020 17:18:19
 */




#include "globals.h"
#include <SkaarhojPgmspace.h>
#include "revision.h"
#include <SPI.h>         // needed for Arduino versions later than 0018

// we need to include some headers here not because they are needed in this file
// but this forces the IDE to include them in the project. Just putting them in
// the modules that need them does not work - yoiu get file not found.
// Ethernet and atem libraries are definitely ones that cause issues
#include <Ethernet.h>
#include <ATEMbase.h>
#include <ATEMext.h>
//==================================

#include <Arduino.h>
#include "utilities.h"
#include "atemutils.h"
#include "camerautils.h"
#include "leddriver.h"
#include <EEPROM.h>
#include "config.h"
#include "screencontrol.h"
#include "tcpcontrol.h"
#include "webserver.h"
#include "buttondriver.h"


#include "SkaarhojUtils.h"
SkaarhojUtils utils;


#if SOFTWARE_SERIAL_AVAILABLE
  #include <SoftwareSerial.h>
#endif

byte mac[] = {
  0x90, 0xA2, 0xDA, 0x0D, 0x6B, 0xB9 };      // <= SETUP!  MAC address of the Arduino



ubyte rebootDelay ;




#include <ClientPanaAWHExTCP.h>




// helper function
static void showprogchecksum(void)
{
    unsigned long total = 0;
    unsigned long ctr = 0;
    do
    {
        total += (unsigned char)pgm_read_byte_near(ctr);
    }
    while ( ++ctr <= FLASHEND );
    Serial.printf(F("FlashSize 0x%lx Checksum 0x%08lx. \n\r"), ctr, total);
}


// A small helper
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}

byte gateway[] = { 192, 168, 1, 1 };
// the subnet:
byte subnet[] = { 255, 255, 0, 0 };


/**************************************************************************/
/*!
*/
/**************************************************************************/
void setup() {

    // Initialise the random number generator
    randomSeed(analogRead(5));  // For random port selection
  
    Serial.begin(SERIAL_BAUD);

    delay(100);
    // do the startup banner info
    printNewLine();
    printRow('#', 20, false);
    Serial.printf(F(" BOOTING UP SYSTEM "));
    printRow('#', 20, true);
    printRevString();


    showprogchecksum();

    initConfig();
    initButtons();
    initLeds();


}

typedef enum
{
    STARTUP_BEGIN,
    STARTUP_FLASH_LEDS,
    STARTUP_INIT_ETHERNET,
    STARTUP_INIT_ETHERNET_DHCP,
    STARTUP_ETH_WAIT,
    STARTUP_INIT_REST,
    STARTUP_COMPLETE,

}tSTARTUP_STAGES;

tSTARTUP_STAGES startupStage = STARTUP_BEGIN;

// the amount f time the leds all flash during startup - suggest 2000-4000mS
#define LEDSTARTUPFLASHmS 4000
static ubyte startupCtr;

boolean isStartupComplete(void)
{
    return (startupStage == STARTUP_COMPLETE);
}

boolean handleButtonDuringBoot(t_Keycodes keyCode, ubyte repeatCt)
{
//    if  (thisRepeat == (1000/KEYREPEATmS))

    if (isStartupComplete())
    {
        return false;
    }
    else
    {
        // can we use the key?
        if (keyCode == KEYCODE_FTB && repeatCt == 12)
        {
            if (startupStage == STARTUP_INIT_ETHERNET)
            {
                startupStage = STARTUP_INIT_ETHERNET_DHCP;
                setAllLEDs(LEDMODE_SPEED_FAST | LEDMODE_PATTERN_12PERCENT , 0);// chnage pattern 
                startupCtr = 2000/UI_UPDATEmS; // add a shortv period of a different flash pattern
            }
        }

        return true;
    }

}
/*
extern uint16_t findFreeRam(void);
extern void report_mem(void); 

uint8_t pac;

*/
void loop()
{
    static uint8_t uiTime;
    boolean uiTick = false;
    if ( ((uint8_t)((uint8_t)millis()-uiTime)) > UI_UPDATEmS )
    {
        // every UIUPDATEmS Tick starting straight after boot
        uiTick = true;
        uiTime = millis();

        if (rebootDelay >0 && --rebootDelay == 0 )
        {
            reboot(0);
        }
        manageLedsTick();
/*
        if (++pac > 30)
        {
        report_mem();
        findFreeRam();
        pac = 0;
        }
*/
    }
    // every loop pass starting straight after boot
    manageKeyPresses();
    manageSerialInterface();

    if (isStartupComplete())
    {
        // now stuff that only runs when we have completed startup
        webServerLoop(); 
        loopCamera();
        loopATEM();
        if(uiTick)
        {
            // every UI_UPDATEmS
            cameraTick75ms();
            atemTick75mS();
            screenTick();
            TCPTick();
            utils.backgroundCalibrate();
        }
    }
    else
    {
        if (uiTick == true && startupCtr > 0) --startupCtr;
        switch (startupStage)
        {
            case STARTUP_BEGIN :
                startupCtr = LEDSTARTUPFLASHmS/UI_UPDATEmS;
                startupStage = STARTUP_INIT_ETHERNET;
                setAllLEDs(LEDMODE_SPEED_FAST | LEDMODE_PATTERN_5050, 0);// flash all leds 
                Serial.printf(F("\nPress and HOLD FTB button for 3 seconds to use DHCP server\n\n"));
                 break;
            case STARTUP_INIT_ETHERNET :
            case STARTUP_INIT_ETHERNET_DHCP :
                if (startupCtr == 0)
                {
                    setAllLEDs( LEDMODE_PATTERN_OFF, 0);// turn off all leds

                    printRow('+', 60, true);
                    printTime(true); Serial.printf(F("Starting Ethernet "));
                
                    //#define NO23DEBUG
                    if (STARTUP_INIT_ETHERNET_DHCP ==startupStage)
                    {
                        Serial.printf(F("USING DHCP because Fade to black button held down during boot\n"));
                        Ethernet.begin(mac); // DHCP
                    }
                    else
                    {
                        #ifdef NO23DEBUG
                        Serial.printf(F("\nDEBUG DEBUG - using IPaddress 192.168.1.197 for No23 debugging\n"));
                        IPAddress debugAddress(192, 168, 1, 197);
                        Ethernet.begin(mac, debugAddress,gateway,subnet);
                        #else
                        // Start the Ethernet
                        Ethernet.begin(mac, configStore.IPConfigArray[IP_ADDRESS_CONSOLE],gateway,subnet);
                        #endif
                    }
                    startupStage = STARTUP_ETH_WAIT;
                    startupCtr = 2;
                }
                break;
            case STARTUP_ETH_WAIT :
                if (startupCtr == 0)
                {
                    Serial.printf(F("Using Ipaddress "));
                    Serial.println(Ethernet.localIP());
                    printNewLine(); printNewLine();
                    startupStage = STARTUP_INIT_REST;
                }
                break;

            case STARTUP_INIT_REST :
                initATEM();
                initWebServer();
                printNewLine();

                // Initializing the joystick at A9(H) and A8(V) + 35 (Joystick 1). A14 and A15, 36 is also possible for Joystick 2
                // tolerance = 5
                utils.joystick_init(5, A9, A8, 35);
            
            
                // now do any more init needed by the userinterface
                initUtilities();
                kickCameraTimer();
                kickATEMTimer();

                // init the screen, projector and SQ6 mixer TCP control
                initScreen();
                initTCPControl();
                initCameras();
                startupStage = STARTUP_COMPLETE;
                printConfig();

                break;

        }
    }
}

/*
        To do list 15/1/2019

        ===========================================
        TO DO STILL+++++++++++
        Check iris handling - sudden jumsp etc
        Make FTB and misc1 leds flash or OFF not ON or OFF

        MISC1 button when going from blank to on is slow if it retsores
        the auto iris too quickly.

        Presets speed needs adjusting
        











*/

