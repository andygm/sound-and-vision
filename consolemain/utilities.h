#ifndef __utilities_h
#define  __utilities_h
#include "globals.h"
#include "config.h"
/*

	Last change: AC 09/04/2020 16:35:47
*/




typedef enum t_TestModes
{
    TEST_MODE_OFF,
    TEST_MODE_RAPID_FLASH,
    TEST_MODE_SCAN_LEDS,
    TEST_MODE_STEP_LEDS,
    TEST_MODE_LAST_MODE,
};

#define UTILITIES_H_VER 5


extern t_TestModes UITestMode;

extern void initUtilities(void);
extern void restoreIPAddresses(void);
extern void printTime(boolean shortversion);
extern void printRow(char character, uint8_t quantity, boolean addNewLine);
extern void printNewLine(void);
extern char * getFirstVal(char* inputBuffer, int *value);
extern void reboot(ubyte sysTicks);  // if count is zero it calls immediate reboot
extern void saveAudioPowerState(boolean isOn);
extern void saveVisualsPowerState(boolean isOn);
extern boolean getVisualsPowerState(void);
extern boolean getAudioPowerState(void);
extern void manageSerialInterface(void);
extern void check_mem(void);
extern void report_mem(void) ;



#endif //  __utilities_h
