#include "globals.h"
PROGMEM const char lastsave[] ="2024-08-05 20:47:56";
#define SCREEN_CPP_VER 3

/** @package 

    screencontrol.cpp
    
    Copyright(c)  2000
    
    Author: ANDREW COPSEY
*/
#include <SkaarhojPgmspace.h>
#include "utilities.h"
#include <wire.h>
#include "config.h"
#include "buttondriver.h"
#include "leddriver.h"
#include "atemutils.h"
#include "camerautils.h"
#include "screencontrol.h"



void initScreen(void)
{

    printModuleStartup(PSTR("Screen control Module"),SCREEN_CPP_VER,SCREEN_H_VER,lastsave);

    pinMode(SCREEN_DOWN_PIN, OUTPUT);
    digitalWrite(SCREEN_DOWN_PIN, LOW);
    pinMode(SCREEN_UP_PIN, OUTPUT);
    digitalWrite(SCREEN_UP_PIN, LOW);
    pinMode(SCREEN_OUT_PIN, OUTPUT);
    digitalWrite(SCREEN_OUT_PIN, LOW);
    pinMode(SCREEN_IN_PIN, OUTPUT);
    digitalWrite(SCREEN_IN_PIN, LOW);

}


enum screenControlStates
{
    SCS_SYSTEM_BOOT,
    SCS_START_IDLE_IN,
    SCS_IDLE_IN,
    SCS_START_OUT,
    SCS_START_OUT1,
    SCS_START_OUT_ACTIVE1,
    SCS_START_OUT_ACTIVE2,
    SCS_IDLE_OUT,
    SCS_START_IN,
    SCS_START_IN1,
    SCS_START_IN_ACTIVE1,
    SCS_START_IN_ACTIVE2,
    SCS_START_IN_ACTIVE3,
};
static enum screenControlStates screenControlState = SCS_SYSTEM_BOOT;




/*
    This handles the ascxreen going in/out/up/down
*/
void screenTick(void)
{
    static uint16_t screenTickCtr;
    switch ( screenControlState )
    {
        case SCS_SYSTEM_BOOT:
            screenControlState = SCS_START_IDLE_IN;
            break;
        case SCS_START_IDLE_IN:
            // screen is fully in
            setLEDMode(LED_SCREEN, LEDMODE_PATTERN_OFF );
            screenControlState = SCS_IDLE_IN;
            break;

        case SCS_IDLE_IN:
            // do nothing
            break;

        case SCS_START_OUT:
            if ( !isLEDTimerBusy(LED_SCREEN) )
            {
                screenControlState = SCS_START_OUT1;
            }
            break;
        case SCS_START_OUT1:
            screenTickCtr = 0;
            controlScreenRelays (SCREEN_OUT_RELAY,true);
            controlScreenRelays (SCREEN_DOWN_RELAY,true);
            setLEDMode(LED_SCREEN, LEDMODE_PATTERN_25PERCENT | LEDMODE_SPEED_SLOW );
            screenControlState = SCS_START_OUT_ACTIVE1;
            break;

        case SCS_START_OUT_ACTIVE1 :
            if ( ++screenTickCtr >= configStore.screenDownTime *(1000L/UI_UPDATEmS) )
            {
                // down is very much quicker than out and has finished
                controlScreenRelays (SCREEN_DOWN_RELAY,false);
                // continue with moving the screen out
                setLEDMode(LED_SCREEN, LEDMODE_PATTERN_5050 | LEDMODE_SPEED_SLOW);
                screenControlState = SCS_START_OUT_ACTIVE2;
            }
            break;

        case SCS_START_OUT_ACTIVE2 :

            if ( ++screenTickCtr >= (SCREEN_OUTmS /(long)UI_UPDATEmS) )
            {
                // we are down AND OUT
                controlScreenRelays (SCREEN_ALL_RELAYS,false);
                screenControlState = SCS_IDLE_OUT;
            }
            break;

        case SCS_IDLE_OUT:
            setLEDMode(LED_SCREEN, LEDMODE_PATTERN_ON );
            break;

        case SCS_START_IN:
            if ( !isLEDTimerBusy(LED_SCREEN) )
            {
                screenControlState = SCS_START_IN1;
            }
            break;

        case SCS_START_IN1:
            // turn off all relays - even if not on and pause in that state for
            // approx 1 second. Just in case the screen was moving out, give motors
            // a chance to stop
            setLEDMode(LED_SCREEN, LEDMODE_PATTERN_25PERCENT | LEDMODE_SPEED_SLOW | LEDMODE_INVERTED);
            screenTickCtr = 0;
            controlScreenRelays (SCREEN_ALL_RELAYS,false);
            screenControlState = SCS_START_IN_ACTIVE1;
            break;

        case SCS_START_IN_ACTIVE1:
            if ( ++screenTickCtr >= (1000L/UI_UPDATEmS) )
            {
                // start screen moving IN and UP
                screenTickCtr = 0;
                controlScreenRelays (SCREEN_IN_RELAY,true);
                controlScreenRelays (SCREEN_UP_RELAY,true);
                screenControlState = SCS_START_IN_ACTIVE2;
            }
            break;
        case SCS_START_IN_ACTIVE2:
            if ( ++screenTickCtr >= (SCREEN_UPmS /(long)UI_UPDATEmS) )
            {
                // up is very much quicker than IN and has finished
                setLEDMode(LED_SCREEN, LEDMODE_PATTERN_25PERCENT | LEDMODE_SPEED_VERY_SLOW | LEDMODE_INVERTED);

                controlScreenRelays (SCREEN_UP_RELAY,false);
                // continue with moving the screen in
                screenControlState = SCS_START_IN_ACTIVE3;
            }
            break;
        case SCS_START_IN_ACTIVE3:

            if ( ++screenTickCtr >= (SCREEN_INmS /UI_UPDATEmS) )
            {
                controlScreenRelays (SCREEN_ALL_RELAYS,false);
                screenControlState = SCS_START_IDLE_IN;
            }
            break;

        default :
            // no action
            break;
    }
}


void controlScreenRelays(uint8_t relay, boolean on)
{

    boolean repeat = (relay == SCREEN_ALL_RELAYS || relay > SCREEN_LAST_RELAY)?true: false;
    if ( repeat )
    {
        // start at the beginning of the list and do it all
        relay = 1;
    }
    do
    {
        printTime(true);
        switch ( relay )
        {
            case SCREEN_OUT_RELAY:
                Serial.printf(F("OUT"));
                if ( digitalRead (SCREEN_OUT_PIN) != on)
                {
                    digitalWrite(SCREEN_OUT_PIN, (on == true)?HIGH:LOW);
                }
                break;
            case SCREEN_IN_RELAY:
                Serial.printf(F("IN"));
                if ( digitalRead (SCREEN_IN_PIN) != on)
                {
                    digitalWrite(SCREEN_IN_PIN, (on == true)?HIGH:LOW);
                }
                break;

            case SCREEN_DOWN_RELAY:
                Serial.printf(F("DOWN"));
                if ( digitalRead (SCREEN_DOWN_PIN) != on)
                {
                    digitalWrite(SCREEN_DOWN_PIN, (on == true)?HIGH:LOW);
                }
                break;
            case SCREEN_UP_RELAY:
                Serial.printf(F("UP"));
                if ( digitalRead (SCREEN_UP_PIN) != on)
                {
                    digitalWrite(SCREEN_UP_PIN, (on == true)?HIGH:LOW);
                }
                break;

            default :
                break;
        }
        if (on)
        {
            Serial.printf(F(" relay is ON\n"));
        }
        else
        {
            Serial.printf(F(" relay is off\n"));
        }

        if ( ++relay > SCREEN_LAST_RELAY || repeat == false)
        {
            break;
        }
    }while(true);
}


// if screen is out or part out this will start it going in and up
void forceScreenIn(void)
{
    if (screenControlState !=SCS_IDLE_IN)
    {
       screenControlState = SCS_START_IN;
       setLEDMode(LED_SCREEN, LEDMODE_PATTERN_5050 , 6);
    }
}


void operateScreen(uint16_t keyRepeatCount)
{
    // called when MISC2 key is down. We handle a short press and a longer press differently

    if ( keyRepeatCount == 1000/KEYREPEATmS )
    {
        // long press or 1 second ish
        // If the screen is part out it will pause for a second and then go back in.
        // If it is fully in it will start going out
        if ( SCS_IDLE_IN == screenControlState )
        {

            setLEDMode(LED_SCREEN, LEDMODE_PATTERN_5050 , 6);
            screenControlState = SCS_START_OUT;
        }
        else  if ( SCS_IDLE_OUT == screenControlState )
        {
            screenControlState = SCS_START_IN;
            setLEDMode(LED_SCREEN, LEDMODE_PATTERN_5050 , 6);
        }
    }
    else if ( keyRepeatCount == 1)
    {
        // very short press.
        // If screen is moving out  stop it and set state to OUT.
        // next long press will start a full timed action
        if (SCS_IDLE_IN != screenControlState && SCS_IDLE_OUT != screenControlState)
        {
            // stop all movement and pretend we are out
            screenControlState = SCS_IDLE_OUT;
            controlScreenRelays (SCREEN_ALL_RELAYS,false);
        }
    }

}




