/*


*/

#include "SkaarhojUtils.h"
#include "utilities.h"


//Use HTML tag <br> for line break
const char revString[] PROGMEM = 
R"rawliteral(
St Johns Sound and Vision Console<br>
Rev 3.19 06-08-2024<br>
Design and coding copyright APC Design 2023-<br><br>

Built around Arduino Mega 2560 with Ethernet interface<br>
Built using Arduino IDE 1.8.12<br>
https://bitbucket.org/andygm/sound-and-vision/src/master/<br><br>

)rawliteral";

const char compileDate[] PROGMEM = "Compiled "__DATE__"  " __TIME__"<br>";
void printRevString(void)
{
    int length = strlen_P(revString);
    int offset = 0;
    char ch;
    while (length-- > 0)
    {
        ch = pgm_read_byte_near(revString+ offset++);
        if (ch =='<')
        {
            // skip
            length -=3;
            offset +=3;
        }
        else
        {
            Serial.write(ch);
        }
    }
    length = strlen_P(compileDate);
    offset = 0;
    while (length-- > 0)
    {
        ch = pgm_read_byte_near(compileDate+ offset++);
        if (ch =='<')
        {
            length -=3;
            offset +=3;
            ch = '\n';
        }
        Serial.write(ch);
    }
}
char *getRevString(void)
{
    return revString;
}
char *getCompileString(void)
{
    return compileDate;
}
void printModuleStartup(char* moduleName,int cppVer,int hVer,char* lastsave)
{
    printRow('+', 60, true);
    char buffer[60];
    sprintf_P(buffer,moduleName);
    printTime(true); Serial.printf(F("Starting %s\n"),buffer);
    if (cppVer != hVer)
    {
        Serial.printf(F("ERROR   ERROR    ERROR. CPP and H versions NOT the same\n"));
    }
    Serial.printf(F("\t%s version: v%d. "),buffer,cppVer);
    sprintf_P(buffer,lastsave);
    Serial.printf(F(" Last saved: %s\n"),buffer);
}


