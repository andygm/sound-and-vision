#ifndef __screencontrol_h
#define __screencontrol_h
/** @package

    screencontrol.h

    Thsi module is responsible for handling the screen in/out

    Copyright(c)  2000
    
    Author: ANDREW COPSEY
    Created: AC  29/12/2018 17:37:08
	Last change: AC 15/04/2020 15:22:00
*/

#define SCREEN_H_VER 3

extern void initScreen(void);
extern void screenTick(void);
extern void operateScreen(uint16_t keyRepeatCount);

extern void forceScreenIn(void);


#define SCREEN_ALL_RELAYS 0
#define SCREEN_OUT_RELAY 1
#define SCREEN_IN_RELAY 2
#define SCREEN_DOWN_RELAY 3
#define SCREEN_UP_RELAY 4
#define SCREEN_LAST_RELAY 4


extern void controlScreenRelays(uint8_t relay, boolean on);



#endif// __screencontrol_h


