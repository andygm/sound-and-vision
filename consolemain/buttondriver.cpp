#include "globals.h"
PROGMEM const char lastsave[] ="2024-08-05 15:21:25";
#define BUTTON_CPP_VER 4

 
 /** @package

    buttondriver.cpp

    Thsi module is responsible for scanning and debouncing inputs from keys, buttons,
    and other user input devices such as keyswitches etc
    
    Copyright(c)  2000
    
    Author: ANDREW COPSEY
 */

#include <SkaarhojPgmspace.h>
#include "utilities.h"
#include <wire.h>
#include "config.h"
#include "buttondriver.h"
#include "leddriver.h"
#include "atemutils.h"
#include "camerautils.h"
#include "screencontrol.h"
#include "tcpControl.h"





// enable this for serial debug
//#define  DEBUGKEYS



void getInputName(char *ptr, t_Keycodes keyCode)
{
    if (ptr != NULL)
    {
        switch (keyCode)
        {
            case KEYCODE_NO_KEY:
                sprintf_P(ptr,PSTR(""));
                break;
            case KEYCODE_MISC1:
                sprintf_P(ptr,PSTR("Misc 1"));
                break;
            case KEYCODE_MISC2:
                sprintf_P(ptr,PSTR("Misc 2"));
                break;
            case KEYCODE_IRIS_AUTO:
                sprintf_P(ptr,PSTR("Iris AUTO"));
                break;
            case KEYCODE_IRIS_OPEN:
                sprintf_P(ptr,PSTR("Iris OPEN"));
                break;
            case KEYCODE_IRIS_CLOSE:
                sprintf_P(ptr,PSTR("Iris CLOSE"));
                break;
            case KEYCODE_ZOOM_OUT:
                sprintf_P(ptr,PSTR("Zoom OUT"));
                break;
            case KEYCODE_ZOOM_IN:
                sprintf_P(ptr,PSTR("Zoom IN"));
                break;
            case KEYCODE_FOCUS_AUTO:
                sprintf_P(ptr,PSTR("Focus AUTO"));
                break;
            case KEYCODE_FOCUS_OTAUTO:
                sprintf_P(ptr,PSTR("Focus ONCE"));
                break;
            case KEYCODE_FOCUS_OUT:
                sprintf_P(ptr,PSTR("Focus OUT"));
                break;
            case KEYCODE_FOCUS_IN:
                sprintf_P(ptr,PSTR("Focus IN"));
                break;
            case KEYCODE_CAMERA_SELECT:
                sprintf_P(ptr,PSTR("Camera select"));
                break;
            case KEYCODE_FTB:
                sprintf_P(ptr,PSTR("Fade to black FTB"));
                break;
            case KEYCODE_PROJECTOR:
                sprintf_P(ptr,PSTR("Projector power"));
                break;
            case KEYCODE_MP3:
                sprintf_P(ptr,PSTR("MP3 power"));
                break;
            case KEYCODE_SCREEN:
                sprintf_P(ptr,PSTR("Screen in-out"));
                break;
            case KEYCODE_SPARE:
                sprintf_P(ptr,PSTR("Spare button"));
                break;
            default :
                if (keyCode >= KEYCODE_CAMERA_PRESET1 && keyCode <= KEYCODE_CAMERA_PRESET7)
                {
                    sprintf_P(ptr,PSTR("Camera preset %d"),(ubyte)keyCode - (ubyte)KEYCODE_CAMERA_PRESET1 + 1 );
                }
                else if (keyCode >= KEYCODE_ATEM_PRESET1 && keyCode <= KEYCODE_ATEM_PRESET8)
                {
                    sprintf_P(ptr,PSTR("ATEM switcher preset %d"),(ubyte)keyCode - (ubyte)KEYCODE_ATEM_PRESET1 + 1 );
                }
                else
                {
                    sprintf_P(ptr,PSTR("Keycode %d not known"),(ubyte)keyCode);
                }
                break;

        }
    }
}





// these are set in intterupt if a new key value stabilises
static volatile uint8_t newScanCode;
static volatile uint8_t auxPort;
static volatile uint16_t newKeyRepeatCtr;


// used to signal between interrupt and foreground code to say do a scan of
//aux inputs
static boolean f_readAux;

// the last read of the aux i2c inputs
static ubyte auxCache;


static ubyte webKeyCtr = 0;        
ubyte webKey = KEYCODE_NO_KEY;


void handleWebKey(t_Keycodes keyCode, boolean pressed)
{
    if (pressed) webKeyCtr = 0;
    webKey = keyCode;
    //Serial.printf("Web Key %d pressed %d\n",keyCode,pressed);
}



extern boolean handleButtonDuringBoot(t_Keycodes keyCode, ubyte repeatCt);

// called every time round the main loop. THis maybe several times per mS or have gaps
//of several 100s of mS if ethernet kit is slow responding. It checks for 
// new keypresses and status inputs and acts on them
void manageKeyPresses(void)
{
    if (f_readAux == true)
    {
        // interrupt has told us to get  an I2C read from the aux port. happens once per scan of the key matrix
        f_readAux = false;
        Wire.beginTransmission(I2C_IN_ADD);
        Wire.write(9);             // Reg 9 GPIO
        Wire.endTransmission();     // stop transmitting
        Wire.requestFrom(I2C_IN_ADD,1,true); // get one byte and release the bus
        auxCache = ~Wire.read();  // invert the signals
    }

//    ubyte index = 0;
//    uint16_t position;
    static uint16_t oldRepeat;
    uint16_t thisRepeat = newKeyRepeatCtr;

    // Check for keys/button changes
    if ( oldRepeat != thisRepeat )
    {
        ubyte index;
        t_Keycodes keyValue = newScanCode;
        oldRepeat = thisRepeat;
        #ifdef DEBUGKEYS
        if ( thisRepeat >= 1 || (thisRepeat% 5) == 0)
        {
            char buffer[30];
            getInputName(buffer,keyValue);
            if (keyValue != KEYCODE_NO_KEY)
            {
                printTime(true);Serial.printf(F("%d key value %d %s\n"),thisRepeat,keyValue,buffer);
            }

        }
        #endif
        if ((keyValue != KEYCODE_NO_KEY) && handleButtonDuringBoot(keyValue,thisRepeat))
        {
            return;
        }

        switch ( keyValue )
        {
            case KEYCODE_NO_KEY:
                if (thisRepeat == 1)stopFocusZoom();
                //do nothing
                break;
            case KEYCODE_ATEM_PRESET1 :
            case KEYCODE_ATEM_PRESET2 :
            case KEYCODE_ATEM_PRESET3 :
            case KEYCODE_ATEM_PRESET4 :
            case KEYCODE_ATEM_PRESET5 :
            case KEYCODE_ATEM_PRESET6 :
            case KEYCODE_ATEM_PRESET7 :
            case KEYCODE_ATEM_PRESET8 :
                index = keyValue - KEYCODE_ATEM_PRESET1;
                Serial.printf(F("DBzzG key %d busy %d\n"),index+1,isLEDTimerBusy(LED_MISC1));

                if  (thisRepeat == 0)
                {
                    // short press and release,
                    boolean busy =  isLEDTimerBusy(LED_MISC1);
                    enableSavePresetMode(false);
                    if (busy)
                    {
                        
                        saveATEMPreset(index);
                        Serial.printf(F("DBGSave %d\n"),index+1);
                    }
                    else
                    {
                        applyATEMPreset(index);
                        Serial.printf(F("DBGApply %d\n"),index+1);
                    }

                }
                break;

            case KEYCODE_MP3 :
                if  (thisRepeat == (1000/KEYREPEATmS))
                {
                    if (getAudioPowerState() )
                    {
                        if (getLEDState(LED_BLUETOOTH_POWER) == 0)
                        {
                            // turn it on - quick flashes and then ON
                            setLEDMode(LED_MP3, LEDMODE_PATTERN_5050 | LEDMODE_INVERTED , 15);
                            setLEDMode(LED_BLUETOOTH_POWER,LEDMODE_PATTERN_ON);
                        }
                        else
                        {
                            // turn it off
                            setLEDMode(LED_MP3, LEDMODE_PATTERN_OFF);
                            setLEDMode(LED_BLUETOOTH_POWER,LEDMODE_PATTERN_OFF);
                        }
                   }
                }
                break;

            case KEYCODE_FTB :
                // only interested in short press to toggle FTB .
                //  If we do a long press to reset presets
                // we will also toggle FTB but it doesn't matter

                kickATEMTimer();
                if (thisRepeat == 1)
                {
                    #ifdef MAINDEBUG
                    Serial.printf(F("DEBUG: FTB\n"));
                    #endif
                    doATEMCommand(ATEM_FTB,1);
                    setLEDMode(LED_FTB, LEDMODE_PATTERN_5050 | LEDMODE_INVERTED, 8);
                }
                if  (thisRepeat == (4000/KEYREPEATmS))
                {
                    #ifdef MAINDEBUG
                    Serial.printf(F("DEBUG: FTB - default presets\n"));
                    #endif
                    // 4 Second press we will default all the presets but not change the ATEM settings
                    defaultATEMPresets();
                }
                break;

            case KEYCODE_CAMERA_SELECT :
                if (thisRepeat == 1)
                {
                    #ifdef MAINDEBUG
                    Serial.printf(F("DEBUG: Next camera\n"));
                    #endif
                    selectNextCamera();
                }
                break;

            case KEYCODE_ZOOM_IN :
            case KEYCODE_ZOOM_OUT :
                doCameraZoom(keyValue == KEYCODE_ZOOM_OUT,thisRepeat);
                break;

            case KEYCODE_FOCUS_OUT :
            case KEYCODE_FOCUS_IN  :
                doCameraFocus(keyValue == KEYCODE_FOCUS_IN,thisRepeat);
                break;

            case KEYCODE_FOCUS_OTAUTO :
                if (thisRepeat == 1)
                {
                    // turn off auto focus
                    if ( getCameraValue(DATA_COMMAND_GET_AUTO_FOCUS) )
                    {
                        queueCameraCommand(COMMAND_AUTOFOCUS, 0);
                    }
                    // do a one shot auto focus
                    queueCameraCommand(COMMAND_ON_TOUCH_AUTOFOCUS, 0);
                }
                break;
            case KEYCODE_FOCUS_AUTO :
                if (thisRepeat == 1)
                {
                    // turn on auto focus
                    queueCameraCommand(COMMAND_AUTOFOCUS, 1);
                }
                break;

            case KEYCODE_CAMERA_PRESET7 :
            case KEYCODE_CAMERA_PRESET6 :
            case KEYCODE_CAMERA_PRESET5 :
            case KEYCODE_CAMERA_PRESET4 :
            case KEYCODE_CAMERA_PRESET3 :
            case KEYCODE_CAMERA_PRESET2 :
            case KEYCODE_CAMERA_PRESET1 :
                keyValue = (ubyte)keyValue -KEYCODE_CAMERA_PRESET1;
                Serial.printf(F("DBzzG Cam preset key %d busy %d\n"),index+1,isLEDTimerBusy(LED_MISC1));

                if  (thisRepeat == 0)
                {
                    // short press and release,
                    boolean busy =  isLEDTimerBusy(LED_MISC1);
                    enableSavePresetMode(false);

                    if (busy)
                    {
                        if (getCameraValue(DATA_COMMAND_ISCONNECTED))
                        {
                            saveCameraPreset((ubyte)keyValue);
                            queueCameraCommand(COMMAND_PRESET_STORE,(ubyte)keyValue);
                            setAllCameraLeds(LEDMODE_PATTERN_5050, 20);
                        }
                        else
                        {
                            setAllCameraLeds(LEDMODE_PATTERN_25PERCENT, 20);
                        }
    
                    }
                    else
                    {
                        // short press and release
                        storeCameraPresetSequence(keyValue);
                    }
                }
                break;

            case KEYCODE_IRIS_OPEN :
            case KEYCODE_IRIS_CLOSE :
                if ( getCameraDim() == true )
                {
                    cancelCameraDim();
                }
                else
                {
                    uint16_t position = calculateIrisPosition(thisRepeat,(keyValue == KEYCODE_IRIS_OPEN)?true:false);
                    if ( position < 0xFFFF )
                    {
                        queueCameraCommand( COMMAND_SET_IRIS, position);
                    }
                }
                break;

            case KEYCODE_IRIS_AUTO :
                if ( getCameraDim() == true )
                {
                    cancelCameraDim();
                }
                else
                {
                    if (thisRepeat == 1)
                    {
                        queueCameraCommand(COMMAND_AUTOIRIS, 1);
                    }
                }
                break;
            case KEYCODE_SCREEN :
                if (getVisualsPowerState())
                {
                    operateScreen(thisRepeat);
                }
                else
                {
                    // we can try and put the screen away even if visuals power is off
                    forceScreenIn();
                }
                break;
            case KEYCODE_PROJECTOR :
                if (thisRepeat == (1000/KEYREPEATmS)) // 1 second press
                {
                    toggleProjectorOnOff();
                }
                break;
            
            // debug
            case KEYCODE_MISC1 :
                //if (thisRepeat == 1)setLEDMode(LED_MISC1,(getLEDState(LED_MISC1) != 0)?LEDMODE_PATTERN_OFF:LEDMODE_PATTERN_ON); // debug code
                if (thisRepeat == 1)
                {
                    enableSavePresetMode(false);
                }
                else if (thisRepeat == (1000/KEYREPEATmS)) // 1 second press
                {
                    enableSavePresetMode(true);
                }
                break;

            case KEYCODE_MISC2 :
                if (thisRepeat == 1)setLEDMode(LED_MISC2,(getLEDState(LED_MISC2) != 0)?LEDMODE_PATTERN_OFF:LEDMODE_PATTERN_ON); // debug code
                break;
            case KEYCODE_SPARE :
                if (thisRepeat == 1)setLEDMode(LED_SPARE,(getLEDState(LED_SPARE) != 0)?LEDMODE_PATTERN_OFF:LEDMODE_PATTERN_ON); // debug code
                break;

            default :
                {
                    char buffer[30];
                    getInputName(buffer,keyValue);
                    Serial.printf(F("Keyvalue %d %s not handled\n"),keyValue, buffer);
                }
                break;

        }
    }

}

//pp:280:9: error: expected ';' before '}' token


// define an array of button matrix scan pins
//static const byte buttonOutputs[] =
static const byte buttonOutputs[] PROGMEM =
{
    BUTTON_OUT0_PIN,
    BUTTON_OUT1_PIN,
    BUTTON_OUT2_PIN,
    BUTTON_OUT3_PIN,
    
};
#define BUTTON_OUT_LAST 4


// define an array of button matrix input scan pins
static const byte buttonInputs[] PROGMEM =
{
    BUTTON_IN0_PIN,
    BUTTON_IN1_PIN,
    BUTTON_IN2_PIN,
    BUTTON_IN3_PIN,
    BUTTON_IN4_PIN,
    BUTTON_IN5_PIN,
    BUTTON_IN6_PIN,
    BUTTON_IN7_PIN,
};

#define BUTTON_IN_LAST 8


#if(BUTTON_OUT_LAST > 4) || (BUTTON_IN_LAST > 8)
#error This design only caters for a maximum of 4 x 8 key scan matrix
#endif



// thistable is used to convert hardware scan codes to key values
static const byte keyLookup[] PROGMEM = 
{
    /* 0 */  KEYCODE_NO_KEY,
    /* 1 */  KEYCODE_ATEM_PRESET1,
    /* 2 */  KEYCODE_ATEM_PRESET2,
    /* 3 */  KEYCODE_ATEM_PRESET3,
    /* 4 */  KEYCODE_ATEM_PRESET4,
    /* 5 */  KEYCODE_ATEM_PRESET5,
    /* 6 */  KEYCODE_ATEM_PRESET6,
    /* 7 */  KEYCODE_ATEM_PRESET7,
    /* 8 */  KEYCODE_ATEM_PRESET8,
    /* 9 */  KEYCODE_FTB,
    /* 10 */ KEYCODE_FOCUS_OUT,
    /* 11 */ KEYCODE_FOCUS_IN,
    /* 12 */ KEYCODE_FOCUS_AUTO,
    /* 13 */ 3,
    /* 14 */ 3,
    /* 15 */ 3,
    /* 16 */ 3,
    /* 17 */ KEYCODE_CAMERA_SELECT,
    /* 18 */ KEYCODE_CAMERA_PRESET1,
    /* 19 */ KEYCODE_CAMERA_PRESET2,
    /* 20 */ KEYCODE_CAMERA_PRESET3,
    /* 21 */ KEYCODE_CAMERA_PRESET4,
    /* 22 */ KEYCODE_CAMERA_PRESET5,
    /* 23 */ KEYCODE_CAMERA_PRESET6,
    /* 24 */ KEYCODE_CAMERA_PRESET7,
    /* 25 */ KEYCODE_MISC1,
    /* 26 */ KEYCODE_MISC2,
    /* 27 */ KEYCODE_IRIS_AUTO,
    /* 28 */ KEYCODE_IRIS_OPEN,
    /* 29 */ KEYCODE_IRIS_CLOSE,
    /* 30 */ KEYCODE_ZOOM_IN,
    /* 31 */ KEYCODE_ZOOM_OUT,
    /* 32 */ KEYCODE_FOCUS_OTAUTO,
    /* 33 */ KEYCODE_MP3,
    /* 35 */ KEYCODE_PROJECTOR,
    /* 35 */ KEYCODE_SCREEN,
    /* 36 */ KEYCODE_SPARE,
};


// OCR1A interrupt occurs every 5mS. Used for key matrix scanning which is set
// up as 8 bit wide x 4 slots giving an update rate of 20mS per full scan
ISR(TIMER1_COMPA_vect)
{
#if(BUTTON_OUT_LAST > 4) || (BUTTON_IN_LAST > 8)
#error This design only caters for a maximum of 4 x 8 key scan matrix
#endif

    static ubyte lastScan;
    static uint16_t keyDebounce;
    static ubyte keyscanphase;
    static ubyte oldAuxCache;
    static ubyte auxDebounce;

    ubyte thisScan = (ubyte)BUTTON_IN_PORT;
    digitalWrite(pgm_read_byte(&buttonOutputs[keyscanphase]), HIGH); // and set scan line high
    //apcdebug Serial.printf("H%d %d ",keyscanphase,pgm_read_byte(&buttonOutputs[keyscanphase]));
    static ubyte newScan = 0;
    if (++keyscanphase <= BUTTON_OUT_LAST )
    {
        ubyte ctr =0;
        if ( thisScan != 0xFF )
        {
            ctr = 1;
            do
            {
                if((thisScan & 0x01) == 0)
                {
                    newScan = ((keyscanphase-1) * 8) + ctr;
                    break;
                }
                else
                {
                    thisScan >>= 1;
                }
            }
            while ( ++ctr <= 8 );
        }
    }
    else
    {

        // every KEYSCANmS
        if (++webKeyCtr > 400/KEYSCANmS)
        {
            // we lost contact with web browser mid key press - clear the key
            webKey = KEYCODE_NO_KEY;
        }

        //if newScan != 0 we have one or more keys in the array pressed
        // now check the aux port value as it has some keys and also status inputs on it.
        // if any of the key boits are non zero set the newScan to 33-36
        if (auxCache != oldAuxCache)
        {
            oldAuxCache = auxCache;
            auxDebounce = 0;
        }
        else
        {
            if (++auxDebounce > 2)
            {
                // aux port state is stable, check the button input bits of it - 0-3 only
                auxDebounce = 3;
                auxPort = auxCache;
                ubyte ctr =0;
                thisScan = auxPort & 0x0F;
                if ( thisScan != 0 )
                {
                    ctr = 1;
                    do
                    {
                        if((thisScan & 0x01) != 0)
                        {
                            newScan = 32 + ctr;
                            break;
                        }
                        else
                        {
                            thisScan >>= 1;
                        }
                    }
                    while ( ++ctr <= 4 );
                }
                saveAudioPowerState(((auxPort & 0x10) ==0)?false:true);
                saveVisualsPowerState(((auxPort & 0x20) ==0)?false:true);

            }
        }
        newScan = pgm_read_byte(&keyLookup[newScan]);
        if (newScan ==0 && webKey != 0)
        {
            newScan = webKey;
        }
        // now debounce the value
        if ( newScan != lastScan )
        {
            keyDebounce = 0;
            lastScan = newScan;
        }
        else
        {
            if ( ++keyDebounce >= SHORTKEYDEBOUNCEmS/KEYSCANmS)
            {
                // max out at 10 seconds
                if ( keyDebounce > 10000/KEYSCANmS )
                {
                    keyDebounce = 10000/KEYSCANmS;
                }
                else
                // report a key once on the SHORTKEYDEBOUNCEmS boundary and then
                // every KEYSCANMULT x KEYSCANmS afterwards (nom 1 00mS). (Used for timing long key presses)
                {
                    uint16_t calc = keyDebounce - SHORTKEYDEBOUNCEmS/KEYSCANmS ;
                    if ( (calc % KEYSCANMULT) == 0 )
                    {
                        // set the variables that can be read by forground code
                        if (newScanCode != 0 && lastScan == 0 && newKeyRepeatCtr < 8)    
                        {
                            // user has just released a key after a short press < 800mS. Report that short press now
                            newKeyRepeatCtr = 0;
                            keyDebounce = 0; // and force the new nokey to be reported after further debounce
                        }
                        else
                        {
                            newKeyRepeatCtr = 1 + calc/KEYSCANMULT;
                            newScanCode = newScan;
                        }
                        //printTime(true);Serial.printf("Key %d repeat %d aux port 0x%0x\n",newScanCode, newKeyRepeatCtr,auxPort);
                    }
                }
            }
        }

        f_readAux = true;// tell the forground to read the port again
        // clear the variables to start a new scan of the key matrix
        newScan = 0;
        keyscanphase = 0;    
    }

    digitalWrite(pgm_read_byte(&buttonOutputs[keyscanphase]), LOW); // set line low for next time

    // apcdebug Serial.printf("L%d %d\n",keyscanphase,pgm_read_byte(&buttonOutputs[keyscanphase]));
}


// Called on startup. Sets up everything button and radio related
void initButtons(void)
{

    printModuleStartup(PSTR("Button driver"),BUTTON_CPP_VER,BUTTON_H_VER,lastsave);

    // set all the button row and scan inputs up
    byte counter;
    for (counter = 0 ; counter < BUTTON_OUT_LAST ; counter++)
    {
        pinMode(pgm_read_byte(&buttonOutputs[counter]), OUTPUT);
        digitalWrite(pgm_read_byte(&buttonOutputs[counter]), HIGH);
    }
    for (counter = 0 ; counter < BUTTON_IN_LAST ; counter++)
    {
        pinMode(pgm_read_byte(&buttonInputs[counter]), INPUT_PULLUP);
    }

    // very last thing , setup regular timer interrupts
    // setup timer1 to act as key scan timer interrupt = every 5mS
    // it is nominally runing at 250Khz as set by Arduino init code so overflow
    //and interrupt at a count of 1250 using OCR1A match
    // WGMn3:0 = 0100 CTC mode.
    TCCR1A = 0x0; // no output compares allowed WGM1:0 are B00
    OCR1A = 1250; // overflow every 5mS
    TCCR1B = B00001011; // start running timer1, div 64, Clear TIMER1 on OCR1A match
    TIMSK1 |= B00000010; // enable interrupts on compare A
   


}


