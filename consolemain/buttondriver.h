/** @package 

    buttondriver.h
    
    Copyright(c)  2000
    
    Author: ANDREW COPSEY
    Created: AC  29/12/2018 17:40:01
	Last change: AC 02/01/2019 19:39:44
*/
#ifndef __buttondriver_h
#define __buttondriver_h

#define BUTTON_H_VER 4



// Define the period to do a complete pass of the keys. It is 5 loops in total each 5ms
#define KEYSCANmS 25
#define SHORTKEYDEBOUNCEmS 40
#define KEYSCANMULT 5
#define KEYREPEATmS (KEYSCANMULT * KEYSCANmS)

// When a key is down it is reported once at SHORTKEYDEBOUNCEmS and again every
// KEYSCANMULT x KEYSCANmS or nominally at 40ms and again every 100mS
// the values in  the shared variables will be as follows if a given key is pressed
//   Time       newKey      newKeyRepeatCtr
//   20mS       0           0
//   40mS       KEYCODE     1
//   140mS      KEYCODE     2
//   240mS      KEYCODE     3
// etc


typedef enum t_Keycodes
{
    KEYCODE_NO_KEY,
    KEYCODE_IRIS_AUTO,
    KEYCODE_IRIS_OPEN,
    KEYCODE_IRIS_CLOSE,
    KEYCODE_ZOOM_OUT,
    KEYCODE_ZOOM_IN,
    KEYCODE_FOCUS_OTAUTO,
    KEYCODE_CAMERA_SELECT,
    
    // The code design needs these in order
    KEYCODE_CAMERA_PRESET1,
    KEYCODE_CAMERA_PRESET2,
    KEYCODE_CAMERA_PRESET3,
    KEYCODE_CAMERA_PRESET4,
    KEYCODE_CAMERA_PRESET5,
    KEYCODE_CAMERA_PRESET6,
    KEYCODE_CAMERA_PRESET7,
    // The code design needs the above kept in order

    // The code design needs these kept in order
    KEYCODE_ATEM_PRESET1,
    KEYCODE_ATEM_PRESET2,
    KEYCODE_ATEM_PRESET3,
    KEYCODE_ATEM_PRESET4,
    KEYCODE_ATEM_PRESET5,
    KEYCODE_ATEM_PRESET6,
    KEYCODE_ATEM_PRESET7,
    KEYCODE_ATEM_PRESET8,
// The code design needs the above kept in order
    
    KEYCODE_FTB,
    KEYCODE_FOCUS_OUT,
    KEYCODE_FOCUS_IN,
    KEYCODE_FOCUS_AUTO,
    // the follwowing order is how the switches appear on the web. 
    // they must be kept together
    KEYCODE_PROJECTOR,
    KEYCODE_SCREEN,
    KEYCODE_MP3,
    KEYCODE_MISC1,
    KEYCODE_MISC2,
    KEYCODE_SPARE,
    KEYCODE_LAST_VALUE,



};    

extern void manageKeyPresses(void);
extern void initButtons(void);
extern void handleWebKey(t_Keycodes keyCode, boolean pressed);
extern void getInputName(char *ptr, t_Keycodes keyCode);


#endif

