#include "globals.h"
PROGMEM const char lastsave[] ="2024-08-05 20:46:56";
#define UTILITIES_CPP_VER 5


#include <Streaming.h>

#include <ClientPanaAWHExTCP.h>

/*

	Last change: AC 12/04/2020 17:53:01
*/

#include <SkaarhojPgmspace.h>
// Include ATEM library
#include <ATEMbase.h>
#include <ATEMext.h>
#include "leddriver.h"
#include "buttondriver.h"
#include "atemutils.h"
#include "camerautils.h"
#include "utilities.h"
#include <Wire.h>
#include "config.h"
#include "screencontrol.h"
#include "tcpcontrol.h"




// If this is non zero we are running a test mode such as flashing leds
t_TestModes UITestMode = TEST_MODE_OFF;


#include "SkaarhojUtils.h"
extern SkaarhojUtils utils;



// create a null function at the start address, jump to this to reboot
void(* resetFunc) (void) = 0;//declare reset function at address 0


extern ubyte rebootDelay ;
void reboot(ubyte inTicks)
{
    if (inTicks == 0)
    {
        resetFunc();
    }
    else
    {
        rebootDelay = inTicks;
    }
}


// print a new line, then n x characters, then optional newline
void printRow(char character, uint8_t quantity, boolean addNewLine)
{
    printNewLine();
    do
    {
        Serial.printf(F("%c"),character);
    } while ( --quantity>0 );

    if ( addNewLine == true )
    {
        printNewLine();
    }
}

void printNewLine(void)
{
   Serial.printf(F("\n\r"));
}


#define SERBUFFSIZE 30
char inputBuffer[SERBUFFSIZE];
ubyte buffoffset;
static void clearSerialBuffer(void)
{
    buffoffset = 0;
    do
    {
        inputBuffer[buffoffset] = '\0';
    }while ( ++buffoffset < SERBUFFSIZE );
    buffoffset = 0;
}


enum serialStates
{
    SIS_IDLE,


};

enum serialStates serialInterfaceState = SIS_IDLE;


void clearBufferDisplayHelp(bool goToIdle)
{
    clearSerialBuffer();
    Serial.printf(F("\n\r\nHELP - St Johns console serial commands\n\r"));
    Serial.printf(F("T - Toggle User interface test mode - Reboot to Quit.\n\r"));
    Serial.printf(F("R - Reboot the device\n\r"));
    Serial.printf(F("1 2 3 4- screen OUT IN  DOWN UP. 0 turns off all relays\n\r"));


    if ( goToIdle == true )
    {
       serialInterfaceState = SIS_IDLE;
    }

};


char * getFirstVal(char* inputBuffer, int *value)
{
    int result = 0;
    ubyte counter = 0;
    char character;
    do
    {
        character = *inputBuffer++;
        if ( character >'9' || character < '0' )
        {
            inputBuffer--;
            break;
        }
        else
        {
            result = (result * 10) + (character - '0');
        }
    }
    while ( ++counter < 5 );
    *value = result;
    return inputBuffer;
}





void manageSerialInterface(void)
{
    static unsigned long timestamp;
    static ubyte timestamp1;
    static ubyte learningPin;
    if (Serial.available() > 0)
    {
        char receivedChar = toupper(Serial.read());

        if ( buffoffset < (SERBUFFSIZE-1) )
        {
            inputBuffer[buffoffset++] = receivedChar;
            inputBuffer[buffoffset] = '\0';
        }
        switch ( serialInterfaceState )
        {
            case SIS_IDLE :
                if ( receivedChar == 'T' )
                {
                    // go into or toggle out of test mode
                    UITestMode = (t_TestModes)((int)UITestMode + 1);
                    if ( UITestMode >= TEST_MODE_LAST_MODE )
                    {
                        Serial.printf(F("Disabling User interface test mode.\n\r"));
                        UITestMode = TEST_MODE_OFF;
                        setAllLEDs(LEDMODE_PATTERN_5050, 4);

                    }
                    else
                    {
                        printNewLine();
                        printRow('=',60,true);
                        Serial.printf(F("Enabling User interface test mode %d. T to step through modes.\n\r"),UITestMode);
                        Serial.printf(F("Keep pressing T or Reboot needed to return to normal operation\n\r"),UITestMode);
                    }
                    if ( UITestMode == TEST_MODE_RAPID_FLASH )
                    {
                        Serial.printf(F("Flashing all leds.\n\r"));
                        setAllLEDs(LEDMODE_PATTERN_5050, 255);
                    }
                    else if ( UITestMode == TEST_MODE_STEP_LEDS )
                    {
                        Serial.printf(F("Any key to Step through all leds. T to exit\n\r"));
                    }
                    else
                    {
                        setAllLEDs(LEDMODE_PATTERN_OFF, 0); // all leds OFF
                    }
                }
                if ( UITestMode != TEST_MODE_OFF )
                {
                    // any key during test mode (excpet T)
                    resetLEDTestTimer();
                }
                else
                {
                    if ( receivedChar >= '0' && receivedChar <= '4' )
                    {
                        // test the screen control relays
                        if ( receivedChar == '0' )
                        {
                            controlScreenRelays(0, false); // turn all off
                        }
                        else
                        {
                            controlScreenRelays(receivedChar - '0', true);
                        }
                    }
                    else if ( receivedChar == 'J' )
                    {
                        Serial.printf(F("\n\rGot J\n\r"));
                    }
                    else if ( receivedChar == 'S' )
                    {
                        Serial.printf(F("\n\rGot S\n\r"));
                    }
                    else if ( receivedChar == 'A' )
                    {
                        Serial.printf(F("\n\rGot A\n\r"));
                    }
                    else if ( receivedChar == 'I' )
                    {
                        Serial.printf(F("\n\rGot I\n\r"));
                    }
                    else if ( receivedChar == 'R' )
                    {
                        Serial.printf(F("\n\rGot R - reset\n\r"));
                        reboot(10);
                    }
                    else
                    {
                        if ( (millis() - timestamp) < 50 )
                        {
                            // left over from last send, throw it away
                        }
                        else
                        {
                            clearBufferDisplayHelp(false);
                            timestamp = millis();
                        }
                    }
                }
                clearSerialBuffer();
                break;
//            case SIS_WAIT_20mS :
//                break;


            default :
                clearBufferDisplayHelp(true);
                break;
        }
    }

}

typedef enum t_Keypress
{
    KEY_NOPRESS,
    KEY_SHORTPRESS_AND_RELEASE,
    KEY_SHORTPRESS,
    KEY_LONGPRESS
};








uint16_t maxheap, minstack;  // I declared these globally
bool memIncreased = false;
void initUtilities(void)
{
    maxheap = RAMSTART;
    minstack = RAMEND;

    printModuleStartup(PSTR("Utilities Module"),UTILITIES_CPP_VER,UTILITIES_H_VER,lastsave);

#ifdef zzzzapc
    report_mem();
    {
        uint8_t *ptr;
        ptr = maxheap;
        do 
        {
            *ptr = 0xFF;
        }while (++ptr < (minstack-12));
    }
    #endif
}


void printTime(boolean shortversion)
{
    unsigned long snapshot = millis();
    unsigned millis= (unsigned int)(snapshot %1000L);
    snapshot /= 1000L;
    unsigned int seconds = (unsigned int)(snapshot %60L);
    snapshot /= 60L;
    unsigned int minutes = (unsigned int)(snapshot %60L);
    snapshot /= 60L;
    unsigned int hours = (unsigned int)(snapshot);

    if ( shortversion != true )
    {
        Serial.printf(F("<%02d:%02d:"),hours,minutes);
    }
    else
    {
        Serial.printf(F("<"));
    }
    Serial.printf(F("%02d.%03d>"), seconds,millis);
}

static boolean lastVisualsPower;
static boolean lastAudioPower;

extern boolean isStartupComplete(void);

void saveAudioPowerState(boolean isOn)
{
    if (isOn != lastAudioPower)
    {
        lastAudioPower = isOn;
        // only act on this if the startup is complete

        if (isStartupComplete())
        {
            if (!isOn)
            {
                // turn off mp3 and bluetooth (if on)
                setLEDMode(LED_MP3, LEDMODE_PATTERN_OFF);
                setLEDMode(LED_BLUETOOTH_POWER, LEDMODE_PATTERN_OFF);
            }
            else
            {
                setSq6Scene(); // do adelayed audio scene set on the desk
            }
        }
    }
}
void saveVisualsPowerState(boolean isOn)
{
    if (isOn != lastVisualsPower)
    {
        lastVisualsPower = isOn;        
    }

}
boolean getVisualsPowerState(void)
{
    return lastVisualsPower;
}

boolean getAudioPowerState(void)
{
    return lastAudioPower;
}

/* This function places the current value of the heap and stack pointers in the
 * variables. You can call it from any place in your code and save the data for
 * outputting or displaying later. This allows you to check at different parts of
 * your program flow.
 * The stack pointer starts at the top of RAM and grows downwards. The heap pointer
 * starts just above the static variables etc. and grows upwards. SP should always
 * be larger than HP or you'll be in big trouble! The smaller the gap, the more
 * careful you need to be. Julian Gall 6-Feb-2009.
 */
#ifdef zzzzapcram
uint16_t findFreeRam(void)
{
    uint8_t *ptr;
    ptr = RAMSTART;
    uint16_t startOfFree = 0;
    uint16_t endOfFree = 0;
    
    uint16_t size = 0;
    do
    {
        if (*ptr == 0xFF)
        {
            ++size;
        }
        else
        {
            if (size > 128)
            {
                endOfFree = ptr;
                break;
            }
              size = 0;
        }
        if (size == 8)
        {
            startOfFree = ptr-8;
        }
        ++ptr;

    }while (ptr < minstack);
    Serial.printf(F("Start of free %d end of free %d\n"),startOfFree,endOfFree);
}

void check_mem() {
  
  uint16_t ptr;
  ptr = (uint16_t)malloc(4);  // use stackptr temporarily
  if (ptr > maxheap)
  {
    maxheap = ptr;
    memIncreased = true;
  }
  free(ptr);                        // free up the memory again (sets stackptr to 0)
  ptr =  (uint16_t)(SP);       // save value of stack pointer
  if (ptr < minstack)
  {
    minstack = ptr;
    memIncreased = true;
  }
}

void report_mem(void) 
{
    check_mem();
    printTime(false);
    if (memIncreased)
    {
        memIncreased = false;
        Serial.printf(F("Memory use increased \n"));
    }
    Serial.printf(F("RAM Start %04x RAM end %04x gap %04x\n\r"),RAMSTART,RAMEND,  minstack-maxheap );
    Serial.printf(F("Spare ram %d bytes\n\r"), minstack-maxheap );

}

#endif