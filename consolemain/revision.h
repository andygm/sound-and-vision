/*
*/



/*
    REv 0.1. Initial design
    REv 0.13. 30/12/2017 Most features working, now concentrating on tidy up of code
    to make it readable
    Rev 0.14 1/1/18. Code for first revision now complete. See to do list at bottom of
    main dot ino file for further work
    rev 1.00 1/1/2018. Updated defaults for ATEM so that defualt 3 now selects
    media player 1 which has a picture of St Johns Church loaded
    Rev 1.01. Added keys to preset save and apply. Needed update to ext library version
    as the key functions didn't work in the std library
    Rev 1.01a started adding IP camera support
    Rev 1.02a Nearly everything working fully. Needs tidying up and full testing
    See list at bottom of consolemain.ino for todo items
    Rev 1.02b Checked in. Going with this version on sunday
    REv 1.02c Correct multiple display of help on H key
    REv 1.02d Timin correct hopefull stopping lockups at 45 minutes approx
    REv 1.02e Changed 75mS timing trigger to avoid uneven led flashing during startup
    REv 1.02f Mentioned IDE version
    REv 1.02g Allowed IP address with a 0 in it
    rev1.03 Changed joystick tilt direction around. Fixed bug on occasional BT button repeats
    Rev 2.00 3/1/2019
        New hardware front panel with extra and better buttons. Speed control for Zoom
        Reduced timeouts on ATEM ethernet
        Changed button scans to update only every 100mS. It was possible to miss
        a pass and a button press when the update rate was 40mS and something delayed ENET comms.
        Changed timing on key repeats which will have altered iris/focus accelleration
        Added auto and quick focus buttons
        Added LEDS to show status of auto focus and auto iris
        Reduced camera count to 2, was 4
        Added check on I2C port 0 corruption - believed to be hardware caused
        Tried with Arduino 1.8.8. Found build problems. Reverted to 1.6.5
        Split up buttonsleds module into buttondriver and leddriver.
        Put radio input onto I2C0 rather than key scan matrix.
        Changed focus in/out to only send turn off auto one time
        changed OT focus to turn off auto focus
        Added control of radio learn mode to serial terminal
        Updated bluetooth firmware to 0.8.0 and did lots of testing on reliability
        Reduced deboiunce on buttons to 40mS - was 60
*/
/*
    rev 2.01 4/1/2019. fixed bug on zoom stop. tweaked joystick zero setting by adding a queue
    rev2.02 4/1/2019 Final code tweaks, tidying up only
    Rev 2.02+ 14/1/2019. Added improvments to focus and iris control to stop clogging the enet with duplicate commands
        Added camera blaning on moves by setting iris to dark and then putting it back again.
        Added save of iris, auto iris, auto focus states to camera presets
        Fixed bug so it saves the sources for aux and prog on atem presets
        Split utilities moduelinto three, utilities, camerautils and atemutils
        Added camera preset save and restore IRIS position, auto iris state, auto focus state
        Added delay sequence on aplying new camera preset. Iris is set to black, camera moves and then
        iris, autoiris and auto focus are reapplied
    Rev 2.03+ 14/1/2019 added camera dim using MISC1 button and led
        Fixed FTB led didn't flash correctly when black
    Rev 2.04 17/1/2019 Got camera fade to black working on presets and MISC 1 button
       fixed flashing led bugs
    Rev 2.04+ 18/2/2019 changed camera blank to use ATEM FTB instead
    Rev 2.05 18/2/2019. Added serach for camera on timeout. Avoids the camera slow
    bootup causing no connect on power up. Ditto for atem
    9/9/2019 swapped around zoom in and out directions
    swapped tilt direction

    Rev 2.06+ 20/3/2020. Added config for Projector IP address
        Added ability to configure which ATEM inputs have cameras fitted
        Added logic outputs for 4 relays to control screen in/out up/down
        Added ethernet control over projector on/off and status request
        Changed radio power enable pin to D41 as D39 pin was faulty
        Moved button and led const arrays into progmem
        Updated ethernet to rev 2.0 as it was causing problems with projector
        Added timing debug in projector function calls
    rev 2.07 14/4/2020. turned off debug and tweaked screen timings
    rev 2.08 15/4/2020 Added screen stop/return on button press

    rev 3.0x 12/09/2023 Started the swicthover to the new system, different projector, different port pins, no bluetooth
    rev 3.01 25/9/2023 Lots of code added and unused stuff stripped out
    rev 3.02 1/10/2023 Webserver confif working
    rev 3.03 3/10/2023 Leds and relays checked and debug added
    rev 3.04 5/10/2023 All home testing complete. SQ6 working - need to try with ATEM, camera and projector
    rev 3.05 6/10/2023 Disbaled camera commadns if visuals power is off        
    rev 3.06 7/10/2023 Corrected projector on off commands and decoding reply
    rev 3.07 10/10/2023 Added in config names for presets and lots of playing with webserver
    rev 3.08 14/10/2023 Corrected zom hold and added more to webserevr
    rev 3.09 19/10/2023 SQ6 scene is now stored as 1-nn rather than 0-n. Config SQ6 delay now shown on webinterface
    rev 3.10 26/10/2023 Most web stuff now done. See bottopm pof webserver .cpp for ToDo web items. Changhed pan/tilt acceleration calc
    rev 3.11 28/10/2023 Corrected repeat web commands being sent to camera. Tweaked led colours
    rev 3.12 31/10/2023 Did curve on pan/tilt commands from web. Also left timing debug in to show camera comms fails
    rev 3.13 21/11/2023 Added ram usage testing. 
    rev 3.14 22/11/2023 Fixed the sq6 scene send so it only happens if the key switch changes
    rev 3.16  05/01/2024 Added delayed call to getconf in webserver as we had problems with android browsers
    rev 3.17  11/01/2024 Changed metgods of web ptz hanling
    Rev 3.18 5/8/2024 Added config printout, added debig for atem lost contact. Added in different defaults for atem presets
        Added in leds flash differently when trying to save if ATEM or camera not detected. Has a lot of serial debug that can be
        remnoved after testing
    Rev 3.19 6/8/2024 Updated startup serial message dump
    

        

*/
extern void printRevString(void);
extern char *getRevString(void);
extern char *getCompileString(void);
extern void printModuleStartup(char* header,int cppVer,int hVer,char* lastsave);



/*
    To do 23/12/2023
        Experiment with sending multiple scene changes so desk is quiet on start up
        Change to w5500 network interface
        

    To do 26/10/2023
    Joystick accelleration seems weird and may be needs to be different pan vs tilt
    Iris adjust is very difficult to do. Also first click after auto iris always goes full open
    Focus cannot get the image correctly when close in.

    To do 19/11/2023
    Maybe swap to 5500 ethernet to get 8 connections



*/
