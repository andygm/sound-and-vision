#include "globals.h"
PROGMEM const char lastsave[] ="2024-08-05 16:22:27";
#define ATEM_CPP_VER 3
/*

	Last change: AC 14/04/2020 14:34:08
*/


// Include ATEM library and make an instance:
#include <ATEMbase.h>
#include <ATEMext.h>

// The port number is chosen randomly among high numbers.
ATEMext AtemSwitcher;


#include "globals.h"
#include <SkaarhojPgmspace.h>
#include "arduino.h"



#include <EEPROM.h>
#include <Wire.h>
#include "config.h"
#include "atemutils.h"
#include "utilities.h"
#include "leddriver.h"




// Some timers that prevent us continuously trying to contact devices that aren't connected
// This avoids the loop functions being delayed whilst ethernet retries are performed
static ubyte ATEMTimer;
void kickATEMTimer(void)
{
    ATEMTimer = ATEM_TIMEOUTmS/UI_UPDATEmS;
}





// Checks an ATEM source index for validity
static boolean validateVideoSrcIndex(t_VideoIndex source)
{
    // is the value we got a valid setting?
    //apc implement this!
    return true;
}

/*
    startStoredFTB

    This checks if the current program output is a camera one and if it is not already
    at black (eg after FTB)
    If true, it instigates a FTB and sets a timer
    When the timer runs down it will clear the fade to black

    The idea is that this function is called when a camera preset is applied. If the camera
    is visible on the program output, it fades to black and returns true
    Otherwise it does nothing and returns false
*/
static ubyte storedFTB = 255;
static ubyte atemFTBtimer;
#define ATEMBLACKWAITmS 1500


boolean startStoredFTB(void)
{

    #ifdef MAINDEBUG
    Serial.printf(F("DEBUG: ATEM start stored FTB\n\r"));
    #endif

    boolean status = false;
    int progSource = getATEMValue(ATEM_GET_PRG_SOURCE);
//    Serial.printf("prg %d cam %d\n\r",progSource, configStore.cameraInput[progSource-1]);
    if ( progSource < 9 && progSource > 0 )
    {
        // its a valid atem input 1-8
        if (configStore.cameraInput[progSource-1] == true)
        {
            // and it has a camera connected to program output
            if (getATEMValue(ATEM_GET_FTB_FULLY_BLACK) == true)
            {
                // already black. Don't do anything
                storedFTB= 255; //illegal number
            }
            else
            {
                //svae the current black rate. Set it to very quick and then
                // fade to black
                storedFTB = (ubyte)getATEMValue(ATEM_GET_FTB_RATE);
                doATEMCommand(ATEM_SET_FTB_RATE, 3); //very fast
                doATEMCommand(ATEM_FTB,0);
                // set timer so we will restore black level after the camera has
                // had time to move
                atemFTBtimer = ATEMBLACKWAITmS/UI_UPDATEmS;
                 status = true;
            }
        }
    }
    return status;
}

// clear any stored FTB action. Called on manual FTB actions such as push button
//or radio
static void clearStoredFTB(void)
{
    storedFTB = 255;
    atemFTBtimer = 0;
}

static void useStoredFTB(void)
{
    // The timer has just run to zero.
    // If FTB is black and there is a stored FTB rate value, put the rate back
    // and then perform FTB
    if ( (storedFTB > 0 && storedFTB < 255) && (getATEMValue(ATEM_GET_FTB_FULLY_BLACK) == true))
    {
        doATEMCommand(ATEM_SET_FTB_RATE, storedFTB);
        doATEMCommand(ATEM_FTB,0);
    }
    storedFTB = 255; // illegal value
}







static void setATEMPresetLED(ubyte ATEMChannel, ubyte state, ubyte time)
{
    if ( ATEMChannel >= MAX_ATEM_PRESETS )
    {
        Serial.printf(F("ERROR: ATEM channel too big %d\n\r"),ATEMChannel);
    }
    else
    {
        setLEDMode((tLED_ids)(ATEMChannel + (ubyte)LED_ATEM_PRESET1), state, time);
    }
}


#ifdef MAINDEBUG
const static char *ATEMCommandNames[] =
{
    "Startup",
    "FTB",
    "set FTB rate",
    "Auto DSK",
    "Change Preview",
    "Change Aux",
    "Do Auto",
    "set keyer on air enabled",
};
const static char *ATEMDataNames[] =
{
    "Connected",
    "PRG source",
    "AUX source",
    "full black?",
    "FTB rate",
    "DSK1?",
    "DSK2?",
    "is keyer on air enabled?",
};

#endif


// A helper function to wrap all ATEM commands. If noit
int doATEMCommand(t_ATEMCommands command, int value)
{
    int returnVal = 0;
    #ifdef MAINDEBUG
    Serial.printf(F("DEBUG: ATEM command %d (%s) val %d\n\r"),command,ATEMCommandNames[command],value);

    #endif
    
#ifdef atemstdlibrary
    #error This build not implemented fully
#else

    if (ATEM_STARTUP == command)
    {
        // Initialize a connection to the ATEM switcher:
        AtemSwitcher.begin(configStore.IPConfigArray[IP_ADDRESS_ATEM]);
        //  AtemSwitcher.serialOutput(0x82);  // set verbose debugging
        AtemSwitcher.serialOutput(0);  // no debugging
        AtemSwitcher.connect();
    }
    else
    {
        if (!getVisualsPowerState())
        {
            return returnVal;
        }
        boolean status = AtemSwitcher.isConnected();
        if (status == false)
        {
            Serial.printf(F("ATEM not present. Command %d value %d aborted.\n\r"),command,value);
        }
        else
        {
            switch ( command )
            {
                case ATEM_SET_FTB_RATE:
                    AtemSwitcher.setFadeToBlackRate(0,(uint8_t) value);
                    break;
                case ATEM_FTB:
                    if ( value != 0 )
                    {
                        // make sure we have no timed recover from FTB setup
                        clearStoredFTB();
                    }
                    AtemSwitcher.performFadeToBlackME(0);
                    break;
                case ATEM_AUTO_DSK :
                    AtemSwitcher.performDownstreamKeyerAutoKeyer((uint8_t) value);
                    break;
                case ATEM_CHANGE_PREVIEW :
                    AtemSwitcher.setPreviewInputVideoSource(0,AtemSwitcher.getVideoIndexSrc(value));
                    break;
                case ATEM_CHANGE_AUX :
                    AtemSwitcher.setAuxSourceInput(0,AtemSwitcher.getVideoIndexSrc(value));
                    break;
                case ATEM_DO_AUTO :
                    AtemSwitcher.performAutoME(0);
                    break;
                case ATEM_SET_KEYER_ON_AIR_ENABLED :
                    AtemSwitcher.setKeyerOnAirEnabled(0,0,value);
                    break;
        
                default :
                    Serial.printf(F("Not handled here\n\r"));
                    break;
            }

        }

    }
#endif
    return returnVal;
}

// A helper function to wrap all ATEM get data commands
int getATEMValue(t_ATEMDATACommands command)
{
    int returnVal = 0;
    #ifdef MAINDEBUG
    //Serial.printf(F("DEBUG: ATEM data request %d (%s)\n\r"),command,ATEMDataNames[command]);
    #endif

    
#ifdef atemstdlibrary
    #error This build not implemented fully
#else

    boolean status = AtemSwitcher.isConnected();
    if (status == false && command != ATEM_GET_IS_CONNECTED)
    {
        returnVal = false;
        //Serial.printf(F("DEBUG: ATEM not connected. Cannot get command %d\n\r"),command);
    }
    else
    {
        switch ( command )
        {
            case ATEM_GET_IS_CONNECTED :
                returnVal = status;
                break;
            case ATEM_GET_PRG_SOURCE :
                returnVal = AtemSwitcher.getVideoSrcIndex(AtemSwitcher.getProgramInputVideoSource(0));
                break;
            case ATEM_GET_AUX_SOURCE :
                returnVal = AtemSwitcher.getVideoSrcIndex(AtemSwitcher.getAuxSourceInput(0));
                break;
            case ATEM_GET_FTB_FULLY_BLACK :
                returnVal = AtemSwitcher.getFadeToBlackStateFullyBlack(0);
                break;
            case ATEM_GET_FTB_RATE :
                returnVal = AtemSwitcher.getFadeToBlackRate(0);
                break;
            case ATEM_GET_DSK1_ON_AIR :
            case ATEM_GET_DSK2_ON_AIR :
                returnVal = AtemSwitcher.getDownstreamKeyerOnAir((ubyte)command - (ubyte)ATEM_GET_DSK1_ON_AIR);
                break;
            case ATEM_GET_KEYER_ON_AIR_ENABLED :
                returnVal = AtemSwitcher.getKeyerOnAirEnabled(0,0);
                break;
    


            default :
                Serial.printf(F("ATEMGet %d Not handled here\n\r"),command);
                break;
        }
    }

#endif
    return returnVal;
}

void dumpKeys(void)
{
    Serial.printf(F("Dump keys - needs to be delayed till ATEM is on line apczzz\n"));
    for (uint8_t me = 0; me < 2; me++)
    {
        for (uint8_t key = 0; key < 4; key++)
        {
            Serial.printf(F("Me%d Key %d source %d fill source %d\n"),me,key,AtemSwitcher.getKeyerKeySource(me, key), AtemSwitcher.getKeyerFillSource(me,key));    
        }

    }
}

/*
    Gets the current settings from the ATEM box and puts them into our RAM working
    copy preset store for the selected preset number.
    It then updates the EEPROM copy of this preset.
    The ATEM parameters read and saved are:
    a) Downstreamkey on air number 1 enabled/disabled
    b) Downstreamkey on air number 2 enabled/disabled
    c) Upstreamkeyer on air enabled/disabled
    d) The current source going to program output
    d) The current source going to aux output (the stage)
*/
void saveATEMPreset(ubyte index)
{
    t_VideoIndex sourceIndex;
    bool keyEnabled;
    #ifdef MAINDEBUG
    Serial.printf(F("DEBUG: Saving ATEM preset %d\n\r"),index);
    #endif
    if ( index < MAX_ATEM_PRESETS )
    {
        kickATEMTimer();

        if (getATEMValue(ATEM_GET_IS_CONNECTED))
        {
            setATEMPresetLED(index, LEDMODE_PATTERN_5050, 30);
    
            keyEnabled = getATEMValue(ATEM_GET_DSK1_ON_AIR);
            configStore.ATEMPresetStore[index].dsk1enabled = keyEnabled;
            keyEnabled = getATEMValue(ATEM_GET_DSK2_ON_AIR);
            configStore.ATEMPresetStore[index].dsk2enabled = keyEnabled;
            keyEnabled = getATEMValue(ATEM_GET_KEYER_ON_AIR_ENABLED);
            configStore.ATEMPresetStore[index].usk1enabled = keyEnabled;
    
            sourceIndex = (t_VideoIndex)getATEMValue(ATEM_GET_PRG_SOURCE);
            if (  validateVideoSrcIndex(sourceIndex))
            {
                configStore.ATEMPresetStore[index].progIndex = sourceIndex;
            }
    
            sourceIndex = getATEMValue(ATEM_GET_AUX_SOURCE);
            if (  validateVideoSrcIndex(sourceIndex))
            {
                configStore.ATEMPresetStore[index].auxIndex = sourceIndex;
            }

        }
        else
        {
            Serial.printf(F("DBGATEM not present. No changes made"));
            setATEMPresetLED(index, LEDMODE_PATTERN_ON , 20);
        }
    }
    saveConfigStoretoEEPROM(); // save any changes
}

/*
    Gets the current settings from indicated preset and applies them
    to the ATEM box
    The ATEM parameters read and applied are:
    a) Downstreamkey on air number 1 enabled/disabled
    b) Downstreamkey on air number 2 enabled/disabled
    c) Upstreamkeyer on air enabled/disabled
    d) The current source going to program output
    d) The current source going to aux output (the stage)
    The index should be from 0 - (MAX_ATEM_PRESETS-1)
*/
void applyATEMPreset(ubyte index)
{
    #ifdef MAINDEBUG

    Serial.printf(F("DEBUG: Applying ATEM preset %d\n\r"),index);
    #endif
    kickATEMTimer();
    // check the prog preset first
    setATEMPresetLED(index,  LEDMODE_PATTERN_5050, 8);
    // check and apply the keys
    bool keyEnabled = getATEMValue(ATEM_GET_DSK1_ON_AIR);
    if (configStore.ATEMPresetStore[index].dsk1enabled != keyEnabled)
    {
        doATEMCommand(ATEM_AUTO_DSK,0);
    }
    keyEnabled = getATEMValue(ATEM_GET_DSK2_ON_AIR);
    if (configStore.ATEMPresetStore[index].dsk2enabled != keyEnabled)
    {
        doATEMCommand(ATEM_AUTO_DSK,1);
    }

    keyEnabled = getATEMValue(ATEM_GET_KEYER_ON_AIR_ENABLED);
    if (configStore.ATEMPresetStore[index].usk1enabled != keyEnabled)
    {
        doATEMCommand(ATEM_SET_KEYER_ON_AIR_ENABLED,configStore.ATEMPresetStore[index].usk1enabled);
    }

    t_VideoIndex currentIndex = (t_VideoIndex)getATEMValue(ATEM_GET_PRG_SOURCE);
    if ( currentIndex != configStore.ATEMPresetStore[index].progIndex )
    {
        //Serial.printf("Curprogindex is %d, we need %d\n\r",currentIndex,configStore.ATEMPresetStore[index].progIndex);
        // set preview to the one we want
        doATEMCommand(ATEM_CHANGE_PREVIEW,configStore.ATEMPresetStore[index].progIndex );
        // now do an auto transition to put the preview onto the program output
        doATEMCommand(ATEM_DO_AUTO,0);
    }
    // check the aux preset
    currentIndex = getATEMValue(ATEM_GET_AUX_SOURCE);
    if ( currentIndex != configStore.ATEMPresetStore[index].auxIndex )
    {
        //Serial.printf("Curauxindex is %d, we need %d\n\r",currentIndex,configStore.ATEMPresetStore[index].auxIndex);
        // set aux to the one we want
        doATEMCommand(ATEM_CHANGE_AUX,configStore.ATEMPresetStore[index].auxIndex);
    }
}

/*
    Gets the current settings from the ATEM box and compares them against
    all our working copies of presets. If it finds a match that button LED is turned on
    It compares the following parameters.
    a) Downstreamkey on air number 1 enabled/disabled
    b) Downstreamkey on air number 2 enabled/disabled
    c) Upstreamkeyer on air enabled/disabled
    d) The current source going to program output
    d) The current source going to aux output (the stage)
*/

boolean isATEMPresetActive(ubyte index)
{
    boolean result = true;
    bool keyEnabled;
    // check the prog preset first
    t_VideoIndex currentIndex = (t_VideoIndex)getATEMValue(ATEM_GET_PRG_SOURCE);
    if ( currentIndex != configStore.ATEMPresetStore[index].progIndex )
    {
        result = false;
    }
    // check the aux preset
    currentIndex = getATEMValue(ATEM_GET_AUX_SOURCE);
    if ( currentIndex != configStore.ATEMPresetStore[index].auxIndex )
    {
        result = false;
    }
    // check the keys
    keyEnabled = getATEMValue(ATEM_GET_DSK1_ON_AIR);
    if (configStore.ATEMPresetStore[index].dsk1enabled != keyEnabled)
    {
        result = false;
    }
    keyEnabled = getATEMValue(ATEM_GET_DSK2_ON_AIR);
    if (configStore.ATEMPresetStore[index].dsk2enabled != keyEnabled)
    {
        result = false;
    }
    keyEnabled = getATEMValue(ATEM_GET_KEYER_ON_AIR_ENABLED);

    if (configStore.ATEMPresetStore[index].usk1enabled != keyEnabled)
    {
        result = false;
    }

    return result;
}



void defaultATEMPresets(void)
{
    // we'll set Preset 1 as both aux and main outputs follow preachers laptop
    // That is ATEM SDI input- input no 5
    // we'll set Preset 2 as aux output and prog follow Propresenter laptop.
    // Program will follow laptop main output (ATEM HDMI input 1) and aux
    // will follow ATEM input 2 - the stage display stream
    // Preset 3 we3'll set to Media player 1 which has a picture of st Johns
    // Presets 4 upwards will defualt to assorted colour bars on each source
     // follow stage display
    ubyte index;
    Serial.printf(F("Defaulting all ATEM presets\n\r"));
    for ( index = 0; index < MAX_ATEM_PRESETS; index++ )
    {
        // flash all preset leds slowly for 2-3 seconds
        setATEMPresetLED(index, LEDMODE_PATTERN_5050 |LEDMODE_SPEED_SLOW, 40);
        switch ( index )
        {
            case 0:// preset 1
                configStore.ATEMPresetStore[index].progIndex = 5; // set program to preachers laptop
                configStore.ATEMPresetStore[index].auxIndex = 43; // as per program
                configStore.ATEMPresetStore[index].dsk1enabled = false;
                configStore.ATEMPresetStore[index].dsk2enabled = false;
                configStore.ATEMPresetStore[index].usk1enabled = false;
                break;
            case 1:// preset 2
                configStore.ATEMPresetStore[index].progIndex = 1; // set program to propresenter laptop
                configStore.ATEMPresetStore[index].auxIndex = 2; // set to PP stage view
                configStore.ATEMPresetStore[index].dsk1enabled = false;
                configStore.ATEMPresetStore[index].dsk2enabled = false;
                configStore.ATEMPresetStore[index].usk1enabled = false;
                break;
            case 2:// preset 3
                configStore.ATEMPresetStore[index].progIndex = 6; // PTZ camera 1
                configStore.ATEMPresetStore[index].auxIndex = 43; // follow program
                configStore.ATEMPresetStore[index].dsk1enabled = 0;
                configStore.ATEMPresetStore[index].dsk2enabled = 0;
                configStore.ATEMPresetStore[index].usk1enabled = 0;
                break;
            case 3: // preset 4
                configStore.ATEMPresetStore[index].progIndex = 6; // PTZ camera
                configStore.ATEMPresetStore[index].auxIndex = 43; // follow program
                configStore.ATEMPresetStore[index].dsk1enabled = 1; //overlay using PP main as mask
                configStore.ATEMPresetStore[index].dsk2enabled = 0;
                configStore.ATEMPresetStore[index].usk1enabled = 0;
                break;
            case 4:// preset 3
                configStore.ATEMPresetStore[index].progIndex = 1; // Sound desk laptop main
                configStore.ATEMPresetStore[index].auxIndex = 43; // as per program
                configStore.ATEMPresetStore[index].dsk1enabled = 0;
                configStore.ATEMPresetStore[index].dsk2enabled = 0;
                configStore.ATEMPresetStore[index].usk1enabled = 0;
                break;
            case 5:// preset 4
                configStore.ATEMPresetStore[index].progIndex = 24; // media player1
                configStore.ATEMPresetStore[index].auxIndex = 43; // as per program
                configStore.ATEMPresetStore[index].dsk1enabled = 0;
                configStore.ATEMPresetStore[index].dsk2enabled = 0;
                configStore.ATEMPresetStore[index].usk1enabled = 0;
                break;
            default :
                configStore.ATEMPresetStore[index].progIndex = 21; // colourbars
                configStore.ATEMPresetStore[index].auxIndex = 22; //
                configStore.ATEMPresetStore[index].dsk1enabled = 0;
                configStore.ATEMPresetStore[index].dsk2enabled = 0;
                configStore.ATEMPresetStore[index].usk1enabled = 0;
                break;
        }
    }
    for ( index = 0; index < MAX_ATEM_INPUTS; index++ )
    {
        configStore.cameraInput[index] = (index== 5)?true:false;
    }
    saveConfigStoretoEEPROM();  // save any changes
}

static ubyte ATEMrecovery;
static uint16_t apcCtrDebug = 0;
void apcdebug(void)
{
    Serial.printf(F("Atem timer %d recovery %d loopctr %d\n"),ATEMTimer,ATEMrecovery,apcCtrDebug);

}
// called every UI_UPDATEmS tick - nominally 75mS
void atemTick75mS(void)
{
    static ubyte apc;
    if (++apc > 100)
    {
        apc= 0;
        apcdebug();
    }
    boolean status = getATEMValue(ATEM_GET_IS_CONNECTED);
    if ( status == true )
    {
        kickATEMTimer();
    }
    else  if ( ATEMTimer > 0 )
    {
        ATEMTimer--;
    }
    if ( ATEMTimer == 0 )
    {
        if ( --ATEMrecovery == 0 )
        {
            ATEMTimer = 5; //have a short stab at reconnecting
        }
    }


    if ( atemFTBtimer > 0 &&  --atemFTBtimer == 0)
    {
        useStoredFTB();
    }


    setLEDMode(LED_ATEM_ENET_OK, status == true?LEDMODE_PATTERN_ON:LEDMODE_PATTERN_OFF);

    // ATEM Fade to black fully in black state?
    setNonBusyLED(LED_FTB, (getATEMValue(ATEM_GET_FTB_FULLY_BLACK) == true)?
        LEDMODE_SPEED_VERY_SLOW |LEDMODE_PATTERN_5050: LEDMODE_PATTERN_OFF);

    // Now check if the ATEM settings match any of our presets
    for ( ubyte ledNumber = (ubyte)LED_ATEM_PRESET1; ledNumber < (MAX_ATEM_PRESETS+(ubyte)LED_ATEM_PRESET1) ; ledNumber++ )
    {
        setNonBusyLED((tLED_ids)ledNumber, (isATEMPresetActive(ledNumber - LED_ATEM_PRESET1) == true && ( status == true ))
            ? LEDMODE_PATTERN_ON:LEDMODE_PATTERN_OFF);
    }

}



// called every pass of the main loop - nominally faster than per milliscond but
// can be a lot slower at times
void loopATEM(void)
{
    if ( ATEMTimer > 0)
    {
        AtemSwitcher.runLoop();
        ++apcCtrDebug;
    }
}

void initATEM(void)
{
    delay(400);

    printModuleStartup(PSTR("ATEM Utilities"),ATEM_CPP_VER,ATEM_H_VER,lastsave);
    Serial.printf(F("\t"));
    printIPAddress(CID_IP_ADDRESS_ATEM);


    doATEMCommand(ATEM_STARTUP,0);

    #if (ATEM_CPP_VER != ATEM_H_VER)
        #error ATEMUtils cpp and h not same revison
    #endif    

}


