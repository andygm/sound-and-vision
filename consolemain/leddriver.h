#ifndef __buttonsleds_h
#define __buttonsleds_h
/*

	Last change: AC 07/04/2020 15:15:23
*/

#define LED_H_VER 3



typedef enum
{
    // this design needs the ATEM presets as a group and in order
    LED_ATEM_PRESET1,
    LED_ATEM_PRESET2,
    LED_ATEM_PRESET3,
    LED_ATEM_PRESET4,
    LED_ATEM_PRESET5,
    LED_ATEM_PRESET6,
    LED_ATEM_PRESET7,
    LED_ATEM_PRESET8,

    LED_FTB,
    LED_ATEM_ENET_OK,
    LED_CAMERA_OK,

    // this design needs the Camera LEDs as a group and in order
    LED_CAMERA1,
    LED_CAMERA2,

    LED_MISC1,
    LED_MISC2,

    LED_AUTO_IRIS,
    LED_AUTO_FOCUS,

    LED_MP3,
    LED_PROJ,
    LED_SCREEN,
    LED_SPARE,
    LED_BLUETOOTH_POWER, // not realy a led - this controls the Bluetooth reciever power
    LED_LAST_LED
}tLED_ids;



// ledmodes that can be ored together when calling setLed or set
// You need to select a speed
#define LEDMODE_SPEED_VERY_SLOW 0x10
#define LEDMODE_SPEED_SLOW 0x20
#define LEDMODE_SPEED_FAST 0x00


// and or the speed with a pattern
#define LEDMODE_PATTERN_OFF 0 // LED is static OFF. Ignores any speed value
#define LEDMODE_PATTERN_ON 1  // led is static ON Ignores any speed value
#define LEDMODE_PATTERN_5050 2 // flash 50-50
#define LEDMODE_PATTERN_25PERCENT 3
#define LEDMODE_PATTERN_GROW 4
#define LEDMODE_PATTERN_12PERCENT 5
// maximum patterns up to 7 as 8 is the inverted bit

#define LEDMODE_SPEED_BIT_SHIFT 4
#define LEDMODE_PATTERN_BIT_SHIFT 0
#define LEDMODE_INVERTED 0x08
#define LEDMODE_CLEAR_ALL  LEDMODE_INVERTED // clear all bits except inverted





extern void setLEDMode(tLED_ids ledID, ubyte state, ubyte time = 0);
extern void setNonBusyLED(tLED_ids ledID, ubyte state, ubyte time = 0);
extern void setAllLEDs(ubyte state, ubyte time);
extern void setAllCameraLeds(ubyte state, ubyte time);
extern void manageLedsTick(void);
extern void initLeds(void);
extern void resetLEDTestTimer(void);


// Returns true if the LED is busy doing a timed function
extern boolean isLEDTimerBusy(tLED_ids ledID);

// Returns true if the LED is flashing or doing a timed pulse
extern boolean isLEDActive(tLED_ids ledID);

// returns 0 if OFF, 1 if flashing and 2 if steady ON at the instant of the call
extern ubyte getLEDState(tLED_ids ledID);




#endif


