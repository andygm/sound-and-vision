#ifndef __atemutils_h
#define  __atemutils_h
#include "globals.h"
#include "config.h"
/*

	Last change: AC 18/02/2019 11:24:48
*/


extern void saveATEMPreset(ubyte index);
extern void applyATEMPreset(ubyte index);
extern boolean isATEMPresetActive(ubyte index);

extern void defaultATEMPresets(void);

extern void loopATEM(void);

extern void kickATEMTimer(void);
extern void atemTick75mS(void);
extern boolean startStoredFTB(void);

extern void initATEM(void);

#define ATEM_H_VER 3


typedef enum t_ATEMDATACommands
{
    ATEM_GET_IS_CONNECTED,
    ATEM_GET_PRG_SOURCE,
    ATEM_GET_AUX_SOURCE,
    ATEM_GET_FTB_FULLY_BLACK,
    ATEM_GET_FTB_RATE,
    ATEM_GET_DSK1_ON_AIR,
    ATEM_GET_DSK2_ON_AIR,
    ATEM_GET_KEYER_ON_AIR_ENABLED,
};
extern int getATEMValue(t_ATEMDATACommands command);




typedef enum t_ATEMCommands
{
    ATEM_STARTUP,
    ATEM_FTB,
    ATEM_SET_FTB_RATE,
    ATEM_AUTO_DSK ,
    ATEM_CHANGE_PREVIEW ,
    ATEM_CHANGE_AUX ,
    ATEM_DO_AUTO ,
    ATEM_SET_KEYER_ON_AIR_ENABLED,
};

extern int doATEMCommand(t_ATEMCommands command, int value);






#endif //  __atemutils_h
