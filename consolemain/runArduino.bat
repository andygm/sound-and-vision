@echo off
rem This file should be in the root folder of the project. It should be run to copy the project preferences.txt file to the arduino executables folder.
rem It then opens the required version of arduino
rem
rem Assumptions
rem a portable version of the ide is installed and the
rem If you want to save settings on a project by porject basis, run the project the first time/
rem then set up board, bootloader. comms options etc and close arduino.


set _apcrevision=1.05
set _apcsketchname=consolemain
set _apcarduinoportableroot=C:\arduino_fixed_versions
set _apcarduinoversion=1.8.12
set _apcarduinoexepath=%_apcarduinoportableroot%\arduino-%_apcarduinoversion%

rem save the current directory where the real sketch is
set _apccurdir=%~dp0

echo.
echo ################################################################
echo This batch File is called runarduino.bat and its revision is z[31m %_apcrevision%
echo.
echo Current sketch directory is as follows:-
echo %_apccurdir%
echo.
echo Sketch name is %_apcsketchname%.ino
echo.
echo Using portable arduinoIDE installed here:-
echo  %_apcarduinoexepath%
echo.
echo Read this for more info on portable: https://www.arduino.cc/en/Guide/PortableIDE 
echo.
echo setting up arduino version %_apcarduinoversion% for sketch %_apcsketchname%
echo ################################################################
echo.
echo.


if exist "%_apccurdir%"\preferences.txt (
    echo Copying local preferences.txt file to %_apcarduinoexepath%\portable
    copy "%_apccurdir%"\preferences.txt %_apcarduinoexepath%\portable
) else (
    echo ++WARNING+++++++WARNING++++++++WARNING++++++++++++++++++++++++++
    echo WARNING: local preferences file does not exist. WARNING 
    echo Using the last one found in %_apcarduinoexepath%\portable
   echo.
    echo You may wish to set the project up, save preferences by closing arduino and then copy 
    echo %_apcarduinoexepath%\portable\preferences.txt back to the sketch path here:-
    echo %_apccurdir%
    echo +++END OF WARNING+++++++++++++++++++++++++++++++++++++++++++++++
    echo.
)
echo.

if exist %_apcarduinoexepath%\portable\sketchbook\%_apcsketchname% (
    echo Using symbolic link %_apcsketchname% to point at sketch location as follows:-
    echo %_apccurdir%
    echo.
) else (
    echo Symbolic link %_apcsketchname% does not exist.Trying to create it
    mklink /d %_apcarduinoexepath%\portable\sketchbook\%_apcsketchname% "%_apccurdir%"
     if exist %_apcarduinoexepath%\portable\sketchbook\%_apcsketchname% (
     echo Link created successfully
     )  else (
       echo Cannot create link. You may need to open and admin cmd prompt and type the following
        echo mklink /d %_apcarduinoexepath%\portable\sketchbook\%_apcsketchname% %_apccurdir%
       pause
       GOTO:eof
    )
)
echo.
echo - Hit any key to continue running Arduino
pause 

rem Opening arduino
cd %_apcarduinoexepath%
arduino.exe "%_apccurdir%\%_apcsketchname%.ino"

